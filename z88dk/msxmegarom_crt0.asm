;       MSX C stub
;
;       Stefano Bodrato - Apr. 2001
;
;	$Id: msx_crt0.asm,v 1.4 2002/04/24 08:15:02 stefano Exp $
;

	DEFC	RAM_Start  = $C000
	DEFC	ENASLT     = $0024      ;enable slot
	DEFC    RSLREG     = $0138      ;read primary slot select register
	DEFC    EXPTBL     = $FCC1      ;slot is expanded or not
	DEFC	CLIKSW	   = $F3DB	; variable to set/reset the keyboard click sound



                MODULE  msr_crt0

;
; Initially include the zcc_opt.def file to find out lots of lovely
; information about what we should do..
;

                INCLUDE "zcc_opt.def"

; No matter what set up we have, main is always, always external to
; this file

                XREF    _main

;
; Some variables which are needed for both app and basic startup
;

        XDEF    cleanup
        XDEF    l_dcal

; Integer rnd seed

        XDEF    int_seed

; vprintf is internal to this file so we only ever include one of the set
; of routines

	XDEF	_vfprintf

;Exit variables

        XDEF    exitsp
        XDEF    exitcount

       	XDEF	heaplast	;Near malloc heap variables
	XDEF	heapblocks

;For stdin, stdout, stder

        XDEF    __sgoioblk
        
; Slot 2 ROM address
	XDEF	_slot2address        

; Now, getting to the real stuff now!


        org     0x4000

        defb     0x41
        defb     0x42
        defw     start
        defw     0x0000
        defw     0x0000
        defw     0x0000
        defw     0x0000
        defw     0x0000
        defw     0x0000

.start
	di
	im 1
;	ld      sp,(0xfc4a)  
	ld	sp,0xf380
        

IF !DEFINED_nostreams
IF DEFINED_ANSIstdio
; Set up the std* stuff so we can be called again
	ld	hl,__sgoioblk+2
	ld	(hl),19	;stdin
	ld	hl,__sgoioblk+6
	ld	(hl),21	;stdout
	ld	hl,__sgoioblk+10
	ld	(hl),21	;stderr
ENDIF
ENDIF

; Page ROM into $8000 - $BFFF

ENAP2:
   	CALL   RSLREG      ;read primary slot #
   	RRCA         ;move it to bit 0,1 of [Acc]
   	RRCA
	AND   00000011B
   	LD   C,A
   	LD   B,0
   	LD   HL,EXPTBL   ;see if this slot is expanded or not
   	ADD   HL,BC
   	LD   C,A      ;save primary slot #
   	LD   A,(HL)      ;See if the slot is expanded or not
   	AND   $80
   	OR   C      ;set MSB if so
   	LD   C,A      ;save it to [C]
   	INC   HL      ;Point to SLTTBL entry
   	INC   HL
   	INC   HL
   	INC   HL
   	LD   A,(HL)      ;Get what is currently output
        	    	 ;to expansion slot register
   	AND   00001100B
   	OR   C      ;Finally form slot address
   	ld (_slot2address), A	; save it!
   	LD   H,80H
   	CALL   ENASLT      ;enable page 2

; Disable keyboard click
	xor a
	LD 	(CLIKSW), A
; Select very basic configuration of ROM banks
; We will use a Konami-without-SCC mapping
; 	Bank 1: Page 0 (always there!)
;	Bank 2: Page 1
;	Bank 3: Page 2
;	Bank 4: Page 3
	ld a,1
	ld ($6000), a
	inc a
	ld ($8000), a
	inc a
	ld ($A000), a
	EI
        call    _main
	
.cleanup
;
;       Deallocate memory which has been allocated here!
;

;IF !DEFINED_nostreams
;IF DEFINED_ANSIstdio
;	LIB	closeall
;	call	closeall
;ENDIF
;ENDIF

.start1
        ld      sp,0
        ret

.l_dcal
        jp      (hl)



; Now, which of the vfprintf routines do we need?


._vfprintf
IF DEFINED_floatstdio
	LIB	vfprintf_fp
	jp	vfprintf_fp
ELSE
	IF DEFINED_complexstdio
		LIB	vfprintf_comp
		jp	vfprintf_comp
	ELSE
		IF DEFINED_ministdio
			LIB	vfprintf_mini
			jp	vfprintf_mini
		ENDIF
	ENDIF
ENDIF


; Static variables kept in safe workspace

DEFVARS RAM_Start
{
	__sgoioblk      ds.b    40      ;stdio control block
	; Seed for integer rand() routines
	defltdsk       ds.b    1
	;Seed for integer rand() routines	
	int_seed       ds.w    1
	;Atexit routine
	exitsp        ds.w    1
	exitcount	ds.b    1
	; Heap stuff
	heaplast	ds.w	1
	heapblocks	ds.w	1
	; Slot 2 ROM address
	_slot2address	ds.b 	1
	; mem stuff
        ;defm  "Small C+ MSX"&0

;All the float stuff is kept in a different file...for ease of altering!
;It will eventually be integrated into the library
;
;Here we have a minor (minor!) problem, we've no idea if we need the
;float package if this is separated from main (we had this problem before
;but it wasn't critical..so, now we will have to read in a file from
;the directory (this will be produced by zcc) which tells us if we need
;the floatpackage, and if so what it is..kludgey, but it might just work!
;
;Brainwave time! The zcc_opt file could actually be written by the
;compiler as it goes through the modules, appending as necessary - this
;way we only include the package if we *really* need it!

	;seed for random number generator - not used yet..
	fp_seed        ds.w	3
	;Floating point registers...
	extra          ds.w    3
	fa             ds.w    3
	fasign         ds.b    1
}

IF NEED_floatpack
        INCLUDE         "#float.asm"
ENDIF

