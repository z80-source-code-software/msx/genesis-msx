Genesis: dawn of a new day MSX FULL source code release
-------------------------------------------------------

So here it is, the full source code release of the game by Retroworks. Full means:

- All source code is included
- All sprites/screens are included
- Full music is included
- Full makefile is included, so you can just type 'make' and get your own ROMafile

Requirements
------------

You need the following components to get a compiled game:

- z88dk, from www.z88dk.org. I used a self-compiled 1.9 version. Beware, some versions
  are buggy!!
- Pasmo assembler, from http://pasmo.speccy.org/
- APACK compressor from http://www.smspower.org/maxim/smssoftware/aplib.html 
  or http://www.ibsensoftware.com/download.html  
- Some kind of make utility (use GNU Make, for example)
- The simple fill8k utility, from the utils directory

Compiling your own version
--------------------------

First, you will need to add the files under the z88dk directory to your z88dk installation.
These files create a new target profile, called msxmegarom, and include all required startup
code. Feel free to use them for your next MSX MegaROM project. A makefile for Linux systems
is provided, if you want to compile it on Windows you'll have to do some minor adjustments.

There are many hardcoded addresses in the code, to overcome the limitations of z88dk and pasmo.
This is a huge mistake that caused me a lot of pain while coding the game, and I will never
repeat (I promise!). You will need to be very careful when modifying something, to avoid breaking 
something else. In short, after executing "make", check the symbol/map files and modify the following 
files according to the new addresses:

- constants.h  (info from levels_1.sym, levels_2.sym, wyzplayer.sym/player_2psg.sym, menu.sym)
- behavior.c (info from wyzplayer.sym/player_2psg.sym)
- engine.h (info from engine.sym)
- maindefs.asm (info from main1..map)
- movement.c (info from wyzplayer.sym/player_2psg.sym)

Then, run "make" again. If you are lucky, you will end up with a usable ROM file. And as a bonus
for doing this, you'll get the same dual PSG ROM we are using for the physical release.


Warnings, disclaimers and licensing
-----------------------------------

All code is distributed under the Apache License 2.0. You will see it on every source code
file. This is a pretty liberal license, which gives you rights even to produce closed-source
programs using this code, so I thought it would be an appropriate one for this game. For the
full license text, refer to http://www.apache.org/licenses/LICENSE-2.0.html

That said, some more warnings:

- The code is barely readable in some areas. Sorry, I had to make it that way.
- Most of the code is in English with reasonable comments... but there is some
  Spanish here and there. If you do not understand, do not worry, just ask me and
  I will try to help.
- If you have any trouble while playing around with the code, just let me know.
  I will try to help if I have some free time.
- All artwork is already processed from the original files (e.g. Mappy map files and similar). 
  If you want to have a look at the original files, please let me know. I'll be happy to 
  send them to you, together with some home grown conversion tools. But beware, sometimes I
  ended up modifying the processed files directly, for example with some level enemies :).
  
  
Contacts:
---------

- Coder & main responsible for this mess: utopian@retroworks.es
- Released by: http://www.retroworks.es
