//   Copyright 2012 Francisco Javier Peña
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.


#include <stdio.h>
#include <stdlib.h>

// Tiny simple utility to expand a ROM file up to 8 KB
//
// Syntax: fill8k.exe <bin file> <rom file> 
//
typedef unsigned short uint16;
typedef unsigned char uchar;



int main(int argc, char **argv)
{
	FILE *in, *out;
	unsigned char dummyb;
	unsigned int counter;

	if (argc != 3)
	{
		printf("Syntax: fill8k.exe <bin file> <rom file>\n");
		return(1);				
	}

	in=fopen(argv[1],"rb");
	if (!in)
	{
		printf("Error opening input file %s\n",argv[1]);
		return(1);
	}

	out=fopen(argv[2],"wb");
	if (!out)
	{
		printf("Error opening output file %s\n",argv[2]);
		return(1);
	}

	counter=0;	
	while(!feof(in))
	{
		dummyb=fgetc(in);
		if(!feof(in)) 
		{
			fputc(dummyb,out);
			counter++;
		}
	}
	while(counter<8192)
	{
			fputc(0,out);
			counter++;		
	}
	fclose(in);
	fclose(out);
	return(0);
}
