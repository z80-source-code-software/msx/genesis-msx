//   Copyright 2012 Francisco Javier Peña
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#asm
DEFVARS $C080
{
	_joystick_type	ds.b 1		; c080
	_joy		ds.b 1			; C081
	_RAMbank	ds.b 1			; c082
	_dummy_b	ds.b 1			; c083
	_dummy_i	ds.w 1			; c084
	_i		ds.b 1			; c086
	_new_enemy	ds.b 1			; c087
	_shoot_xchar	ds.b 1		; c088
	_shoot_ychar	ds.b 1		; c089

;load_level
	_level_pointer 	ds.w 1		; c08a
	_int_pointer	ds.w 1		; c08c
	_length_tiles	ds.w 1		; c08e
	_length_map	ds.w 1			; c090

; Level variables
	_CurLevel_XLength ds.b 1		; c092
	_CurLevel_NTiles  ds.b 1		; c093
	_map_xpos	ds.b 1			; c094
	_map_displacement ds.b 1		; c095
	_current_level	ds.b 1		; c096
	
; Draw state
       _draw_stage      ds.b 1		; c097
       _in_game		ds.b 1		; c098

; In-game variables
	_ship_x 	ds.b 1			; c099
	_ship_y 	ds.b 1			; c09a
	_speed_x 	ds.b 1			; c09b
	_speed_y	ds.b 1			; c09c
	_frames_to_shoot 	ds.b 1		; c09d
	_current_weapon 	ds.b 1		; c09e
	_current_weapon_sprite	ds.b 1	; c09f
	_current_weapon_energy	ds.b 1	; c0a0
	_current_screen	ds.b 1		; c0a1
	_max_shoots	ds.b 1			; c0a2
	_mayday		ds.b 1		; c0a3

; Ship sprites, used for dying animation
	 _ship0spr ds.b 1				; c0a4

; Array of existing enemies and shoots (max 8 enemies for now)
	_active_enemies		ds.b  96    ; c0a5
	_my_active_shoots 	ds.b	96	; c105
	_enemy_active_shoots	ds.b 	96	; c165
	_power_up		ds.b 	12		; c1c5

; Final enemy
	_final_enemy_active	ds.b 1	; c1d1
	_final_enemy_components	ds.b 1	; c1d2
	_fenemy_defeat		ds.b 1	; c1d3

; Used in behavior.c
	_new_e			ds.b 8	; c1d4

; Current song being played	
	_current_song		ds.b 1	; c1dc

; Number of frames when the fire key has been pressed
	_frames_fire_pressed	ds.b 1	; c1dd
	_available_superbombs	ds.b 1	; c1de
	_fenemy_activation_counter ds.b 1 ; c1df

; Score-related values
	_score			ds.w 1 ; c1e0
	_hiscore		ds.w 1 ; c1e2
	_next_extralife		ds.w 1 ; c1e4
	_life_counter		ds.b 1 ; c1e6
	_respawn_xpos		ds.b 1 ; c1e7
	_border_color		ds.b 1 ; c1e8
	_numbers		ds.b 5 ; c1e9 - c1ed
	_update_score		ds.b 1 ; c1ee
	_update_superb		ds.b 1 ; c1ef
	_update_life		ds.b 1 ; c1f0
	_previous_level		ds.b 1 ; c1f1
	_credit_counter		ds.b 1 ; c1f2
	_end_game		ds.b 1 ; c1f3

; Sprite colors
	_sprite_colors		ds.b 64 ; c1f4 - c233

; Delay for 60 Hz MSX systems
	_DELAY60Hz		ds.b 1  ; c234	

; Blink color for Genesis pieces
	_blink_color		ds.b 1	; c235
}
; XXX bytes for now, finishing at

; --- Start of Scope Defns ---
	XDEF	_joystick_type
	XDEF	_joy
	XDEF	_RAMbank
	XDEF	_dummy_b
	XDEF	_dummy_i
	XDEF	_i
	XDEF	_new_enemy
	XDEF	_shoot_xchar
	XDEF	_shoot_ychar
	XDEF	_level_pointer
	XDEF	_int_pointer
	XDEF	_length_tiles
	XDEF	_length_map
	XDEF 	_CurLevel_XLength
	XDEF 	_CurLevel_NTiles
	XDEF	_map_xpos
	XDEF	_map_displacement
	XDEF	_current_level
        XDEF    _draw_stage
        XDEF    _in_game
	XDEF	_ship_x 
	XDEF	_ship_y 	
	XDEF	_speed_x 	
	XDEF	_speed_y	
	XDEF	_frames_to_shoot 
	XDEF	_current_weapon 	
	XDEF	_current_weapon_sprite	
	XDEF	_current_weapon_energy	
	XDEF	_current_screen
	XDEF	_max_shoots
	XDEF	_mayday	
	XDEF	_ship0spr
	XDEF	_active_enemies		
	XDEF	_my_active_shoots 	
	XDEF	_enemy_active_shoots	
	XDEF	_power_up		
	XDEF	_final_enemy_active	
	XDEF	_final_enemy_components	
	XDEF	_fenemy_defeat		
	XDEF	_new_e
	XDEF	_current_song
	XDEF	_frames_fire_pressed
	XDEF	_available_superbombs
	XDEF 	_fenemy_activation_counter
	XDEF	_score			
	XDEF	_hiscore		
	XDEF	_next_extralife		
	XDEF	_life_counter
	XDEF	_respawn_xpos
	XDEF	_border_color
	XDEF 	_numbers
	XDEF	_update_score		
	XDEF	_update_superb		
	XDEF	_update_life	
	XDEF	_previous_level	
	XDEF	_credit_counter
	XDEF	_end_game
	XDEF	_sprite_colors
	XDEF	_DELAY60Hz	
	XDEF	_blink_color
#endasm
