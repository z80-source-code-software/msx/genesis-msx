//   Copyright 2012 Francisco Javier Peña
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

// Definition for the movement functions

#ifndef DUAL_PSG
	#define move_downleft	0x8C51
	#define move_downright	0x8C6C
	#define move_explosion	0x8A30
	#define move_find_enemy_to_follow 0x8E20
	#define move_follow_down 0x8E81
	#define move_follow_downright 0x8EA6
	#define move_follow_right 0x8E5C
	#define move_furby	0x8A46
	#define move_homing	0x8B37
	#define move_kamikaze	0x8AE3
	#define move_kamikaze_2	0x8DDD
	#define move_kamikaze_noanim 0x8D1B
	#define move_left	0x8985
	#define move_left_accel	0x8ECD
	#define move_left_expire 0x8999
	#define move_leftanim	0x8BA9
	#define move_leftanim2	0x8C09
	#define move_missile	0x8D74
	#define move_none	0x8F00
	#define move_pingpong	0x89C8
	#define move_pingpong_final4 0x89FC
	#define move_rest_final1 0x925A
	#define move_rest_final2 0x94FE
	#define move_rest_final3 0x9622
	#define move_rest_final4 0x9349
	#define move_rest_final5 0x98BC
	#define move_rest_final6 0x9AB0
	#define move_rest_final7 0x9E76
	#define move_right	0x89B1
	#define move_right_accel 0x8EE5
	#define move_target	0x8C9B
	#define move_up		0x8C8A
	#define move_upleft	0x8C18
	#define move_upright	0x8C30
	#define move_wave	0x8B75
	#define move_wave_nogoneleft 0x8D48
#else
	#define move_downleft	0x8D86
	#define move_downright	0x8DA1
	#define move_explosion	0x8B65
	#define move_find_enemy_to_follow 0x8F55
	#define move_follow_down 0x8FB6
	#define move_follow_downright 0x8FDB
	#define move_follow_right 0x8F91
	#define move_furby	0x8B7B
	#define move_homing	0x8C6C
	#define move_kamikaze	0x8C18
	#define move_kamikaze_2	0x8F12
	#define move_kamikaze_noanim 0x8E50
	#define move_left	0x8ABA
	#define move_left_accel	0x9002
	#define move_left_expire 0x8ACE
	#define move_leftanim	0x8CDE
	#define move_leftanim2	0x8D3E
	#define move_missile	0x8EA9
	#define move_none	0x9035
	#define move_pingpong	0x8AFD
	#define move_pingpong_final4 0x8B31
	#define move_rest_final1 0x938F
	#define move_rest_final2 0x9633
	#define move_rest_final3 0x9757
	#define move_rest_final4 0x947E
	#define move_rest_final5 0x99F1
	#define move_rest_final6 0x9BE5
	#define move_rest_final7 0x9FAB
	#define move_right	0x8AE6
	#define move_right_accel 0x901A
	#define move_target	0x8DD0
	#define move_up		0x8DBF
	#define move_upleft	0x8D4D
	#define move_upright	0x8D65
	#define move_wave	0x8CAA
	#define move_wave_nogoneleft 0x8E7D
#endif


void __FASTCALL__ *movement_funcs[]={move_none,move_left,move_right,move_pingpong,move_explosion,move_furby,move_kamikaze,move_wave,move_leftanim,move_upleft,move_upright,
				    move_downleft, move_left_expire,move_pingpong_final4,move_homing,move_downright,move_up,move_target,move_kamikaze_noanim,
				    move_wave_nogoneleft,move_missile,move_kamikaze_2,move_follow_right,move_follow_down,move_follow_downright,move_left_accel,move_right_accel,move_leftanim2};



