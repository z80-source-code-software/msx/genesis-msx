;   Copyright 2012 Francisco Javier Peña
;
;   Licensed under the Apache License, Version 2.0 (the "License");
;   you may not use this file except in compliance with the License.
;   You may obtain a copy of the License at
;
;       http://www.apache.org/licenses/LICENSE-2.0
;
;   Unless required by applicable law or agreed to in writing, software
;   distributed under the License is distributed on an "AS IS" BASIS,
;   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;   See the License for the specific language governing permissions and
;   limitations under the License.

org $A000

genesis_title_pattern 	INCBIN "genesis_title.sc1.bin"
genesis_title_color 	INCBIN "genesis_title.sc2.bin"
credits_bkg_pattern 	INCBIN "credits_bkg.sc1.bin"
credits_bkg_color 	INCBIN "credits_bkg.sc2.bin"
hiscores_pattern 	INCBIN "title_hiscore.sc1.bin"
hiscores_color 		INCBIN "title_hiscore.sc2.bin"
background		INCBIN "bkg.bin"
