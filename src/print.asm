;   Copyright 2012 Francisco Javier Peña
;
;   Licensed under the Apache License, Version 2.0 (the "License");
;   you may not use this file except in compliance with the License.
;   You may obtain a copy of the License at
;
;       http://www.apache.org/licenses/LICENSE-2.0
;
;   Unless required by applicable law or agreed to in writing, software
;   distributed under the License is distributed on an "AS IS" BASIS,
;   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;   See the License for the specific language governing permissions and
;   limitations under the License.

; Some support routines: print numbers

_numbers EQU $c1e9

; Decompose a 5-digit number in single digits
; INPUT:	HL: number
;		IX: pointer to string where the individual numbers will be located


; Divide HL by BC
;
; HL: number
; BC: divider
; DE: result (HL / BC)
; HL: remainder

divide_large:
    xor a
    ld de, 0
divide_loop:
    sbc hl, bc
    jp c, divide_completed
    inc de
    jp divide_loop    
divide_completed:
    add hl, bc
    ret

decompose_5digit:
	ld bc, 10000		; get the fifth digit
	call divide_large	; E has the result, HL the remainder
	ld (ix+0), e
	ld bc, 1000
	call divide_large
	ld (ix+1), e
	ld bc, 100
	call divide_large
	ld (ix+2), e
	ld bc, 10
	call divide_large
	ld (ix+3), e
	ld (ix+4), l		; L has the last remainder
	ret


; Print large char on screen (2x1), from a base number 0..9
; INPUT:
;		B: position in X (chars)
;		C: position in Y (chars)
;		A: number to print


PrintLargeNumber:
	; ẗhe position on VRAM is $1800 + Y*32 + X
	; the tile to store is 128 + 64 + A*2

	push af
	ld h, 0
	ld l, c

	add hl, hl	; *2
	add hl, hl	; *4
	add hl, hl	; *8
	add hl, hl	; *16
	add hl, hl	; *32
	ld de, $1800
	add hl, de

	ld a, b
	add a, l
	ld l, a
	pop af
	add a, a
	add a, 192
	push af
	push hl	
	call WRTVRM
	pop hl
	ld a, l
	add a, 32
	ld l, a
	pop af
	inc a
	call WRTVRM

	ret


; Print 2-digit number, large
; INPUT:
;		B: position in X (chars)
;		C: position in Y (chars)
;		A: number to print



PrintLarge_2:
	push bc
	ld e, a
	ld d, 10
	call Div8		; A = number /10 (first digit), D = remainder (second digit)	
	ld hl, _numbers
	ld (hl), a
	inc hl
	ld (hl), d	
	pop bc
	push bc
	push hl
	call PrintLargeNumber	; Print first number
	pop hl
	pop bc
	inc b			; move 1 char right
	ld a, (hl)		; get second digit
	call PrintLargeNumber	; Print second number
	ret

; Print 5-digit number, large
; INPUT:
;		B: position in X (chars)
;		C: position in Y (chars)
;		HL: number to print

PrintLarge_5:
	ld ix, _numbers
	push bc
	call decompose_5digit
	pop bc

	ld a, (ix+0)
	push bc
	call PrintLargeNumber
	pop bc
	inc b

	ld a, (ix+1)
	push bc
	call PrintLargeNumber
	pop bc
	inc b

	ld a, (ix+2)
	push bc
	call PrintLargeNumber
	pop bc
	inc b

	ld a, (ix+3)
	push bc
	call PrintLargeNumber
	pop bc
	inc b

	ld a, (ix+4)
	call PrintLargeNumber
	ret

; Print 5-digit number, small
; INPUT:
;		B: position in X (pixels)
;		C: position in Y (pixels)
;		D: base number type (0,10,20)
;		HL: number to print

PrintSmall_5:
	ld ix, _numbers
	push bc
	push de
	call decompose_5digit
	pop de
	pop bc
	ld a, (ix+0)
	push bc
	push de
	call PrintNumber
	pop de
	pop bc
	inc b

	ld a, (ix+1)
	push bc
	push de
	call PrintNumber
	pop de
	pop bc
	inc b

	ld a, (ix+2)
	push bc
	push de
	call PrintNumber
	pop de
	pop bc
	inc b

	ld a, (ix+3)
	push bc
	push de
	call PrintNumber
	pop de
	pop bc
	inc b

	ld a, (ix+4)
	call PrintNumber
	ret


; Print normal char on screen (1x1), from a base number 0..9
; INPUT:
;		B: position in X (chars)
;		C: position in Y (chars)
;		D: base number type (0,10,20)
;		A: number to print

PrintNumber:
	; ẗhe position on VRAM is $1800 + Y*32 + X
	; the tile to store is 128 + D + A

	push af
	push de
	ld h, 0
	ld l, c

	add hl, hl	; *2
	add hl, hl	; *4
	add hl, hl	; *8
	add hl, hl	; *16
	add hl, hl	; *32

	ld de, $1800
	add hl, de

	ld a, b
	add a, l
	ld l, a
	pop de
	pop af
	add a, d
	add a, 128
	call WRTVRM
	ret

; Print string on screen
; INPUT:
;		B: position in X (chars)
;		C: position in Y (chars)
;		IX: pointer to string, terminated by 0
PrintString:
	push af
	push de
	ld h, 0
	ld l, c

	add hl, hl	; *2
	add hl, hl	; *4
	add hl, hl	; *8
	add hl, hl	; *16
	add hl, hl	; *32

	ld de, $1800
	add hl, de
	ld a, b
	add a, l
	ld l, a
	pop de
	pop af

string_outerloop:
	ld a, (ix+0)
	and a
	ret z			; terminate with null
	sub 'a'			; a is the first char
	add a, 224		; 224 will be a
	call WRTVRM
	
	inc hl	
	inc ix			; next char in VRAM
	jp string_outerloop

	ret





;Divide 8-bit values
;In: Divide E by divider D
;Out: A = result, D = rest
;
Div8:
    xor a
    ld b,8
Div8_Loop:
    rl e
    rla
    sub d
    jr nc,Div8_NoAdd
    add a,d
Div8_NoAdd:
    djnz Div8_Loop
    ld d,a
    ld a,e
    rla
    cpl
    ret
