;   Copyright 2012 Francisco Javier Peña, Risto Jrvinen
;
;   Licensed under the Apache License, Version 2.0 (the "License");
;   you may not use this file except in compliance with the License.
;   You may obtain a copy of the License at
;
;       http://www.apache.org/licenses/LICENSE-2.0
;
;   Unless required by applicable law or agreed to in writing, software
;   distributed under the License is distributed on an "AS IS" BASIS,
;   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;   See the License for the specific language governing permissions and
;   limitations under the License.


org $8000

include "engine-functions.asm"
include "maindefs.asm"

RDVRM	EQU $004A	; Read the value of VRAM address HL into A
NSTARS	EQU 48		; number of stars per plane

; VRAM definitions

VRAMFRM     	EQU 	$0000          ; Tile Definitions
VRAMTILES   	EQU	$1800          ; Tile map (3x256 bytes)
VRAMATTR    	EQU	$1B00          ; Sprite attributes (Y,X,COL,NUM)
VRAMCOL     	EQU	$2000          ; Tile Colors
VRAMSPRITES 	EQU	$3800          ; Sprite definitions

; INPUT:
;	DE: last score. If higher than any of the current high scores
;	    we will ask the player to enter his-her name

menu:
	call check_hiscore
	ld HL, 12345
	ld (rand_seed), hl		; generate rand seed
	xor a
	ld (timer), a
	ld (screen_to_show), a
;	ld (inertia_cheat), a
	ld (anim_frame), a
	ld (fire_pressed_wait), a
	ld (key_i_pressed), a
	ld (key_s_pressed), a
	ld (starting_level), a
	ld a, 3
	ld (sound_selection), a

	call generate_starfield
	; First, uncompress pattern data and copy to VRAM
	ld hl, genesis_title_pattern
	ld de, $D600			; Safe buffer!
	call depack

	call VDP_ActDeact
	ld de, 0
	ld bc, 6912
	ld hl, $d600
	call LDIRVRM_safe		; forms + tile definition

	; Second, uncompress color data and copy to VRAM
	ld hl, genesis_title_color
	ld de, $D600			; Safe buffer!
	call depack	

	ld de, $2000
	ld bc, 6144
	ld hl, $d600
	call LDIRVRM_safe		; color definition

	; Enable VDP and continue...
	call VDP_ActDeact

	call menu_display_inertia_status
	call menu_display_sound_status

menu_loop: 
	ld a, (fire_pressed_wait)
	and a
	jr z, menu_move_stars
	dec a
	ld (fire_pressed_wait), a
	ret z	
menu_move_stars:
	;move starfield
	ld a, (timer)
	inc a
	ld (timer), a
	jp nz, menu_nochange
	ld a, (screen_to_show)
	inc a
	cp 3
	jr nz, menu_nowrap
	xor a			; wrap from 2 to 0
menu_nowrap:
	ld (screen_to_show), a
	jr z, menu_puttitle
	cp 1
	jr z, menu_puthiscores
menu_putcredits:
	; First, uncompress pattern data and copy to VRAM
	ld hl, credits_bkg_pattern
	ld de, $D600			; Safe buffer!
	call depack

	call VDP_ActDeact
	ld de, 0
	ld bc, 6912
	ld hl, $d600
	call LDIRVRM_safe		; forms + tile definition

	; Second, uncompress color data and copy to VRAM
	ld hl, credits_bkg_color
	ld de, $D600			; Safe buffer!
	call depack	

	ld de, $2000
	ld bc, 6144
	ld hl, $d600
	call LDIRVRM_safe		; color definition

	; Enable VDP and continue...
	call VDP_ActDeact
	jr menu_nochange	
menu_puthiscores:
	call showhiscores
	call menu_display_inertia_status
	call menu_display_sound_status
	jr menu_nochange
menu_puttitle:
	; First, uncompress pattern data and copy to VRAM
	ld hl, genesis_title_pattern
	ld de, $D600			; Safe buffer!
	call depack

	call VDP_ActDeact
	ld de, 0
	ld bc, 6912
	ld hl, $d600
	call LDIRVRM_safe		; forms + tile definition

	; Second, uncompress color data and copy to VRAM
	ld hl, genesis_title_color
	ld de, $D600			; Safe buffer!
	call depack	

	ld de, $2000
	ld bc, 6144
	ld hl, $d600
	call LDIRVRM_safe		; color definition

	; Enable VDP and continue...
	call VDP_ActDeact
	call menu_display_inertia_status
	call menu_display_sound_status
menu_nochange:
	call check_firepress
	jr nc, menu_nofirepressed		; fire not pressde
; fire pressed
	ld a, (fire_pressed_wait)
	and a
	jr nz, menu_nofirepressed		; ignore multiple fires
; Play sound
	ld hl,  FX_START_GAME
	push hl
        call _wyz_effect
        pop hl
	ld a, 20
	ld (fire_pressed_wait), a
menu_nofirepressed:
	; if we are at the credits screen, do not check for inertia or any other cheat!
	ld a, (screen_to_show)
	cp 2
	jr z, menu_continue
	call check_cheats
	jr nc, menu_check_sound

; Play sound
	ld hl, FX_MENU_INERTIA
	push hl
        call _wyz_effect
       pop hl

	call menu_display_inertia_status
	jr menu_continue

menu_check_sound:
	call check_sound
	jr nc, menu_check_jukebox

	ld hl, FX_MENU_INERTIA
	push hl
        call _wyz_effect
        pop hl

	call menu_display_sound_status

menu_check_jukebox:
	call check_jukebox


menu_continue:

	call erase_starfield
	call move_starfield
	call display_starfield
	ld a, (screen_to_show)
	and a
	jp nz, menu_loop		; only update the animation if we are on the title screen

	ld a, (anim_frame)	; This little piece of code will change the animation position every 8 frames
	inc a
	ld (anim_frame), a
	and $3F
	call DrawStarAnim

	jp menu_loop
	ret

menu_display_inertia_status:
	ld a, (inertia_cheat)
	and a
	jr nz, start_inertia_off
	ld b, 20
	ld c, 20
	ld ix, string_on
	call PrintString
	ret
start_inertia_off:
	ld b, 20
	ld c, 20
	ld ix, string_off
	call PrintString
	ret

menu_display_sound_status:
IF DEFINED DUAL_PSG
	ret
ELSE
	nop
ENDIF

	ld a, (sound_selection)
	and a
	jr z, menu_print_silence
	dec a
	jr z, menu_print_musiconly
	dec a
	jr z, menu_print_sfxonly
menu_print_music_and_sound:
	ld b, 10
	ld c, 23
	ld ix, string_music_and_fx
	call PrintString	
	ret
menu_print_sfxonly:
	ld b, 10
	ld c, 23
	ld ix, string_fx
	call PrintString	
	ret
menu_print_musiconly:
	ld b, 10
	ld c, 23
	ld ix, string_music
	call PrintString	
	ret
menu_print_silence:
	ld b, 10
	ld c, 23
	ld ix, string_silence
	call PrintString
	ret



showhiscores:
	; load title screen
	; First, uncompress pattern data and copy to VRAM
	ld hl, hiscores_pattern
	ld de, $D600			; Safe buffer!
	call depack

	call VDP_ActDeact
	ld de, 0
	ld bc, 6912
	ld hl, $d600
	call LDIRVRM_safe		; forms + tile definition

	; Second, uncompress color data and copy to VRAM
	ld hl, hiscores_color
	ld de, $D600			; Safe buffer!
	call depack	

	ld de, $2000
	ld bc, 6144
	ld hl, $d600
	call LDIRVRM_safe		; color definition

	; Enable VDP and continue...
	call VDP_ActDeact

showhiscores_loop:
	; loop through the hi score names
	ld b, 8
	ld c, 8
	ld ix, hiscore_names
	call PrintString
	ld b, 8
	ld c, 10
	ld ix, hiscore_names+9
	call PrintString
	ld b, 8
	ld c, 12
	ld ix, hiscore_names+18
	call PrintString
	ld b, 8
	ld c, 14
	ld ix, hiscore_names+27
	call PrintString
	ld b, 8
	ld c, 16
	ld ix, hiscore_names+36
	call PrintString
	ld b, 8
	ld c, 18
	ld ix, hiscore_names+45
	call PrintString

	; loop through the hi score names
	ld hl, (hiscore_values)
	ld ix, hiscore_string
	ld c, 8
	call printhi
	ld hl, (hiscore_values+2)
	ld ix, hiscore_string
	ld c, 10
	call printhi
	ld hl, (hiscore_values+4)
	ld ix, hiscore_string
	ld c, 12
	call printhi
	ld hl, (hiscore_values+6)
	ld ix, hiscore_string
	ld c, 14
	call printhi
	ld hl, (hiscore_values+8)
	ld ix, hiscore_string
	ld c, 16
	call printhi
	ld hl, (hiscore_values+10)
	ld ix, hiscore_string
	ld c, 18
	call printhi
	ret


; UPDATE STAR POSITION

move_starfield:
	ld ix, starsx_medium
	ld hl, stars_pixels_medium
	ld b, NSTARS
updatemedium_loop:
	inc (ix)			; update value 2 pixels right
	inc (ix)
	ld a, (hl)
	rrca				; rotate right 2 pixels
	rrca
	ld (hl), a
	inc hl
	inc ix
	djnz updatemedium_loop


	ld ix, starsx_fast
	ld hl, stars_pixels_fast
	ld b, NSTARS
updatefast_loop:
	inc (ix)			; update value 4 pixels right
	inc (ix)
	inc (ix)
	inc (ix)
	ld a, (hl)
	rrca				; rotate right 1 pixels
	rrca
	rrca
	rrca
	ld (hl), a
	inc hl
	inc ix
	djnz updatefast_loop

	ret



; DISPLAY STARFIELD
display_starfield:
	
	ld ix, starsx_medium
	ld iy, starsy_medium
	ld de, stars_pixels_medium
	ld a, NSTARS
	ld (starfield_variable), a
	call DisplayLoop

	ld ix, starsx_fast
	ld iy, starsy_fast
	ld de, stars_pixels_fast
	ld a, NSTARS
	ld (starfield_variable), a
	call DisplayLoop
	ret

DisplayLoop:
	; First, find position in color VRAM

	ld a, (ix)	; X position
	rrca
	rrca
	rrca
	and $1f		; X /8
	rlca
	rlca
	rlca
	ld l, a

	ld a, (iy)
	rrca
	rrca
	rrca
	and $1f		; Y /8
	add a, $20	
	ld h, a		; HL holds the position in color VRAM of this pixel
		
	call RDVRM	; read the value into A
	cp $E0
	jp nz, disp_dontdraw ; this is marking that this char is not ready for the background

	; Now, find position in pattern VRAM
	ld a, (ix)	; X position
	rrca
	rrca
	rrca
	and $1f		; X /8
	rlca
	rlca
	rlca
	ld l, a

	ld a, (iy)
	rrca
	rrca
	rrca
	and $1f		; Y /8
	ld h, a		
	ld a, (iy)
	and $07
	or l
	ld l, a		; HL now holds the position in VRAM

	ld a, (de)	; get pixels
	call WRTVRM	; write in VRAM

disp_dontdraw:

	inc ix
	inc iy
	inc de
	
	ld a, (starfield_variable)
	dec a
	ld (starfield_variable), a
	jp nz, DisplayLoop		; continue with loop
	ret




; ERASE STARFIELD
erase_starfield:
	
	ld ix, starsx_medium
	ld iy, starsy_medium
	ld a, NSTARS
	ld (starfield_variable), a
	call EraseLoop

	ld ix, starsx_fast
	ld iy, starsy_fast
	ld a, NSTARS
	ld (starfield_variable), a
	call EraseLoop
	ret

EraseLoop:
	; First, find position in color VRAM

	ld a, (ix)	; X position
	rrca
	rrca
	rrca
	and $1f		; X /8
	rlca
	rlca
	rlca
	ld l, a

	ld a, (iy)
	rrca
	rrca
	rrca
	and $1f		; Y /8
	add a, $20	
	ld h, a		; HL holds the position in color VRAM of this pixel
		
	call RDVRM	; read the value into A
	cp $E0
	jp nz, disp_donterase ; this is marking that this char is not ready for the background

	; Now, find position in pattern VRAM
	ld a, (ix)	; X position
	rrca
	rrca
	rrca
	and $1f		; X /8
	rlca
	rlca
	rlca
	ld l, a

	ld a, (iy)
	rrca
	rrca
	rrca
	and $1f		; Y /8
	ld h, a		
	ld a, (iy)
	and $07
	or l
	ld l, a		; HL now holds the position in VRAM

	xor a
	call WRTVRM	; write in VRAM

disp_donterase:

	inc ix
	inc iy
	inc de
	
	ld a, (starfield_variable)
	dec a
	ld (starfield_variable), a
	jp nz, EraseLoop		; continue with loop
	ret


; the starfield will be randomly generated. Y will be 32-160, X will be 0-255

; Supporting function
genstars_evenY:
		push hl
		push bc
		call rand
		pop bc
		ld a, l ; random value for X
		ld c, h	; random value for Y, in 0..127
		pop hl
		ld (hl), a
		inc hl

		ld a, c ; random value for Y, in 0..127
		add a, 28 ; now it is in 20..147
		and $FE	  ; make sure it is an even number

		ld (de), a
		inc de
		djnz genstars_evenY
		ret



genstars_oddY:
		push hl
		push bc
		call rand
		pop bc
		ld a, l ; random value for X
		ld c, h	; random value for Y, in 0..127
		pop hl
		ld (hl), a
		inc hl

		ld a, c ; random value for Y, in 0..127
		add a, 28 ; now it is in 20..147
		or 1	  ; make sure it is an odd number

		ld (de), a
		inc de
		djnz genstars_oddY
		ret


; Another supporting function
genpixels:
		ld a, (hl)		; we have the byte in A
		and $7			;
		ld c, a			; C has the pixel addr
		ld a, $80
genpix_shiftloop:
		rrca			; rotate right
		dec c
		jr nz, genpix_shiftloop   ; until we reach the end		
		ld (de), a		; store 
		inc hl
		inc de
		djnz genpixels
		ret


generate_starfield:	
		ld hl, starsx_medium
		ld de, starsy_medium
		ld b, NSTARS
		call genstars_evenY
		ld hl, starsx_fast
		ld de, starsy_fast
		ld b, NSTARS
		call genstars_oddY
; we have generated the addresses, now we have to generate the pixels...

		; Medium speed pixels
		ld hl, starsx_medium
		ld de, stars_pixels_medium
		ld b, NSTARS
		call genpixels

		; Fast pixels
		ld hl, starsx_fast
		ld de, stars_pixels_fast
		ld b, NSTARS
		call genpixels
		ret


; INPUT: 
; 	A: Animation position (0-7)

starattr db 24,32,0,$F

DrawStarAnim:
		;  draw the animation, just a sprite at X=32, Y=24
		ld hl, anim_order
		ld d,0
		ld e,a
		add hl, de
		ld a, (hl)	; A holds the animation position
		ld hl, 0
		and a
		jr z, noadd
		ld de, 32
dr_chooseanimpos:
		add hl, de
		dec a
		jr nz, dr_chooseanimpos
noadd:
		ld de, star_anim
		add hl, de		; Address of sprite animation
		ld de, VRAMSPRITES
		ld bc, 32
		call LDIRVRM_safe

		; Set attributes (in fact, it is only needed once, but anyway...)
		ld hl, starattr	
		ld de, VRAMATTR
		ld bc, 4
		call LDIRVRM_safe
		ret



; Print string on screen
; INPUT:
;		B: position in X (chars)
;		C: position in Y (chars)
;		IX: pointer to string, terminated by 0
PrintString:
	ld d, c
	ld a, b
	add a, a
	add a, a
	add a, a 
	ld e, a			; DE = position in VRAM 	

string_outerloop:
	ld a, (ix+0)
	and a
	ret z			; terminate with null
	sub 'a'			; a is the first char
	rlca
	rlca
	rlca
	and $f8			; multiply by 8

	ld hl, charfont
	ld c, a
	ld b, 0
	add hl, bc		; HL points to the first row of the char


	ld bc, 8

	push de
	push hl
	call LDIRVRM_safe
	pop hl
	pop de

	ld bc, 8
	ex de, hl
	add hl, bc
	ex de, hl		; next char in VRAM
	inc ix
	jp string_outerloop
	ret





; Print normal char on screen (1x1), from a base number 0..9
; INPUT:
;		B: position in X (chars)
;		C: position in Y (chars)
;		A: number to print

PrintNumber:
	push af
	ld d, c
	ld a, b
	add a, a
	add a, a
	add a, a 
	ld e, a			; DE = position in VRAM 
	pop af

	ld hl, numfont
	add a, a
	add a, a
	add a, a 		; multiply by 8


	ld c, a
	ld b, 0
	add hl, bc		


	ld bc, 8
	call LDIRVRM_safe
	ret


; INPUT:
;	HL: hi score value
;	IX: hiscore_string
;	C: value in Y
printhi:
	push bc
	call decompose_5digit
	pop bc
	ld b, 19
	ld a, (ix+0)
	push bc	
	call PrintNumber
	pop bc
	ld b, 20
	ld a, (ix+1)
	push bc	
	call PrintNumber
	pop bc
	ld b, 21
	ld a, (ix+2)
	push bc	
	call PrintNumber
	pop bc
	ld b, 22
	ld a, (ix+3)
	push bc	
	call PrintNumber
	pop bc
	ld b, 23
	ld a, (ix+4)
	call PrintNumber
	ret




; Divide HL by BC
;
; HL: number
; BC: divider
; DE: result (HL / BC)
; HL: remainder

divide_large:
    xor a
    ld de, 0
divide_loop:
    sbc hl, bc
    jp c, divide_completed
    inc de
    jp divide_loop    
divide_completed:
    add hl, bc
    ret

; INPUT: HL: number
; 	 IX: string with the number to print

decompose_5digit:
	ld bc, 10000		; get the fifth digit
	call divide_large	; E has the result, HL the remainder
	ld (ix+0), e
	ld bc, 1000
	call divide_large
	ld (ix+1), e
	ld bc, 100
	call divide_large
	ld (ix+2), e
	ld bc, 10
	call divide_large
	ld (ix+3), e
	ld (ix+4), l		; L has the last remainder
	ret

JOY_CURSOR 	EQU 0
JOY_0		EQU 1
JOY_1		EQU 2

; Read joysticks, set carry if fire is pressed
check_firepress:
	xor a
	call get_joystick
	and $10
	jr z, check_joy0
	ld a, JOY_CURSOR
	ld (selected_joystick), a
	scf
	ret
check_joy0:
	ld a, 1
	call get_joystick
	and $10
	jr z, check_joy1
	ld a, JOY_0
	ld (selected_joystick), a
	scf
	ret
check_joy1:
	ld a, 2
	call get_joystick
	and $10
	ret z			; no fire pressed, carry clear
	ld a, JOY_1
	ld (selected_joystick), a
	scf
	ret

; Check for cheats, set carry if any cheat was enabled
check_cheats:
	ld hl, KEY_I
	call get_keyboard
	and a
	jr z, no_cheat_pressed
	ld a, (key_i_pressed)
	and a
	jr z, cheat_pressed
	xor a	
	ret			; carry flag is cleared
cheat_pressed:
	ld a, (inertia_cheat)
	xor 1
	ld (inertia_cheat), a
	ld a, 1
	ld (key_i_pressed), a
	scf
	ret
no_cheat_pressed:
	ld a, (key_i_pressed)
	and a
	ret z	; key was already released, and carry flag is now 0
	xor a	; clear carry		
	ld (key_i_pressed), a
	ret

; Read keyboard to find if we want to change sound
check_sound:
	ld hl, KEY_S
	call get_keyboard
	and a
	jr z, no_sound_pressed
	ld a, (key_s_pressed)
	and a
	jr z, sound_pressed
	xor a
	ret			; carry is cleared already
sound_pressed:
	ld a, (sound_selection)
	inc a
	and 3
	ld (sound_selection), a
	ld a, 1
	ld (key_s_pressed), a
	scf
	ret
no_sound_pressed:
	ld a, (key_s_pressed)
	and a
	ret z	; key was already released, and carry flag is now 0
	xor a	; clear carry		
	ld (key_s_pressed), a
	ret


; Check if any of the keys 1..7 where pressed, to enable cheating to that level
check_levelcheat:
	ld hl, KEY_1
	call get_keyboard
	and a
	jr z, no_1_pressed
yes_1_pressed:
	ld hl, KEY_1
	call get_keyboard
	and a
	jr nz, yes_1_pressed
	xor a
	ld (starting_level), a
	xor a
	ret
no_1_pressed:
	ld hl, KEY_2
	call get_keyboard
	and a
	jr z, no_2_pressed
yes_2_pressed:
	ld hl, KEY_2
	call get_keyboard
	and a
	jr nz, yes_2_pressed
	ld a, 1
	ld (starting_level), a
	xor a
	ret
no_2_pressed:
	ld hl, KEY_3
	call get_keyboard
	and a
	jr z, no_3_pressed
yes_3_pressed:
	ld hl, KEY_3
	call get_keyboard
	and a
	jr nz, yes_3_pressed
	ld a, 2
	ld (starting_level), a
	xor a
	ret
no_3_pressed:
	ld hl, KEY_4
	call get_keyboard
	and a
	jr z, no_4_pressed
yes_4_pressed:
	ld hl, KEY_4
	call get_keyboard
	and a
	jr nz, yes_4_pressed
	ld a, 3
	ld (starting_level), a
	xor a
	ret
no_4_pressed:
	ld hl, KEY_5
	call get_keyboard
	and a
	jr z, no_5_pressed
yes_5_pressed:
	ld hl, KEY_5
	call get_keyboard
	and a
	jr nz, yes_5_pressed
	ld a, 4
	ld (starting_level), a
	xor a
	ret
no_5_pressed:
	ld hl, KEY_6
	call get_keyboard
	and a
	jr z, no_6_pressed
yes_6_pressed:
	ld hl, KEY_6
	call get_keyboard
	and a
	jr nz, yes_6_pressed

	ld a, 5
	ld (starting_level), a
	xor a
	ret
no_6_pressed:
	ld hl, KEY_7
	call get_keyboard
	and a
	jr z, no_7_pressed
yes_7_pressed:
	ld hl, KEY_7
	call get_keyboard
	and a
	jr nz, yes_7_pressed

	ld a, 6
	ld (starting_level), a
	xor a
	ret
no_7_pressed:
	scf
	ret


check_jukebox:
	ld hl, KEY_J	
	call get_keyboard
	and a
	ret z
	; if Z was pressed, go to jukebox
	call go_jukebox
	ld a, 255
	ld (timer), a		; when coming back from the jukebox, force a new screen reset
	ret

; Display jukebox and loop through it


; Print string on screen
; INPUT:
;		B: position in X (chars)
;		C: position in Y (chars)
;		IX: pointer to string, terminated by 0
TEXTO0:			DB	92,91,"GENESIS",91,"JUKEBOX",91,92,0
TEXTO1:			DB	91,91,91,"MUSIC",91,"BY",91,"WYZ",0

TEXTO2:			DB	"A",91,92,91,"PARASOL",91,91,91,91,"B",91,92,91,"FIN",0
TEXTO3:			DB	"C",91,92,91,"JEFF",91,91,91,91,91,91,91,"D",91,92,91,"MICROINT",0
TEXTO4:			DB	"E",91,92,91,"LINE",91,91,91,91,91,91,91,"F",91,92,91,"TOWN",0
TEXTO5:			DB	"G",91,92,91,"COMPOTE",91,91,91,91,"H",91,92,91,"WORDS",0
TEXTO6:			DB	"I",91,92,91,"ALICE",91,91,91,91,91,91,"J",91,92,91,"GANGWAY",0
TEXTO7:			DB	"K",91,92,91,"HOMAGE",91,91,91,91,91,"L",91,92,91,"HOC",0
TEXTO8:			DB	"M",91,92,91,"WARRIOR",91,91,91,91,"N",91,92,91,"EQUINOX",91,"V",0
TEXTO9:			DB	"PRESS",91,"X",91,"TO",91,"EXIT",0

juk_keylist: DW KEY_A,KEY_B,KEY_C,KEY_D,KEY_E,KEY_F,KEY_G,KEY_H,KEY_I,KEY_J,KEY_K,KEY_L,KEY_M,KEY_N
juk_songlist: DB 0,2,4,11,5,13,6,12,7,1,8,14,3,9


go_jukebox:
	xor a
	ld (anim_frame), a
	call DrawStarAnim
	xor a
	ld de, $0
	ld bc, $1800
	call FILLVRM
	ld a, $f0
	ld de, $2000
	ld bc, $1800
	call FILLVRM

	ld b, 6
	ld c, 1
	ld ix, TEXTO0
	call PrintString	
	ld b, 6
	ld c, 3
	ld ix, TEXTO1
	call PrintString
	ld b, 2
	ld c, 8
	ld ix, TEXTO2
	call PrintString	
	ld b, 2
	ld c, 10
	ld ix, TEXTO3
	call PrintString
	ld b, 2
	ld c, 12
	ld ix, TEXTO4
	call PrintString
	ld b, 2
	ld c, 14
	ld ix, TEXTO5
	call PrintString
	ld b, 2
	ld c, 16
	ld ix, TEXTO6
	call PrintString	
	ld b, 2
	ld c, 18
	ld ix, TEXTO7
	call PrintString
	ld b, 2
	ld c, 20
	ld ix, TEXTO8
	call PrintString
	ld b, 8
	ld c, 23
	ld ix, TEXTO9
	call PrintString
jukebox_loop:
	ld hl, KEY_X
	call get_keyboard
	and a
	ret nz

menu_check_level:
	call check_levelcheat
	jr c, jukebox_nolevelcheat
	ld hl, FX_MENU_INERTIA
	push hl
        call _wyz_effect
        pop hl

jukebox_nolevelcheat:
	ld b, 0
	ld de, juk_keylist
jukebox_keyloop:
	ld a, (de)
	inc de
	ld l, a
	ld a, (de)
	inc de
	ld h, a		; get key to check
	push hl
	push bc
	call get_keyboard
	pop bc
	pop hl
	and a
	jr nz, jukebox_playsong	
	inc b
	ld a,b
	cp 14
	jr nz, jukebox_keyloop
	jr jukebox_loop
	ret
jukebox_playsong:
	push hl
	push bc
	call get_keyboard
	pop bc
	pop hl
	and a
	jr nz, jukebox_playsong	;wait for key to be released
	ld hl, juk_songlist
	ld c, b
	ld b, 0
	add hl, bc
	ld a, (hl)
	ld l, a
	ld h, 0
	push hl
        call _wyz_load_music
        pop hl
	;  And screens 1 bank 11 at $A000	
	ld a, 11
	call setROM3
	jr jukebox_loop


; INPUT:  DE: new potential hi score
check_hiscore:
	ld hl, hiscore_values
	ld a, 6
loop_hiscores:
	ld c, (hl)
	inc hl
	ld b, (hl)
	inc hl
	ex de, hl		; HL now has the high score, and DE the pointer
	push hl
	push de
	sbc hl, bc		; if bc is higher than hl, no new hi score
	pop hl
	pop de			; we POP in reverse order!
	jr nc, hiscore_found
	dec a
	jr nz, loop_hiscores
nohiscore_found:
	ret			; no new high score, just exit
hiscore_found:
	; in this case, we found the high score. We have to push the high scores down
	dec a
	jr z, hiscore_last ; A -1 scores we need to move
	push af
	push de
	add a, a  	     ; do it twice (words)
	ld b, a
	ld hl, hiscore_values+11
	ld de, hiscore_values+9
pushscore_loop:
	ld a, (de)
	ld (hl), a
	dec hl
	dec de
	djnz pushscore_loop	
	pop de
	ld (hl), d
	dec hl
	ld (hl), e	    ; and put the new high score
	pop af		    ; get again the number of scores to move
	push de 
	ld b, a
	add a, a	    ; A * 2
	add a, a	    ; A * 4
	add a, a	    ; A * 8
	add a, b	    ; A * 9 (9 bytes per name)
	ld de, hiscore_names + 53
	ld hl, hiscore_names + 44
	ld c, a
	ld b, 0
	lddr		    ; copy down all names	
	pop de
	jr hiscore_pushed
hiscore_last:
	ld hl, hiscore_values+11
	ld (hl), d
	dec hl
	ld (hl), e
hiscore_pushed:
	; now go backwards in the name list, and find the first score equal to the high score
	; DE still has the high score
	ld ix, hiscore_values+11
	ld iy, hiscore_names+45
	ld b, 5
findname_loop:
	ld a, (ix)
	dec ix
	ld h, a
	ld a, (ix)
	dec ix
	ld l, a
	xor a
	sbc hl, de
	jr z, foundname
	dec iy
	dec iy
	dec iy
	dec iy
	dec iy
	dec iy
	dec iy
	dec iy
	dec iy
	djnz findname_loop
foundname:
	ld (iy+0), 92
	ld (iy+1), 91
	ld (iy+2), 91
	ld (iy+3), 91
	ld (iy+4), 91
	ld (iy+5), 91
	ld (iy+6), 91
	ld (iy+7), 91
	push iy
	call showhiscores
	call menu_display_inertia_status
	pop hl
	ld b, 0				; B will be the counter for the current position
readloop:
	push bc
	push hl
	call SCAN_KEYBOARD		; read keyboard in A
	pop hl
	pop bc
	cp  13				; 13 is ENTER
	jr nz, read_noenter
	ld a, (hl)
	cp 92
	jr nz, read_exit				; do nothing if we are in the last char
	ld (hl), 91			; else, substitute the cursor by a space
read_exit:
        ; Play sound
	ld hl,  FX_START_GAME
	push hl
        call _wyz_effect
        pop hl
	ret
read_noenter:
	cp 'a'
	jr c, read_checkzero			; if less than a, check for zero (delete)
	cp 'z' + 1
	jr nc, read_checkzero		; if higher than z, check for delete
	ld (hl), a			; store the new key press
	ld a, b
	cp 7
	jr z, read_continue		; don't go beyond 8 characters
	inc hl
	ld (hl),92
	inc b

	push bc
        push hl
	ld hl, FX_MENU_INERTIA
	push hl
        call _wyz_effect
        pop hl
        pop hl
        pop bc

	jr read_continue
read_checkzero:
	cp 0
	jr z, read_deletepressed
	jr readloop
read_deletepressed:
	ld a, b
	and a
	jr z, readloop
	dec b
	ld (hl), 91
	dec hl
	ld (hl), 92
read_continue:
	push bc
	push hl
	call showhiscores_loop
	pop hl
	pop bc
	jr readloop
	ret


; Scan the keyboard to find a single keypress
; Input: n/a
; Output: key scan code, in A
; Will block until the key is pressed
BIOSkeys EQU $FBE5

KeyCodes:
   defb '0','1','2','3','4','5','6','7'
   defb '8','9','-','=','\','[',']',';'
   defb '"','~',',','.','/',255,'a','b'
   defb 'c','d','e','f','g','h','i','j'
   defb 'k','l','m','n','o','p','q','r'
   defb 's','t','u','v','w','x','y','z'
   defb 255,255,255,255,255,255,255,255 ; F3  F2  F1  CODE  CAPS  GRAPH CTRL  SHIFT
   defb 255,255,255,255,255, 0, 255,13; RET  SELECT  BS  STOP  TAB  ESC  F5  F4 (inverted)
   defb 255,255,255,0, 255,255,255,255 ; →  ↓  ↑  ←  DEL  INS  HOME  SPACE (inverted)
   defb 255,255,255,255,255,255,255,255 ; NUM4  NUM3  NUM2  NUM1  NUM0 NUM/  NUM+  NUM*
   defb 255,255,255,255,255,255,255,255 ; NUM.  NUM,  NUM-  NUM9  NUM8 NUM7  NUM6  NUM5

; Read a keyboard row
; INPUT  -> B: row
; OUTPUT -> A: value read from PPI
 
readkbrow:
	in a, ($AA)
	and $F0		;only change bits 0-3
	or b		;take row number from B
	out ($AA), a	
	in a, ($A9)	;read row into A
	ret
   
SCAN_KEYBOARD:
        ld b, 0;	; read 10 lines
	ld ix, KeyCodes
scan_loop:
	call readkbrow

	bit 0, a
	jr z, bitzero
	bit 1, a
	jr z, bitone
	bit 2, a
	jr z, bittwo
	bit 3, a
	jr z, bitthree
	bit 4, a
	jr z, bitfour
	bit 5, a
	jr z, bitfive
	bit 6, a
	jr z, bitsix
	bit 7, a
	jr z, bitseven
	; no key pressed here, go next
	ld de, 8
	add ix, de		; next line of keys
	inc b
	ld a, b
	cp 10
	jr c, scan_loop
	; block until a key is pressed, get back to scan_keyboard!
	jp SCAN_KEYBOARD

bitzero:
	ld a, (ix+0) ; This is the scan code, now wait for the key release
	push af
wait0release:
	call readkbrow
	bit 0, a
	jr z, wait0release
	pop af
	ret
bitone:
	ld a, (ix+1) ; This is the scan code, now wait for the key release
	push af
wait1release:
	call readkbrow
	bit 1, a
	jr z, wait1release
	pop af
	ret
bittwo:
	ld a, (ix+2) ; This is the scan code, now wait for the key release
	push af
wait2release:
	call readkbrow
	bit 2, a
	jr z, wait2release
	pop af
	ret
bitthree:
	ld a, (ix+3) ; This is the scan code, now wait for the key release
	push af
wait3release:
	call readkbrow
	bit 3, a
	jr z, wait3release
	pop af
	ret
bitfour:
	ld a, (ix+4) ; This is the scan code, now wait for the key release
	push af
wait4release:
	call readkbrow
	bit 4, a
	jr z, wait4release
	pop af
	ret
bitfive:
	ld a, (ix+5) ; This is the scan code, now wait for the key release
	push af
wait5release:
	call readkbrow
	bit 5, a
	jr z, wait5release
	pop af
	ret
bitsix:
	ld a, (ix+6) ; This is the scan code, now wait for the key release
	push af
wait6release:
	call readkbrow
	bit 6, a
	jr z, wait6release
	pop af
	ret
bitseven:
	ld a, (ix+7) ; This is the scan code, now wait for the key release
	push af
wait7release:
	call readkbrow
	bit 7, a
	jr z, wait7release
	pop af
	ret
	




; aPPack decompressor
; original source by dwedit
; very slightly adapted by utopian
; optimized by Metalbrain

;hl = source
;de = dest

depack:		ld	ixl,128
apbranch1:	ldi
aploop0:	ld	ixh,1		;LWM = 0
aploop:		call 	ap_getbit
		jr 	nc,apbranch1
		call 	ap_getbit
		jr 	nc,apbranch2
		ld 	b,0
		call 	ap_getbit
		jr 	nc,apbranch3
		ld	c,16		;get an offset
apget4bits:	call 	ap_getbit
		rl 	c
		jr	nc,apget4bits
		jr 	nz,apbranch4
		ld 	a,b
apwritebyte:	ld 	(de),a		;write a 0
		inc 	de
		jr	aploop0
apbranch4:	and	a
		ex 	de,hl 		;write a previous byte (1-15 away from dest)
		sbc 	hl,bc
		ld 	a,(hl)
		add	hl,bc
		ex 	de,hl
		jr	apwritebyte
apbranch3:	ld 	c,(hl)		;use 7 bit offset, length = 2 or 3
		inc 	hl
		rr 	c
		ret 	z		;if a zero is encountered here, it is EOF
		ld	a,2
		adc	a,b
		push 	hl
		ld	iyh,b
		ld	iyl,c
		ld 	h,d
		ld 	l,e
		sbc 	hl,bc
		ld 	c,a
		jr	ap_finishup2
apbranch2:	call 	ap_getgamma	;use a gamma code * 256 for offset, another gamma code for length
		dec 	c
		ld	a,c
		sub	ixh
		jr 	z,ap_r0_gamma		;if gamma code is 2, use old r0 offset,
		dec 	a
		;do I even need this code?
		;bc=bc*256+(hl), lazy 16bit way
		ld 	b,a
		ld 	c,(hl)
		inc 	hl
		ld	iyh,b
		ld	iyl,c

		push 	bc
		
		call 	ap_getgamma

		ex 	(sp),hl		;bc = len, hl=offs
		push 	de
		ex 	de,hl

		ld	a,4
		cp	d
		jr 	nc,apskip2
		inc 	bc
		or	a
apskip2:	ld 	hl,127
		sbc 	hl,de
		jr 	c,apskip3
		inc 	bc
		inc 	bc
apskip3:	pop 	hl		;bc = len, de = offs, hl=junk
		push 	hl
		or 	a
ap_finishup:	sbc 	hl,de
		pop 	de		;hl=dest-offs, bc=len, de = dest
ap_finishup2:	ldir
		pop 	hl
		ld	ixh,b
		jr 	aploop

ap_r0_gamma:	call 	ap_getgamma		;and a new gamma code for length
		push 	hl
		push 	de
		ex	de,hl

		ld	d,iyh
		ld	e,iyl
		jr 	ap_finishup


ap_getbit:	ld	a,ixl
		add	a,a
		ld	ixl,a
		ret	nz
		ld	a,(hl)
		inc	hl
		rla
		ld	ixl,a
		ret

ap_getgamma:	ld 	bc,1
ap_getgammaloop: call 	ap_getbit
		rl 	c
		rl 	b
		call 	ap_getbit
		jr 	c,ap_getgammaloop
		ret

; Blatant copy from the z88dk RAND routine
; by Risto Jrvinen
; originally published with the Clarified Artistic License,
; which I assume is compatible with ASL 2.0

rand:
   ld   hl,(rand_seed)
   ld   a,h
   add  a,a                     ;Set highest bit of seed to carry
   rl   l                       ;rotate L left (C<=L<=C)
   rl   h                       ;rotate H left (C<=L<=C)
   add  a,a                     ;Set second highest bit of seed to carry
   rl   l
   rl   h
   add  a,a
   rl   l
   rl   h
   ld   bc,$7415
   add  hl,bc                   ;Add $7415 to HL
   ld   (rand_seed),hl
   res     7,h                  ;force to be +ve
   ret


string_on db "n",91,0
string_off db "ff",0

string_silence		db 91,91,"silence",91,91,91,0
string_music_and_fx	db "music",91,"and",91,"fx",0
string_music		db 91,"music",91,"only",91,0
string_fx		db 91,91,"fx",91,"only",91,91,91,0

; Character font
charfont:
	DEFB	  0,127,  1,127, 65,127,  0,  0
	DEFB	 64,127, 65, 65, 65,127,  0,  0
	DEFB	  0,127, 64, 64, 64,127,  0,  0
	DEFB	  1,127, 65, 65, 65,127,  0,  0
	DEFB	  0,127, 65,127, 64,127,  0,  0
	DEFB	  0,127, 64,127, 64, 64,  0,  0
	DEFB	  0,127, 65, 65,127,  1,127,  0
	DEFB	 64,127, 65, 65, 65, 65,  0,  0
	DEFB	  0,  8,  8,  8,  8,  8,  0,  0
	DEFB	  0,  2,  2,  2,  2,  2,126,  0
	DEFB	  0, 65, 65,126, 65, 65,  0,  0
	DEFB	  0, 64, 64, 64, 64,127,  0,  0
	DEFB	  0,127, 73, 73, 73, 73,  0,  0
	DEFB	  0,127, 65, 65, 65, 65,  0,  0
	DEFB	  0,127, 65, 65, 65,127,  0,  0
	DEFB	  0,127, 65, 65, 65,127, 64,  0
	DEFB	  0,127, 65, 65, 65,127,  1,  0

	DEFB	  0,127, 64, 64, 64, 64,  0,  0
	DEFB	  0,127, 64,127,  1,127,  0,  0
	DEFB	 64,126, 64, 64, 64,127,  0,  0
	DEFB	  0, 65, 65, 65, 65,127,  0,  0
	DEFB	  0, 65, 65, 34, 20,  8,  0,  0
	DEFB	  0, 73, 73, 73, 73,127,  0,  0
	DEFB	  0, 65, 34, 28, 34, 65,  0,  0
	DEFB	  0, 65, 65, 65,127,  1,127,  0
	DEFB	  0,127,  3, 28, 96,127,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0		; ARTIFICIAL char for the space, number 91
	DEFB	$7E,$7E,$7E,$7E,$7E,$7E,$7E,$7E		; ARTIFICIAL char for the cursor, number 92
	DEFB	 60, 98, 98,126, 98, 98, 98,  0		; inverted A, number 93
	DEFB	124,  6,  6, 62,  6,  6,  6,  0		; inverted F, number 94
	DEFB	 60, 98, 98, 98, 98, 98, 60,  0		; inverted O, number 95
	DEFB	124, 64, 64, 64, 70, 70,124,  0		; inverted J, number 96

; Number font
numfont:
	DEFB	  0,127, 65, 73, 65,127,  0,  0
	DEFB	  0, 24,  8,  8,  8,  8,  0,  0
	DEFB	  0,127,  1,127, 64,127,  0,  0
	DEFB	  0,127,  1,127,  1,127,  0,  0
	DEFB	  0, 65, 65,127,  1,  1,  0,  0
	DEFB	  0,127, 64,127,  1,127,  0,  0
	DEFB	  0,127, 64,127, 65,127,  0,  0
	DEFB	  0,127,  1,  1,  1,  1,  0,  0
	DEFB	  0,127, 65,127, 65,127,  0,  0
	DEFB	  0,127, 65,127,  1,127,  0,  0

star_anim:
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0		; first frame
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0

	DEFB	  0,  0,  0,  0,  0,  0,  0,  0		; second frame
	DEFB	  0,  0,  0,  1,  0,  1,  1, 22
;	DEFB	  1,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,128

	DEFB	  0,  0,  0,  0,  0,  0,  0,  0		; third frame
	DEFB	  0,  0,  0,  1,  8,  5,  3, 22
;	DEFB	  1,  1,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,192




	DEFB	  0,  0,  0,  0,  0,  0,  0,  1		; fourth frame	;1
	DEFB	128,  0, 32,  1,  8,  5,  3,150				;2
;	DEFB	  1,  1,  0,  1,  0,  0,  0,  0				;3
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0				;3
	DEFB	  0,  0,  0,  0,  0,  0,  0,208				;4









anim_order: 	db	0,0,0,0,0,0,0,0
	    	db	1,1,1,1,2,2,2,2
		db 	2,2,3,3,3,3,3,3
		db	3,3,3,3,3,3,3,3
		db	3,3,3,3,3,3,3,3
		db	2,2,2,2,2,2,1,1
		db	1,1,0,0,0,0,0,0
		db	0,0,0,0,0,0,0,0
		db	0,0,0,0,0,0,0,0



credits_bkg_color EQU $AF54
credits_bkg_pattern EQU $A607
genesis_title_color EQU $A3CD
genesis_title_pattern EQU $A000
hiscores_color	EQU $B775
hiscores_pattern EQU $B5E3

starsx_medium 	EQU $C400
starsx_fast	EQU $C400 + NSTARS
starsy_medium 	EQU $C400 + NSTARS*2
starsy_fast	EQU $C400 + NSTARS*3
stars_pixels_medium EQU $C400 + NSTARS*4
stars_pixels_fast   EQU $C400 + NSTARS*5		; $C520
rand_seed	EQU $C520				; $C522
starfield_variable EQU $C522				; $C523
screen_to_show 	EQU $c523				; $C524 0: title screen; 1: hi scores; 2: credits
timer 		EQU $C524				; $C525
inertia_cheat	EQU $C525				; $C526
sound_selection EQU $C526				; $C527
hiscore_string  EQU $C527				; $C52C
anim_frame 	EQU $C52C				; $C52D
starting_level	EQU $C52D				; $C52D
selected_joystick EQU $C080				; $C52E		
fire_pressed_wait EQU $C52E				; $C52F
key_i_pressed	EQU $C52F				; $C530
key_s_pressed	EQU $C530				; $C531
hiscore_names   EQU $C531				; $C567
hiscore_values  EQU $C567				; $C573
