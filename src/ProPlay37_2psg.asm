;   Copyright 2012 Jos� Vicente Mas�
;
;   Licensed under the Apache License, Version 2.0 (the "License");
;   you may not use this file except in compliance with the License.
;   You may obtain a copy of the License at
;
;       http://www.apache.org/licenses/LICENSE-2.0
;
;   Unless required by applicable law or agreed to in writing, software
;   distributed under the License is distributed on an "AS IS" BASIS,
;   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;   See the License for the specific language governing permissions and
;   limitations under the License.


org $8000
                

PLAYER_INIT:
; AJUSTES INICIALES
		CALL PLAYER_OFF

; MUSICA DATOS INICIALES

                LD      HL,BUFFER_DEC        	;* RESERVAR MEMORIA PARA BUFFER DE SONIDO!!!!!
                LD      [CANAL_A],HL
                
                LD      HL,BUFFER_DEC+$10        	
                LD      [CANAL_B],HL       	

                LD      HL,BUFFER_DEC+$20       	
                LD      [CANAL_C],HL 

                LD      HL,BUFFER_DEC+$30       	
                LD      [CANAL_P],HL 
		ret


;___________________________________________________________

INICIO:		LD   A, (SOUND_SFX)
		AND  2
		JR   Z, NO_EFECTOS
		;CALL    REPRODUCE_EFECTO
NO_EFECTOS:
		CALL    ROUT
                CALL    PLAY	
  		RET
      

;______________________________________________________________


;FADE_OUT 100% A 0%
;IN [A]:CADENCIA

START_FADE_OUT:	LD	[DECAY],A
		LD	[DECAY_TEMP],A
		XOR	A
		LD	[FADE_METER],A
		LD	HL,FADE
		SET	0,[HL]
		RET
		
FADE_OUT:	LD	HL,DECAY_TEMP
		DEC	[HL]
		JR	NZ,FADEOJP0
		LD	A,[DECAY]
		LD	[HL],A
		LD	A,[FADE_METER]
		CP	$10			;FADE OUT COMPLETO
		JP	Z,PLAYER_OFF
		INC	A
		LD	[FADE_METER],A
FADEOJP0:	LD	A,[FADE_METER]
		LD	C,A
		LD	HL,PSG_REG+8
		LD	B,3 
FADEOBC0:	LD	A,[HL]
		SUB	C
		JR	NC,@@NO_RESET_VOL
		XOR	A
@@NO_RESET_VOL:	LD	[HL],A
		INC	HL
		DJNZ	FADEOBC0

		LD	HL,PSG_REG_EXT+8
		LD	B,3 
FADEOBC0EXT:	LD	A,[HL]
		SUB	C
		JR	NC,@@NO_RESET_VOL2
		XOR	A
@@NO_RESET_VOL2:	LD	[HL],A
		INC	HL
		DJNZ	FADEOBC0EXT

		RET






PLAYER_OFF:	LD	A,[INTERR]
		AND	00001000B			;***** IMPORTANTE SI NO HAY MUSICA ****
		LD	[INTERR],A
	
		XOR	A			;FINALIZA FADE OUT
		LD	[FADE],A

		LD	HL,PSG_REG
		LD	DE,PSG_REG+1
		LD	BC,14
		LD	[HL],A
		LDIR

		LD	HL,PSG_REG_EXT
		LD	DE,PSG_REG_EXT+1
		LD	BC,14
		LD	[HL],A
		LDIR

	
		LD      A,10111000B		; **** POR SI ACASO ****
		LD      [PSG_REG+7],A
	        LD      [PSG_REG_EXT+7],A   
		CALL	ROUT
		LD   A,$0D         ;APAGADO FORZADO DE LAS ENVOLVENTES 01
      		OUT     [$A0],A
                XOR   A
                OUT   [$A1],A
                
      		LD   A,$0D
      		OUT     [$10],A
                XOR   A
                OUT   [$11],A 

		CALL	FIN_SONIDO
		RET


;INICIA EL SONIDO N� [A]

INICIA_SONIDO:  LD      HL,TABLA_SONIDOS
                CALL    EXT_WORD
                LD      [PUNTERO_SONIDO],HL
                LD      HL,INTERR
                SET     2,[HL]
                RET


;CARGA UNA CANCION
;IN:[A]=N� DE CANCION

CARGA_CANCION:  
		LD      HL,SONG
                LD      [HL],A          ;N� A
        

;DECODIFICAR
;IN-> INTERR 0 ON
;     SONG

;CARGA CANCION SI/NO

DECODE_SONG:    LD      A,[SONG]

;LEE CABECERA DE LA CANCION
;BYTE 0=TEMPO

                LD      HL,TABLA_SONG
                CALL    EXT_WORD                              
                LD      A,[HL]
                LD      [TEMPO],A
		XOR	A
		LD	[TTEMPO],A
                
;N� DE PAUTA DEL CANAL A,B,C    **** OBSOLETO ****

                ;LD      IX,PUNTERO_P_A
                ;CALL    SET_PAUTA
                ;LD      IX,PUNTERO_P_B
                ;CALL    SET_PAUTA
                ;LD      IX,PUNTERO_P_C
                ;CALL    SET_PAUTA

;HEADER BYTE 1
;[-|-|-|-|-|-|-|LOOP]
                INC	HL		;LOOP 1=ON/0=OFF?
                LD	A,[HL]
                BIT	0,A
                JR	Z,NPTJP0
                PUSH	HL
                LD	HL,INTERR
                SET	4,[HL]
                POP	HL
             
                
NPTJP0:         INC	HL		;2 BYTES RESERVADOS
                INC	HL
                INC	HL

;BUSCA Y GUARDA INICIO DE LOS CANALES EN EL MODULO MUS

		
		LD	[PUNTERO_P_DECA],HL
		LD	E,$3F			;CODIGO INTRUMENTO 0
		LD	B,E
BGICMODBC1:	XOR	A			;BUSCA EL BYTE 0
		CPIR
		DEC	HL
		DEC	HL
		LD	A,E			;ES EL INSTRUMENTO 0??
		CP	[HL]
		INC	HL
		INC	HL
		JR	Z,BGICMODBC1

		LD	[PUNTERO_P_DECB],HL

BGICMODBC2:	XOR	A			;BUSCA EL BYTE 0
		CPIR
		DEC	HL
		DEC	HL
		LD	A,E
		CP	[HL]			;ES EL INSTRUMENTO 0??
		INC	HL
		INC	HL
		JR	Z,BGICMODBC2

		LD	[PUNTERO_P_DECC],HL
		
BGICMODBC3:	XOR	A			;BUSCA EL BYTE 0
		CPIR
		DEC	HL
		DEC	HL
		LD	A,E
		CP	[HL]			;ES EL INSTRUMENTO 0??
		INC	HL
		INC	HL
		JR	Z,BGICMODBC3
		LD	[PUNTERO_P_DECP],HL
		
                
;LEE DATOS DE LAS NOTAS
;[|][|||||] LONGITUD\NOTA

INIT_DECODER:   LD      DE,[CANAL_A]
                LD      [PUNTERO_A],DE
                LD	HL,[PUNTERO_P_DECA]
                CALL    DECODE_CANAL    ;CANAL A
                LD	[PUNTERO_DECA],HL
                
                LD      DE,[CANAL_B]
                LD      [PUNTERO_B],DE
                LD	HL,[PUNTERO_P_DECB]
                CALL    DECODE_CANAL    ;CANAL B
                LD	[PUNTERO_DECB],HL
                
                LD      DE,[CANAL_C]
                LD      [PUNTERO_C],DE
                LD	HL,[PUNTERO_P_DECC]
                CALL    DECODE_CANAL    ;CANAL C
                LD	[PUNTERO_DECC],HL
                
                LD      DE,[CANAL_P]
                LD      [PUNTERO_P],DE
                LD	HL,[PUNTERO_P_DECP]
                CALL    DECODE_CANAL    ;CANAL P
                LD	[PUNTERO_DECP],HL
 
PLAYER_ON:	LD      HL,INTERR       
                SET     1,[HL]          ;REPRODUCE CANCION
                RET



;INICIA PAUTA PARA UN CANAL
;IN [A]:  N� DE PAUTA
;   [IX]: PUNTERO PAUTA

;SET_PAUTA:      INC     HL
;                LD      A,[HL]
;                PUSH    HL
;                LD      HL,TABLA_PAUTAS
;                CALL    EXT_WORD
;                LD      [IX+0],L
;                LD      [IX+1],H
;                LD      [IX+6],L
;                LD      [IX+7],H
;                POP     HL
;                RET                

;DECODIFICA NOTAS DE UN CANAL
;IN [DE]=DIRECCION DESTINO
;NOTA=0 FIN CANAL
;NOTA=1 SILENCIO
;NOTA=2 PUNTILLO
;NOTA=3 COMANDO I

DECODE_CANAL:   LD      A,[HL]
                AND     A               ;FIN DEL CANAL?
                JR      Z,FIN_DEC_CANAL
                CALL    GETLEN

                CP      00000001B       ;ES SILENCIO?
                JR      NZ,NO_SILENCIO
                SET     6,A
                JR      NO_MODIFICA
                
NO_SILENCIO:    CP      00111110B       ;ES PUNTILLO?
                JR      NZ,NO_PUNTILLO
                OR      A
                RRC     B
                XOR     A
                JR      NO_MODIFICA

NO_PUNTILLO:    CP      00111111B       ;ES COMANDO?
                JR      NZ,NO_MODIFICA
                BIT     0,B             ;COMADO=INSTRUMENTO?
                JR      Z,NO_INSTRUMENTO   
                LD      A,11000001B     ;CODIGO DE INSTRUMENTO      
                LD      [DE],A
                INC     HL
                INC     DE
                LD      A,[HL]          ;N� DE INSTRUMENTO
                LD      [DE],A
                INC     DE
                INC	HL
                JR      DECODE_CANAL
                
NO_INSTRUMENTO: BIT     2,B
                JR      Z,NO_ENVOLVENTE
                LD      A,11000100B     ;CODIGO ENVOLVENTE
                LD      [DE],A
                INC     DE
                INC	HL
                JR      DECODE_CANAL
     
NO_ENVOLVENTE:  BIT     1,B
                JR      Z,NO_MODIFICA           
                LD      A,11000010B     ;CODIGO EFECTO
                LD      [DE],A                  
                INC     HL                      
                INC     DE                      
                LD      A,[HL]                  
                CALL    GETLEN   
                
NO_MODIFICA:    LD      [DE],A
                INC     DE
                XOR     A
                DJNZ    NO_MODIFICA
		SET     7,A
		SET 	0,A
                LD      [DE],A
                INC     DE
                INC	HL
                RET			;** JR      DECODE_CANAL
                
FIN_DEC_CANAL:  SET     7,A
                LD      [DE],A
                INC     DE
                RET

GETLEN:         LD      B,A
                AND     00111111B
                PUSH    AF
                LD      A,B
                AND     11000000B
                RLCA
                RLCA
                INC     A
                LD      B,A
                LD      A,10000000B
DCBC0:          RLCA
                DJNZ    DCBC0
                LD      B,A
                POP     AF
                RET
                
                

        
                
;PLAY __________________________________________________


PLAY:          	LD      HL,INTERR       ;PLAY BIT 1 ON?
                BIT     1,[HL]
                RET     Z
;TEMPO          
                LD      HL,TTEMPO       ;CONTADOR TEMPO
                INC     [HL]
                LD      A,[TEMPO]
                CP      [HL]
                JR      NZ,PAUTAS
                LD      [HL],0
                
;INTERPRETA      
                LD      IY,PSG_REG
                LD      IX,PUNTERO_A
                LD      BC,PSG_REG+8
                CALL    LOCALIZA_NOTA
                LD      IY,PSG_REG+2
                LD      IX,PUNTERO_B
                LD      BC,PSG_REG+9
                CALL    LOCALIZA_NOTA
                LD      IY,PSG_REG+4
                LD      IX,PUNTERO_C
                LD      BC,PSG_REG+10
                CALL    LOCALIZA_NOTA
                LD      IX,PUNTERO_P    ;EL CANAL DE EFECTOS ENMASCARA OTRO CANAL
                CALL    LOCALIZA_EFECTO              

;PAUTAS 
                
PAUTAS:         LD      IY,PSG_REG+0
                LD      IX,PUNTERO_P_A
                LD      HL,PSG_REG+8
                CALL    PAUTA           ;PAUTA CANAL A
                LD      IY,PSG_REG+2
                LD      IX,PUNTERO_P_B
                LD      HL,PSG_REG+9
                CALL    PAUTA           ;PAUTA CANAL B
                LD      IY,PSG_REG+4
                LD      IX,PUNTERO_P_C
                LD      HL,PSG_REG+10
                CALL    PAUTA           ;PAUTA CANAL C
                

                RET
                


;REPRODUCE EFECTOS DE SONIDO 

REPRODUCE_SONIDO:

		LD      HL,INTERR   
                BIT     2,[HL]          ;ESTA ACTIVADO EL EFECTO?
                RET     Z
                LD      HL,[PUNTERO_SONIDO]
                LD      A,[HL]
                CP      $FF
                JR      Z,FIN_SONIDO
                LD      [PSG_REG_EXT+0],A
                INC     HL
                LD      A,[HL]
                RRCA
                RRCA
                RRCA
                RRCA
                AND     00001111B
                LD      [PSG_REG_EXT+1],A
                LD      A,[HL]
                DEC	A
                DEC	A
                
                AND     00001111B

                LD      [PSG_REG_EXT+8],A
                INC     HL
                LD      A,[HL]
                AND     A
                JR      Z,NO_RUIDO
                LD      [PSG_REG_EXT+6],A
                LD      A,10110000B
                JR      SI_RUIDO
NO_RUIDO:       LD      A,10111000B
SI_RUIDO:       LD      [PSG_REG_EXT+7],A
       
                INC     HL
                LD      [PUNTERO_SONIDO],HL
                RET
FIN_SONIDO:     LD      HL,INTERR
                RES     2,[HL]

FIN_NOPLAYER:   ;LD	A,$0E
		;LD	[PSG_REG_SEC+13],A
       		LD      A,10111000B
       		LD      [PSG_REG_EXT+7],A
                RET         
                
;VUELCA BUFFER DE SONIDO AL PSG


ROUT:
		LD	A,[PSG_REG+8]
		LD	[PSG_REG_EXT+8],A
		LD	A,[PSG_REG+9]
		LD	[PSG_REG_EXT+9],A
		LD	A,[PSG_REG+10]
		LD	[PSG_REG_EXT+10],A	
		
		LD	HL,[PSG_REG+0]
		DEC	HL
		LD	[PSG_REG_EXT+0],HL
		LD	HL,[PSG_REG+2]
		DEC	HL
		LD	[PSG_REG_EXT+2],HL
		LD	HL,[PSG_REG+4]
		DEC	HL
		LD	[PSG_REG_EXT+4],HL
		CALL    REPRODUCE_SONIDO
		
		LD	HL,FADE
		BIT	0,[HL]
		CALL	NZ,FADE_OUT					
		CALL	REPRODUCE_EFECTO  ; <--- atencion a esto


		CALL	PSG_INTERNO
		CALL	PSG_EXTERNO
		RET

PSG_INTERNO:	XOR     A
		LD      C,$A0
                LD      HL,PSG_REG_EXT
INLOUT:		OUT     [C],A
                INC     C
                OUTI 
                DEC     C
                INC     A
                CP      13
                JR      NZ,INLOUT
                OUT     [C],A
                LD      A,[HL]
                AND     A
                RET     Z
                INC     C
                OUT     [C],A
               
                XOR     A
                ;LD      [PSG_REG_SEC+13],A
                LD	[PSG_REG_EXT+13],A
                RET

PSG_EXTERNO:	XOR     A
		LD      C,$10
                LD      HL,PSG_REG
EXLOUT:		OUT     [C],A
                INC     C
                OUTI 
                DEC     C
                INC     A
                CP      13
                JR      NZ,EXLOUT
                OUT     [C],A
                LD      A,[HL]
                AND     A
                RET     Z
                INC     C
                OUT     [C],A
                XOR     A
                LD	[PSG_REG+13],A
                RET



;LOCALIZA NOTA CANAL A
;IN [PUNTERO_A]

LOCALIZA_NOTA:  LD      L,[IX+0]       ;HL=[PUNTERO_A_C_B]
                LD      H,[IX+1]
                LD      A,[HL]
                AND     11000000B      ;COMANDO?
                CP      11000000B
                JR      NZ,LNJP0

;BIT[0]=INSTRUMENTO
                
COMANDOS:       LD      A,[HL]
                BIT     0,A             ;INSTRUMENTO
                JR      Z,COM_EFECTO

                INC     HL
                LD      A,[HL]          ;N� DE PAUTA
                INC     HL
                LD      [IX+00],L
                LD      [IX+01],H
                LD      HL,TABLA_PAUTAS
                CALL    EXT_WORD
                LD      [IX+18],L
                LD      [IX+19],H
                LD      [IX+12],L
                LD      [IX+13],H
                LD      L,C
                LD      H,B
                RES     4,[HL]        ;APAGA EFECTO ENVOLVENTE ********** TEMP�RAL
                XOR     A
                LD      [PSG_REG_EXT+13],A
                LD	[PSG_REG+13],A
                JR      LOCALIZA_NOTA

COM_EFECTO:     BIT     1,A             ;EFECTO DE SONIDO
                JR      Z,COM_ENVOLVENTE

                INC     HL
                LD      A,[HL]
                INC     HL
                LD      [IX+00],L
                LD      [IX+01],H
                CALL    INICIA_SONIDO
                RET

COM_ENVOLVENTE: BIT     2,A
                RET     Z               ;IGNORA - ERROR
                
           
                INC     HL
                LD      [IX+00],L
                LD      [IX+01],H
                LD      L,C
                LD      H,B
                LD	[HL],00010000B          ;ENCIENDE EFECTO ENVOLVENTE ********** TEMP�RAL
                JR      LOCALIZA_NOTA
                
              
LNJP0:          LD      A,[HL]
                INC     HL
                BIT     7,A
                JR      Z,NO_FIN_CANAL_A	;
                BIT	0,A
                JR	Z,FIN_CANAL_A


FIN_NOTA_A:	LD      E,[IX+6]
		LD	D,[IX+7]	;PUNTERO BUFFER AL INICIO
		LD	[IX+0],E
		LD	[IX+1],D
		LD	L,[IX+30]	;CARGA PUNTERO DECODER
		LD	H,[IX+31]
		PUSH	BC
                CALL    DECODE_CANAL    ;DECODIFICA CANAL
                POP	BC
                LD	[IX+30],L	;GUARDA PUNTERO DECODER
                LD	[IX+31],H
                JP      LOCALIZA_NOTA
                
FIN_CANAL_A:    LD	HL,INTERR	;LOOP?
                BIT	4,[HL]              
                JR      NZ,FCA_CONT
                CALL	PLAYER_OFF
                RET

FCA_CONT:	LD	L,[IX+24]	;CARGA PUNTERO INICIAL DECODER
		LD	H,[IX+25]
		LD	[IX+30],L
		LD	[IX+31],H
		JR      FIN_NOTA_A
                
NO_FIN_CANAL_A: LD      [IX+0],L        ;[PUNTERO_A_B_C]=HL GUARDA PUNTERO
                LD      [IX+1],H
                AND     A               ;NO REPRODUCE NOTA SI NOTA=0
                JR      Z,FIN_RUTINA
                BIT     6,A             ;SILENCIO?
                JR      Z,NO_SILENCIO_A
                LD	A,[BC]
                AND	00010000B
                JR	NZ,SILENCIO_ENVOLVENTE
                XOR     A
                LD	[BC],A		;RESET VOLUMEN
                LD	[IY+0],A
                LD	[IY+1],A
		RET
		
SILENCIO_ENVOLVENTE:
		LD	A,$FF
                LD	[PSG_REG+11],A
                LD	[PSG_REG+12],A
                
                xor	a
                LD	[PSG_REG+13],A
                               
                LD	[IY+0],A
                LD	[IY+1],A
                RET


NO_SILENCIO_A:  CALL    NOTA            ;REPRODUCE NOTA

                LD      L,[IX+18]       ; HL=[PUNTERO_P_A0] RESETEA PAUTA 
                LD      H,[IX+19]
                LD      [IX+12],L       ;[PUNTERO_P_A]=HL
                LD      [IX+13],H
FIN_RUTINA:     RET


;LOCALIZA EFECTO
;IN HL=[PUNTERO_P]

LOCALIZA_EFECTO:LD      L,[IX+0]       ;HL=[PUNTERO_P]
                LD      H,[IX+1]
                LD      A,[HL]
                CP      11000010B
                JR      NZ,LEJP0

                INC     HL
                LD      A,[HL]
                INC     HL
                LD      [IX+00],L
                LD      [IX+01],H
                CALL    INICIA_SONIDO
                RET
            
              
LEJP0:          INC     HL
                BIT     7,A
                JR      Z,NOFINCANALP	;
                BIT	0,A
                JR	Z,FIN_CANAL_P
FIN_NOTA_P:	LD      DE,[CANAL_P]
		LD	[IX+0],E
		LD	[IX+1],D
		LD	HL,[PUNTERO_DECP]	;CARGA PUNTERO DECODER
		CALL    DECODE_CANAL    	;DECODIFICA CANAL	
                LD	[PUNTERO_DECP],HL	;GUARDA PUNTERO DECODER
                JP      LOCALIZA_EFECTO
                
FIN_CANAL_P:	LD	HL,[PUNTERO_P_DECP]	;CARGA PUNTERO INICIAL DECODER
		LD	[PUNTERO_DECP],HL
		JR      FIN_NOTA_P
                
NOFINCANALP: 	LD      [IX+0],L        ;[PUNTERO_A_B_C]=HL GUARDA PUNTERO
                LD      [IX+1],H
                RET

; PAUTA DE LOS 3 CANALES
; IN:[IX]:PUNTERO DE LA PAUTA
;    [HL]:REGISTRO DE VOLUMEN
;    [IY]:REGISTROS DE FRECUENCIA

; FORMATO PAUTA	
;	    7    6     5     4   3-0                     3-0  
; BYTE 1 [LOOP|OCT-1|OCT+1|SLIDE|VOL] - BYTE 2 [ | | | |PITCH]

PAUTA:          BIT     4,[HL]        ;SI LA ENVOLVENTE ESTA ACTIVADA NO ACTUA PAUTA
                RET     NZ

		LD	A,[IY+0]
		LD	B,[IY+1]
		OR	B
		RET	Z


                PUSH	HL
                ;LD      L,[IX+0]
                ;LD      H,[IX+1]
                		
                ;LD	A,[HL]		;COMPRUEBA SLIDE BIT 4
		;BIT	4,A
		;JR	Z,PCAJP4
		;LD	L,[IY+0]	;FRECUENCIA FINAL
		;LD	H,[IY+1]
		;SBC	HL,DE
		;JR	Z,PCAJP4
		;JR	C,SLIDE_POS
		;EX	DE,HL
		;RRC	D		;/4
		;RR	E
		;RRC	D
		;RR	E


		;ADC	HL,DE
		;LD	[IY+0],L
		;LD	[IY+1],H
SLIDE_POS:		
		;POP	HL
		;RET
                
PCAJP4:         LD      L,[IX+0]
                LD      H,[IX+1]         
		LD	A,[HL]
		
		BIT     7,A		;LOOP / EL RESTO DE BITS NO AFECTAN
                JR      Z,PCAJP0
                AND     00011111B       ;LOOP PAUTA [0,32]X2!!!-> PARA ORNAMENTOS
                RLCA			;X2
                LD      D,0
                LD      E,A
                SBC     HL,DE
                LD      A,[HL]

PCAJP0:		BIT	6,A		;OCTAVA -1
		JR	Z,PCAJP1
		LD	E,[IY+0]
		LD	D,[IY+1]

		AND	A
		RRC	D
		RR	E
		LD	[IY+0],E
		LD	[IY+1],D
		JR	PCAJP2
		
PCAJP1:		BIT	5,A		;OCTAVA +1
		JR	Z,PCAJP2
		LD	E,[IY+0]
		LD	D,[IY+1]

		AND	A
		RLC	E
		RL	D
		LD	[IY+0],E
		LD	[IY+1],D		


PCAJP2:		INC     HL
		PUSH	HL
		LD	E,A
		LD	A,[HL]		;PITCH DE FRECUENCIA
		LD	L,A
		AND	A
		LD	A,E
		JR	Z,ORNMJP1

                LD	A,[IY+0]	;SI LA FRECUENCIA ES 0 NO HAY PITCH
                ADD	A,[IY+1]
                AND	A
                LD	A,E
                JR	Z,ORNMJP1
                

		BIT	7,L
		JR	Z,ORNNEG
		LD	H,$FF
		JR	PCAJP3
ORNNEG:		LD	H,0
		
PCAJP3:		LD	E,[IY+0]
		LD	D,[IY+1]
		ADC	HL,DE
		LD	[IY+0],L
		LD	[IY+1],H
ORNMJP1:	POP	HL
		
		INC	HL
                LD      [IX+0],L
                LD      [IX+1],H
PCAJP5:         POP	HL
                AND	00001111B	;VOLUMEN FINAL
                LD      [HL],A
                RET




;NOTA : REPRODUCE UNA NOTA
;IN [A]=CODIGO DE LA NOTA
;   [IY]=REGISTROS DE FRECUENCIA


NOTA:           ;ADD	6		;*************************
		LD      L,C
                LD      H,B
                BIT     4,[HL]
                LD      B,A
                JR	NZ,EVOLVENTES
                LD	A,B
                LD      HL,DATOS_NOTAS
                RLCA                    ;X2
                LD      D,0
                LD      E,A
                ADD     HL,DE
                LD      A,[HL]
                LD      [IY+0],A
                INC     HL
                LD      A,[HL]
                LD      [IY+1],A
                RET

;IN [A]=CODIGO DE LA ENVOLVENTE
;IN [A]=CODIGO DE LA ENVOLVENTE
;   [IY]=REGISTRO DE FRECUENCIA

EVOLVENTES:     
		PUSH	AF
		CALL	ENV_RUT1
		LD	DE,$0000
		LD      [IY+0],E
                LD      [IY+1],D		
	
		POP	AF	
		ADD	A,48
		CALL	ENV_RUT1
		
	
		LD	A,E
                LD      [PSG_REG+11],A
                LD	A,D
                LD      [PSG_REG+12],A
                LD      A,$0E
                LD      [PSG_REG+13],A
                RET

;IN[A] NOTA
ENV_RUT1:	LD      HL,DATOS_NOTAS
		RLCA                    ;X2
                LD      D,0
                LD      E,A
                ADD     HL,DE
                LD	E,[HL]
		INC	HL
		LD	D,[HL]
                RET

;EXTRAE UN WORD DE UNA TABLA
;IN:[HL]=DIRECCION TABLA
;   [A]= POSICION
;OUT[HL]=WORD

EXT_WORD:       LD      D,0
                SLA     A               ;*2
                LD      E,A
                ADD     HL,DE
                LD      E,[HL]
                INC     HL
                LD      D,[HL]
                EX      DE,HL
                RET

;BANCO DE INSTRUMENTOS 2 BYTES POR INT.

;[0][RET 2 OFFSET]
;[1][+-PITCH]

TABLA_PAUTAS: DW 	PAUTA_1,PAUTA_2,PAUTA_3,PAUTA_4,PAUTA_5,PAUTA_6,PAUTA_7;,PAUTA_8,PAUTA_9,PAUTA_10,PAUTA_11,PAUTA_12,PAUTA_13,PAUTA_14,PAUTA_15,PAUTA_16,PAUTA_17,PAUTA_18


INCLUDE	"parasol_a2psg.mus.asm"



;DATOS DE LOS EFECTOS DE SONIDO
;EFECTOS DE SONIDO

TABLA_SONIDOS:  DW      SONIDO1,SONIDO2,SONIDO3,SONIDO4,SONIDO5;,SONIDO6,SONIDO7,SONIDO8
                
;DATOS MUSICA

TABLA_SONG:     DW     SONG_0,SONG_1,SONG_2,SONG_3,SONG_4,SONG_5,SONG_6,SONG_7,SONG_8,SONG_9,SONG_10,SONG_11,SONG_12,SONG_13,SONG_14;,SONG_15


		
;DATOS_NOTAS:    .INCBIN "NOTAS.DAT"        ;DATOS DE LAS NOTAS


DATOS_NOTAS:	DW $0000,$0000
		;DW $077C,$0708,
;		dw $06B0,$0640,$05EC,$0594,$0544,$04F8,$04B0,$0470,$042C,$03FD 
;		DW $03BE,$0384,$0358,$0320,$02F6,$02CA,$02A2,$027C,$0258,$0238,$0216,$01F8
;		DW $01DF,$01C2,$01AC,$0190,$017B,$0165,$0151,$013E,$012C,$011C,$010A,$00FC
;		DW $00EF,$00E1,$00D6,$00C8,$00BD,$00B2,$00A8,$009F,$0096,$008E,$0085,$007E
;		DW $0077,$0070,$006B,$0064,$005E,$0059,$0054,$004F,$004B,$0047,$0042,$003F
;		DW $003B,$0038,$0035,$0032,$002F,$002C,$002A,$0027,$0025,$0023,$0021,$001F
;		DW $001D,$001C,$001A,$0019,$0017,$0016,$0015,$0013,$0012,$0011,$0010,$000F

;TABLA3:		;dw $0CDA,$0C22,$0B73,$0ACF,$0A33,$09A1,$0917,$0894,$0819,$07A4,$0737,
		dw $06CF,$066D,$0611,$05BA,$0567,$051A,$04D0,$048B,$044A,$040C,$03D2,$039B,$0367
		dw $0337,$0308,$02DD,$02B4,$028D,$0268,$0246,$0225,$0206,$01E9,$01CE,$01B4
		dw $019B,$0184,$016E,$015A,$0146,$0134,$0123,$0112,$0103,$00F5,$00E7,$00DA
		dw $00CE,$00C2,$00B7,$00AD,$00A3,$009A,$0091,$0089,$0082,$007A,$0073,$006D
		dw $0067,$0061,$005C,$0056,$0052,$004D,$0049,$0045,$0041,$003D,$003A,$0036
		dw $0033,$0031,$002E,$002B,$0029,$0027,$0024,$0022,$0020,$001F,$001D,$001B
		dw $001A,$0018,$0017,$0016,$0014,$0013,$0012,$0011,$0010,$000F,$000E,$000D
		
		;DW	$688,$62A
		;DW	$5D2,$57E,$52F,$4E5,$49E,$45C,$41D,$3E2,$3AA,$376
		;DW	$344,$315,$2E9,$2BF,$297,$272,$24F,$22E,$20E,$1F1,$1D5,$1BB
		;DW	$1A2,$18A,$174,$15F,$14B,$139,$127,$117,$107,$F8,$EA,$DD
		;DW	$D1,$C5,$BA,$AF,$A5,$9C,$93,$8B,$83,$7C,$75,$6E
		;DW	$68,$62,$5D,$57,$52,$4E,$49,$45,$41,$3E,$3A,$37
		;DW	$34,$31,$2E,$2B,$29,$27,$24,$22,$20,$1F,$1D,$1B
		;DW	$1A,$18,$17,$15,$14,$13,$12,$11,$10,$F,$E,$D
		
;DATOS_ENV:      .INCBIN "ENV_L.DAT"          ;DATOS DE LAS ENVOLVENTES

SONG_0: EQU $B191 ;INCBIN "parasol.2psg.mus.bin" 		; Main menu
SONG_1: EQU $A673 ;INCBIN "genesis_gangway.2psg.mus.bin"	; Game over
SONG_2: EQU $B518 ;INCBIN "genesis_fin.2psg.mus.bin"		; Happy end
SONG_3: EQU $A000 ;INCBIN "Genesis_warrior.2psg.mus.bin"	; Happy end for losers without inertia
SONG_4: EQU $A000 ;INCBIN "genesis_jeff2.2psg.mus.bin"		; Level 1
SONG_5: EQU $AA0B ;INCBIN "genesis_line.2psg.mus.bin"		; Level 2
SONG_6: EQU $A000 ;INCBIN "genesis_compote.2psg.mus.bin"	; Level 3
SONG_7: EQU $A000 ;INCBIN "genesis_alice.2psg.mus.bin"		; Level 4
SONG_8: EQU $A761 ;INCBIN "genesis_homage.2psg.mus.bin"		; Level 5
SONG_9: EQU $A787 ;INCBIN "Genesis_equinox_v2.2psg.mus.bin"	; Level 6
SONG_10: EQU $A000 ;INCBIN "genesis_jeff2.2psg.mus.bin"		; Level 7
SONG_11: EQU $BC7A ;INCBIN "genesis_microint.2psg.mus.bin"	; Final boss level 1 and 5
SONG_12: EQU $AB32 ;INCBIN "genesis_words.2psg.mus.bin"		; Final boss level 2 and 6
SONG_13: EQU $B57D ;INCBIN "genesis_town.2psg.mus.bin"		; Final boss level 3 and 7
SONG_14: EQU $BB23 ;INCBIN "genesis_hoc.2psg.mus.bin"		; Final boss level 4


; Include effects player

INCLUDE	"genesis_sfx_2psg.asm"


; LOS DATOS QUE HAY QUE VARIAR :
; * BUFFER DE SONIDO DONDE SE DECODIFICA TOTALMENTE EL ARCHIVO MUS
; * N� DE CANCION. 
; * TABLA DE CANCIONES
INTERR          EQU     $C580           ;INTERRUPTORES 1=ON 0=OFF
                                        ;BIT 0=CARGA CANCION ON/OFF
                                        ;BIT 1=PLAYER ON/OFF
                                        ;BIT 2=EFECTOS ON/OFF
					;BIT 4=LOOP ON/OFF
					;BIT 5=EFECTO CANAL A
					;BIT 6=EFECTO CANAL B
					;BIT 7=EFECTO CANAL C

;MUSICA **** EL ORDEN DE LAS VARIABLES ES FIJO ******



SONG            EQU     $C581           ;DBN� DE CANCION
TEMPO           EQU     $C582           ;DB TEMPO
TTEMPO          EQU     $C583           ;DB CONTADOR TEMPO

PUNTERO_A       EQU     $C584           ;DW PUNTERO DEL CANAL A
PUNTERO_B       EQU     $C586           ;DW PUNTERO DEL CANAL B
PUNTERO_C       EQU     $C588           ;DW PUNTERO DEL CANAL C

CANAL_A         EQU     $C58A           ;DW DIRECION DE INICIO DE LA MUSICA A
CANAL_B         EQU     $C58C           ;DW DIRECION DE INICIO DE LA MUSICA B
CANAL_C         EQU     $C58E           ;DW DIRECION DE INICIO DE LA MUSICA C

PUNTERO_P_A     EQU     $C590           ;DW PUNTERO PAUTA CANAL A
PUNTERO_P_B     EQU     $C592           ;DW PUNTERO PAUTA CANAL B
PUNTERO_P_C     EQU     $C594           ;DW PUNTERO PAUTA CANAL C

PUNTERO_P_A0    EQU     $C596           ;DW INI PUNTERO PAUTA CANAL A
PUNTERO_P_B0    EQU     $C598           ;DW INI PUNTERO PAUTA CANAL B
PUNTERO_P_C0    EQU     $C59A           ;DW INI PUNTERO PAUTA CANAL C

PUNTERO_P_DECA	EQU	$C59C		;DW PUNTERO DE INICIO DEL DECODER CANAL A
PUNTERO_P_DECB	EQU	$C59E		;DW PUNTERO DE INICIO DEL DECODER CANAL B
PUNTERO_P_DECC	EQU	$C5A0		;DW PUNTERO DE INICIO DEL DECODER CANAL C

PUNTERO_DECA	EQU	$C5A2		;DW PUNTERO DECODER CANAL A
PUNTERO_DECB	EQU	$C5A4		;DW PUNTERO DECODER CANAL B
PUNTERO_DECC	EQU	$C5A6		;DW PUNTERO DECODER CANAL C       


;CANAL DE EFECTOS - ENMASCARA OTRO CANAL

PUNTERO_P       EQU     $C5A8           ;DW PUNTERO DEL CANAL EFECTOS
CANAL_P         EQU     $C5AA           ;DW DIRECION DE INICIO DE LOS EFECTOS
PUNTERO_P_DECP	EQU	$C5AE		;DW PUNTERO DE INICIO DEL DECODER CANAL P
PUNTERO_DECP	EQU	$C5B0		;DW PUNTERO DECODER CANAL P

;EFECTOS DE SONIDO

N_SONIDO        EQU     $C5B2           ;DB : NUMERO DE SONIDO
PUNTERO_SONIDO  EQU     $C5B3           ;DW : PUNTERO DEL SONIDO QUE SE REPRODUCE

; SOUND AND FX SELECTOR
SOUND_SFX:	EQU 	$C5B5		;DB	3; 0: Silence; 1: Music only; 2: SFX ony; 3: Musix+SFX


;DB [13] BUFFERs DE REGISTROS DEL PSG

PSG_REG         EQU     $C5C0
PSG_REG_EXT	EQU	$C5D0
BUFFER_DEC	EQU	$C5E0		; $40 bytes

;FADE_OUT

FADE		EQU	$C630		;BIT 0 FADE OUT ON/OFF
					;BIT 1 FADE IN  ON/OFF
FADE_METER	EQU	$C631		;DB FADE_METER (TIEMPO DE ACTUACION)
DECAY		EQU	$C632		;DB CADENCIA>1
DECAY_TEMP	EQU	$C633		;DB

; Include movement and behavior
include "move.asm"
include "behav.asm"

