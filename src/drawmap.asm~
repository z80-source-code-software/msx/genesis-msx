; Rutina para pintar el mapa... que Dios me pille confesado

; Input:
; 	DE: Map + displacement thru map in tiles
; 	BC: displacement thru map; B: chars (0-2), C: pixels (0,1,2,3)
; 	HL: map width in characters
;	TablaTiles: table with preshifted tiles (see create_shifted_tables.asm), at $9000
;	CurrentHalf: current half of the tiles to modify (0-127 or 128-255). It will be either 0 or 128

; Additionally used registers:
;	IYh: height counter (16)
;	IYl: width counter, in tiles (10)
;       IXl: counter for the number of thirds (2)
;
;	B' copy of CurrentHalf
;	HL': pointer moving through the TileMap array

; Temporary areas
;
;	TileMap: 512 bytes, 256 per screen third
;	TmpColor: 512 bytes (64*8), to temporarily store the generated colors for each screen third
;	TmpTiles: 512 bytes (64*8), to temporarily store the generated tiles for each screen third


DrawMap_stage0:   ; same for PAL and NTSC
  	; STEP 1: transfer the 64 fixed positions for each third, both bitmap and color

	ld a, c
	ld (drawdisp), a ; Store for later use
	exx		; switch to the alternate register set
	add a,a		; desp * 2
	add a, $90
	ld h, a
	ld l, 0		; HL points to the tiles for 0-1 and 1-2
	ld a, (CurrentHalf)
	rlca
	rlca
	rlca		; A can only be 0 or 128. If it is 128, it will be 4 after this, exactly what we need
	ld d, a
	ld e, l		; DE points to the address in VRAM for the first third, l =0
	push hl
	push de
	call LDIRVRM_fast	; TRANSFERRED: 512 bytes
	pop de			; HL and BC will be the same. We just need to add 2048 to DE
	ld hl, 2048
	add hl, de
	ex de, hl
	pop hl
	ld bc, 512
	call LDIRVRM_medium	; TRANSFERRED: 1024 bytes (accumulated)
        exx
	ret


DrawMap_stage1:		; will change for NTSC, removing the last call to CleanupTileCache
	; Bitmaps transferred, now transfer color. This is a bit easier, as no shift is used!
        exx
	ld hl, ColorsFixed
	ld a, (CurrentHalf)
	rlca
	rlca
	rlca		; A can only be 0 or 128. If it is 128, it will be 4 after this, exactly what we need
	add a, $20	; Colors start at $2000
	ld d, a
	ld e, 0		; DE points to the address in VRAM for the first third of colors
	push de
	call LDIRVRM_fast	; TRANSFERRED: 1536 bytes (accumulated)
	pop de

	ld a, d
	add a, 8	; $2800, start of the second third
	ld d, a		; DE points to the address in VRAM for the second third of colors
	ld bc, 512
	ld hl, ColorsFixed
	call LDIRVRM_medium	; TRANSFERRED: 2048 bytes (accumulated)
	; Clean up tile cache
	call CleanupTileCache
	exx			; Back to the original register set

	ret

DrawMap_stage1_NTSC:		
	; Bitmaps transferred, now transfer color. This is a bit easier, as no shift is used!
        exx
	ld hl, ColorsFixed
	ld a, (CurrentHalf)
	rlca
	rlca
	rlca		; A can only be 0 or 128. If it is 128, it will be 4 after this, exactly what we need
	add a, $20	; Colors start at $2000
	ld d, a
	ld e, 0		; DE points to the address in VRAM for the first third of colors
	push de
	call LDIRVRM_fast	; TRANSFERRED: 1536 bytes (accumulated)
	pop de

	ld a, d
	add a, 8	; $2800, start of the second third
	ld d, a		; DE points to the address in VRAM for the second third of colors
	ld bc, 512
	ld hl, ColorsFixed
	call LDIRVRM_medium	; TRANSFERRED: 2048 bytes (accumulated)
	; Clean up tile cache
	exx			; Back to the original register set

	ret

DrawMap_stage2:		; different for NTSC: first cleanuptilecache, then just 4 characters high
	push de
	ld de, -10
	add hl, de
	pop de				; precalc the map width - 10, to add it later on
	; STEP 2: walk through map. For tiles within the fixed area, just print them. For variable tiles,
	; generate them and place them in the TmpColor and TmpTiles

	exx
	ld hl, TmpColor
	ld (PtrColor), hl
	ld hl, TmpTiles
	ld (PtrTiles), hl
	ld hl, TileMap+1	; HL' moves through the tile map
	ld a, (CurrentHalf)
	ld b, a			; B' has a copy of CurrentHalf
	add a, 64
	ld (NextTile), a	; next tile

	exx

	ld iyh, 8			; 8 characters high (only one third for now)

        call draw_loopy
        ld (SaveMap), de       ; Save the map position, to recover it for stage 3


        ret

DrawMap_stage2_1_NTSC:		; different for NTSC: first cleanuptilecache, then just 4 characters high
	exx			; Alternate regset
	call CleanupTileCache
	exx			; Back to the original register set

	push de
	ld de, -10
	add hl, de
	pop de				; precalc the map width - 10, to add it later on
	; STEP 2: walk through map. For tiles within the fixed area, just print them. For variable tiles,
	; generate them and place them in the TmpColor and TmpTiles

	exx
	ld hl, TmpColor
	ld (PtrColor), hl
	ld hl, TmpTiles
	ld (PtrTiles), hl
	ld hl, TileMap+1	; HL' moves through the tile map
	ld a, (CurrentHalf)
	ld b, a			; B' has a copy of CurrentHalf
	add a, 64
	ld (NextTile), a	; next tile

	exx

	ld iyh, 4			; 4 characters high (half a third for now)

        call draw_loopy
        ld (SaveMap), de       ; Save the map position, to recover it for stage 3
        ret


DrawMap_stage2_2_NTSC:		; new for NTSC: just 4 characters high
	ld de, -10
	add hl, de				; precalc the map width - 10, to add it later on
	ld de, (SaveMap)
	; walk through map. For tiles within the fixed area, just print them. For variable tiles,
	; generate them and place them in the TmpColor and TmpTiles

 	exx
	ld hl, TileMap+128+1	; HL' moves through the tile map
	ld a, (CurrentHalf)
	ld b, a			; B' has a copy of CurrentHalf
	exx

	ld iyh, 4			; 8 characters high (second third)

	call draw_loopy
        ld (SaveMap), de       ; Save the map position, to recover it for stage 3
        ret


DrawMap_stage3:			; for NTSC, no cleanuptilecache
        ; Transfer the generated tiles
	ld a, (CurrentHalf)
	add a, 64
	rlca
	rlca
	rlca
	ld d, a
	ld e, 0		; DE points to the address in VRAM for the first third
	ld hl, TmpTiles
	call LDIRVRM_fast	; TRANSFERRED: 2560 bytes
	ld a, (CurrentHalf)
	add a, 64
	rlca
	rlca
	rlca
	add a, $20	; Colors start at $2000
	ld d, a
	ld e, 0		; DE points to the address in VRAM for the first third
	ld hl, TmpColor
	ld bc, 512
	call LDIRVRM_medium	; TRANSFERRED: 3072 bytes
	; Clean up tile cache
        call CleanupTileCache
        ret

DrawMap_stage3_NTSC:			; for NTSC, no cleanuptilecache
        ; Transfer the generated tiles
	ld a, (CurrentHalf)
	add a, 64
	rlca
	rlca
	rlca
	ld d, a
	ld e, 0		; DE points to the address in VRAM for the first third
	ld hl, TmpTiles
	call LDIRVRM_fast	; TRANSFERRED: 2560 bytes
	ld a, (CurrentHalf)
	add a, 64
	rlca
	rlca
	rlca
	add a, $20	; Colors start at $2000
	ld d, a
	ld e, 0		; DE points to the address in VRAM for the first third
	ld hl, TmpColor
	ld bc, 512
	call LDIRVRM_medium	; TRANSFERRED: 3072 bytes
        ret

DrawMap_stage4:
	ld de, -10
	add hl, de				; precalc the map width - 10, to add it later on
	ld de, (SaveMap)
	; STEP 2: walk through map. For tiles within the fixed area, just print them. For variable tiles,
	; generate them and place them in the TmpColor and TmpTiles

 	exx
	ld hl, TmpColor
	ld (PtrColor), hl
	ld hl, TmpTiles
	ld (PtrTiles), hl
	ld hl, TileMap+256+1	; HL' moves through the tile map
	ld a, (CurrentHalf)
	ld b, a			; B' has a copy of CurrentHalf
	add a, 64
	ld (NextTile), a	; next tile
	exx

	ld iyh, 8			; 8 characters high (second third)

	call draw_loopy
        ret

DrawMap_stage4_1_NTSC:		; different for NTSC: first cleanuptilecache, then just 4 characters high
	exx			; Alternate regset
	call CleanupTileCache
	exx			; Back to the original register set

	ld de, -10
	add hl, de				; precalc the map width - 10, to add it later on
	ld de, (SaveMap)
	; STEP 2: walk through map. For tiles within the fixed area, just print them. For variable tiles,
	; generate them and place them in the TmpColor and TmpTiles

 	exx
	ld hl, TmpColor
	ld (PtrColor), hl
	ld hl, TmpTiles
	ld (PtrTiles), hl
	ld hl, TileMap+256+1	; HL' moves through the tile map
	ld a, (CurrentHalf)
	ld b, a			; B' has a copy of CurrentHalf
	add a, 64
	ld (NextTile), a	; next tile
	exx

	ld iyh, 4			; 8 characters high (second third)

	call draw_loopy
        ld (SaveMap), de       ; Save the map position, to recover it for stage 3
        ret

DrawMap_stage4_2_NTSC:		; new for NTSC: just 4 characters high
	ld de, -10
	add hl, de				; precalc the map width - 10, to add it later on
	ld de, (SaveMap)
	; walk through map. For tiles within the fixed area, just print them. For variable tiles,
	; generate them and place them in the TmpColor and TmpTiles

 	exx
	ld hl, TileMap+384+1	; HL' moves through the tile map
	ld a, (CurrentHalf)
	ld b, a			; B' has a copy of CurrentHalf
	exx

	ld iyh, 4			; 8 characters high (second third)

	call draw_loopy
        ret

DrawMap_stage5:
	; Transfer the generated tiles for the second third
	ld a, (CurrentHalf)
	add a, 64
	rlca
	rlca
	rlca
	add a, $08	; The second third starts at $0800
	ld d, a
	ld e, 0		; DE points to the address in VRAM for the second third
	ld hl, TmpTiles
	call LDIRVRM_fast	; TRANSFERRED: 3584 bytes
	ld a, (CurrentHalf)
	add a, 64
	rlca
	rlca
	rlca
	add a, $28	; Colors for the second third start at $2800
	ld d, a
	ld e, 0		; DE points to the address in VRAM for the first third
	ld bc, 512
	ld hl, TmpColor
	call LDIRVRM_medium	; TRANSFERRED: 4096 bytes
	ret

DrawMap_stage6:
	; Update the CurrentHalf variable
	ld a, (CurrentHalf)
	xor $80
	ld (CurrentHalf), a

	; Finally, switch screen
	ld hl, $D000		; TileMap
	ld de, $1800
	call LDIRVRM_fast
	ret


draw_loopy:
	ld a, b
	dec a				; A == 1
	jp z, drawleft_1char
	dec a				; A == 2
	jp z, drawleft_2char
drawleft_0char:
	ld iyl, 10			; 10 full tiles width
	jp drawcenter			; no special processing needed, go straight and draw
drawleft_2char:
	ld iyl, 9			; 9 full tiles width
	; only draw the variable tile
	ld a, (de)
	inc de				; A has the current tile
	exx				; alternate regset
	ld c, a
	exx				; normal regset
	ld a, (de)			; C holds the current tile, A the next one
	exx				; alternate regset
	call MergeTiles 		; Mix the tiles
	ld (hl), a			; A is the merged tile
	inc hl
	exx				; normal regset 
	jp drawcenter
drawleft_1char:
	ld iyl, 9			; 9 full tiles width
	; now, draw 1 fixed tile and 1 variable tile
	ld a, (de)
	inc de				; A has the current tile
	exx				; alternate regset
	ld c, a				; C = current tile
	add a, a			; tile * 2
	or b
	inc a				; tile * 2 + 1
	ld (hl), a			
	inc hl		
	exx				; normal regset
	ld a, (de)			; C holds the current tile, A the next one
	exx				; alternate regset
	call MergeTiles 		; Mix the tiles
	ld (hl), a			; A is the merged tile
	inc hl
	exx				; normal regset 	

drawcenter:	; draw center tiles
	ld a, (de)
	inc de				; A has the current tile
	exx				; alternate regset
	ld c, a				; c = current tile	
	add a, a			; tile * 2
	or b
	ld (hl), a
	inc hl
	inc a
	ld (hl), a			
	inc hl				; the first two tiles are done, now for the difficult third
	exx				; normal regset
	ld a, (de)			; C holds the current tile, A the next one
	exx				; alternate regset
	call MergeTiles 		; Mix the tiles
	ld (hl), a			; A is the merged tile
	inc hl
	exx				; normal regset 	
	dec iyl
	jp nz, drawcenter 
	; now draw the right portion
	ld a, b
	dec a				; A == 1
	jp z, drawright_1char
	dec a				; A == 2
	jp nz, nextline			; if B is 0, just skip to the nextline
drawright_2char:
	; we have to draw two fixed tiles
	ld a, (de)			; A has the current tile, no inc needed!
	exx				; alternate regset
	add a, a			; tile * 2
	or b
	ld (hl), a
	inc hl
	inc a
	ld (hl), a
	inc hl				
	exx				; normal regset
	jp nextline
drawright_1char:
	; we have to draw only one fixed tile
	ld a, (de)			; A has the current tile, no inc needed!
	exx				; alternate regset
	add a, a			; tile * 2
	or b
	ld (hl), a
	inc hl
	exx				; normal regset


nextline:
	; adjust DE to point to the next line, and HL' to do the same
	ex de, hl	; now HL has the pointer, and DE the map width - 10
	add hl, de
	ex de, hl	; DE + map width - 10
	exx				; alternate regset
	ld de, 2
	add hl, de			; skip 1 tiles at the right, and 1 more at the left	
	exx				; normal regset
	dec iyh
	jp nz, draw_loopy
	ret






; This function will merge the shifted tiles
;
; INPUT:
;	C: current tile
;	A: next tile
;	PtrColor: pointer to the TmpColor array, current position
;	PtrTiles: pointer to the TmpTiles array, current position
;	drawdisp: current pixel displacement for this frame 
;
; REGISTERS:
;
; OUTPUT:
; 	NextTile: variable specifying the last value stored in the TileMap
;	A: 	  value to store in the TileMap
MergeTiles:
	push hl	
	push bc

	push af 	; A = 000YYYYY, C = 000XXXXX
	rrca
	rrca
	rrca		; A = YYY000YY
	ld b, a		; B = YYY000YY
	and $E0		; A = YYY00000
	or c		; A = YYYXXXXX
	ld l, a
	ld a, b
	and $03		; A = 000000YY
	or $AC		; A = 101011YY
	ld h, a		; HL points to the address in TileCache of A-C
	ld a, (hl)
	and a		; if a = 0, then this tile is not used. Otherwise, we got it
	jp nz, merge_alreadydone

	ld a, (NextTile) 
	ld (hl), a	; store the tile in the cache...

	pop af	 	; If we do not have the tiles merged, then do so now
	push af

	add a, a
	add a, a
	add a, a		; A is 0-31, so A*8 will not be more than 255
	ld l, a
	ld a, (drawdisp)
	add a, $98		; tiles for part 0 start here
	ld h, a			; HL points to the first byte of the next tile

	add a, 4			;$9C-$98		; tiles for part 2 start here
	ld b, a			; BC points to the last char of the current tile

	ld a, c
	add a, a
	add a, a
	add a, a		; A is 0-31, so A*8 will not be more than 255
	ld c, a


	ld de, (PtrTiles)	; and now, start loading and mixing

	ld a, (bc)
	or (hl)
	ld (de), a
	inc c
	inc e
	inc l
	ld a, (bc)
	or (hl)
	ld (de), a
	inc c
	inc e
	inc l
	ld a, (bc)
	or (hl)
	ld (de), a
	inc c
	inc e
	inc l
	ld a, (bc)
	or (hl)
	ld (de), a
	inc c
	inc e
	inc l
	ld a, (bc)
	or (hl)
	ld (de), a
	inc c
	inc e
	inc l
	ld a, (bc)
	or (hl)
	ld (de), a
	inc c
	inc e
	inc l
	ld a, (bc)
	or (hl)
	ld (de), a
	inc c
	inc e
	inc l
	ld a, (bc)
	or (hl)
	ld (de), a
	inc de		; mixing done!

	ld (PtrTiles), de	; store pointer back

	pop af
	pop bc		; C still holds the current tile. For colors, we store the current tile color and go, if the current tile is not 0. If it is, we store 
			; the color of the next tile, which was in A
	push bc
	ld b, a
	ld a, c
	and a
	jr nz, current_tile_not_zero
	ld a, b		; B is the next tile!
current_tile_not_zero:
	add a, a
	add a, a
	add a, a		; A is 0-31, so A*8 will not be more than 255
	ld l, a
	ld h, $A3		; Color for tiles part 2	
	ld de, (PtrColor)
	
	ld a, (hl)
	ld (de), a
	inc l
	inc e
	ld a, (hl)
	ld (de), a
	inc l
	inc e
	ld a, (hl)
	ld (de), a
	inc l
	inc e
	ld a, (hl)
	ld (de), a
	inc l
	inc e
	ld a, (hl)
	ld (de), a
	inc l
	inc e
	ld a, (hl)
	ld (de), a
	inc l
	inc e
	ld a, (hl)
	ld (de), a
	inc l
	inc e
	ld a, (hl)
	ld (de), a
	inc de

	ld (PtrColor), de

	ld a, (NextTile)
	inc a
	ld (NextTile), a	; This will be the next value
	dec a			; And this is the one to store

	pop bc
	pop hl
	ret			; and off we go

merge_alreadydone:
	ld b, a
	pop af
	ld a, b			; A is the value to store in the TileMap
	pop bc
	pop hl
	ret			; and off we go



CleanupTileCache:
	push hl
	ld hl, 0
	ld ($cf00), sp
	ld sp, TileCache+1024
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	push hl
	ld sp, ($cf00)
	pop hl
	ret
