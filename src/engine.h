//   Copyright 2012 Francisco Javier Peña
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef ENGINE_H
#define ENGINE_H


// Function definitions...is there a way to automate this?

#asm
DEFC flickerptr = $C05C

DEFC CreaTablaTiles = $676E
DEFC DrawMap_stage0 = $68C9
DEFC DrawMap_stage1 = $68F0
DEFC DrawMap_stage1_NTSC = $6916
DEFC DrawMap_stage2 = $6939
DEFC DrawMap_stage2_1_NTSC = $6964
DEFC DrawMap_stage2_2_NTSC = $6994
DEFC DrawMap_stage3 = $69B0
DEFC DrawMap_stage3_NTSC = $69DB
DEFC DrawMap_stage4 = $6A03
DEFC DrawMap_stage4_1_NTSC = $6A2C
DEFC DrawMap_stage4_2_NTSC = $6A5E
DEFC DrawMap_stage5 = $6A76
DEFC DrawMap_stage6 = $6AA0
DEFC DrawSprite   = $6DEA
DEFC DrawShipSprite   = $6E21
DEFC EnDisHorInterrupts = $7125
DEFC EndSpriteDraw = $6E41
DEFC Fadeout = $7075
DEFC FILLVRM = $6650
DEFC INIT_ENGINE  = $6EA4
DEFC INSTALL_ISR  = $6093
DEFC INSTALL_HORISR = $6097
DEFC LDIRVRM_fast = $60E3
DEFC LDIRVRM_fast_128 = $64EE
DEFC LDIRVRM_medium = $65F9
DEFC LDIRVRM_safe = $60AB
DEFC LDIRVRM_safe_DI = $60C8
DEFC LoadSprBlock = $6E86
DEFC PrintLargeNumber = $6F06
DEFC PrintLarge_2	= $6F2A
DEFC PrintLarge_5	= $6F45
DEFC PrintNumber	= $6FB7
DEFC PrintSmall_5	= $6F79
DEFC PrintString	= $6FD1
DEFC ROMbank1	  = $C04C
DEFC ROMbank2	  = $C04D
DEFC ROMbank3	  = $C04E
DEFC SetDefaultPalette = $70C1
DEFC SetHorInterruptLine = $711B
DEFC SetLevelPalette = $7106
DEFC TransferSprites = $6E68
DEFC VDP_ActDeact = $6662
DEFC WRTVDP_DI = $668A
DEFC WRTVRM = $666E
DEFC WRTVRM_DI = $667D
DEFC curspr	   = $C05B
DEFC enableSLOT2  = $66C4
DEFC enableSLOT2_EI  = $66CC
DEFC get_joystick = $6009
DEFC get_keyboard = $6024
DEFC initROMbanks = $6693
DEFC msxHZ	= $C049
DEFC searchramnormal80 = $66D6
DEFC setROM2	  = $66A4
DEFC setROM2_DI	  = $66AD
DEFC setROM3	  = $66B4
DEFC setROM3_DI	  = $66BD

#endasm


// Joystick definitions

#define JOY_UP		0x1
#define JOY_DOWN	0x2
#define JOY_LEFT	0x4
#define JOY_RIGHT	0x8
#define JOY_FIRE	0x10

// Used keyboard definitions
#define KEY_C		0x0103

// VDP functions in BIOS
#define WRTVDP		 $0047	// Write B to VDP register C
#endif
