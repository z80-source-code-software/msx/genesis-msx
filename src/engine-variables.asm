;   Copyright 2012 Francisco Javier Peña
;
;   Licensed under the Apache License, Version 2.0 (the "License");
;   you may not use this file except in compliance with the License.
;   You may obtain a copy of the License at
;
;       http://www.apache.org/licenses/LICENSE-2.0
;
;   Unless required by applicable law or agreed to in writing, software
;   distributed under the License is distributed on an "AS IS" BASIS,
;   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;   See the License for the specific language governing permissions and
;   limitations under the License.

; All variables used by the engine

FrameCounter 	EQU 	$C040	; 2 bytes
UserISR		EQU	$C042	; 2 bytes
InterruptBuffer EQU	$C044	; 5 bytes
msxHZ		EQU	$C049	; 1 byte
thisslt		EQU	$C04A	; 1 byte
ROMBank0	EQU	$C04B	; 1 byte
ROMBank1	EQU	$C04C	; 1 byte
ROMBank2	EQU	$C04D	; 1 byte
ROMBank3	EQU	$C04E	; 1 byte
SaveTablaTiles	EQU	$C04F	; 2 bytes
CurrentHalf	EQU	$C051	; 1 byte
PtrColor	EQU	$C052	; 2 bytes
PtrTiles	EQU	$C054	; 2 bytes
NextTile	EQU	$C056	; 1 byte
drawdisp	EQU	$C057	; 1 byte
MapDone		EQU	$C058	; 1 byte
SaveMap         EQU     $C059   ; 2 bytes
curspr		EQU	$C05B	; 1 byte
flickerptr	EQU	$C05C	; 1 byte
palette		EQU	$C05D	; 32 bytes, next is c07d!
msx2ISR		EQU	$C07D   ; 2 bytes, next is c07f (last byte!)


; And some constants

TablaTiles_Temp	EQU	$A000
TablaTiles	EQU	$9000
ColorsFixed	EQU	$A000
ColorsTile0	EQU	$A200
ColorsTile2	EQU	$A300
TileCache	EQU 	$AC00
TileMap		EQU	$D000
TmpColor	EQU	$D200
TmpTiles	EQU	$D400
SprAttCopy1	EQU	$B000
SprAttCopy2	EQU	$B080


