;   Copyright 2012 Francisco Javier Peña
;
;   Licensed under the Apache License, Version 2.0 (the "License");
;   you may not use this file except in compliance with the License.
;   You may obtain a copy of the License at
;
;       http://www.apache.org/licenses/LICENSE-2.0
;
;   Unless required by applicable law or agreed to in writing, software
;   distributed under the License is distributed on an "AS IS" BASIS,
;   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;   See the License for the specific language governing permissions and
;   limitations under the License.

org $A000

genesis_compote: INCBIN"genesis_compote_2psg.mus"
genesis_equinox: INCBIN"genesis_equinox_v2_2psg.mus"
genesis_fin: INCBIN"genesis_fin_2psg.mus"
genesis_hoc: INCBIN"genesis_hoc_2psg.mus"
