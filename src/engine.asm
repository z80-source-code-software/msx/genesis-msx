;   Copyright 2012 Francisco Javier Peña
;
;   Licensed under the Apache License, Version 2.0 (the "License");
;   you may not use this file except in compliance with the License.
;   You may obtain a copy of the License at
;
;       http://www.apache.org/licenses/LICENSE-2.0
;
;   Unless required by applicable law or agreed to in writing, software
;   distributed under the License is distributed on an "AS IS" BASIS,
;   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;   See the License for the specific language governing permissions and
;   limitations under the License.

; This is the main file for the scrolling engine
; It will just set the assembler origin, and include the files


org $6000

include "engine-variables.asm"
include "input.asm"
include "interrupt.asm"
include "vdp.asm"
include "memory.asm"
include "create_shifted_tables.asm"
include "drawmap.asm"
include "drawsprite.asm"

; Initialize Engine!
INIT_ENGINE:
		call init_screen2
		call init_ISR	

		; initialize all the map temporary variables		
		xor a
		ld (CurrentHalf), a
		ld (MapDone), a
		ld hl, TileMap
		ld de, TileMap+1
		ld (hl), 0
		ld bc, 1535
		ldir		; Fill TileMap, TmpColor and TmpTiles with zeroes
		
		; Find out whether this is a PAL or NTSC MSX
		ld a, ($002b)
		and $80		; The highest bit is 1 for PAL, 0 for NTSC
		jr z, isjapanese
		ld a, 50
		jr setmsxHZ
isjapanese:	ld a, 60
setmsxHZ:
		ld (msxHZ), a
		ret

include "print.asm"
include "msx2.asm"

