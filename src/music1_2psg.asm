;   Copyright 2012 Francisco Javier Peña
;
;   Licensed under the Apache License, Version 2.0 (the "License");
;   you may not use this file except in compliance with the License.
;   You may obtain a copy of the License at
;
;       http://www.apache.org/licenses/LICENSE-2.0
;
;   Unless required by applicable law or agreed to in writing, software
;   distributed under the License is distributed on an "AS IS" BASIS,
;   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;   See the License for the specific language governing permissions and
;   limitations under the License.

org $A000

genesis_alice: INCBIN"genesis_alice_2psg.mus"
genesis_gangway: INCBIN"genesis_gangway_2psg.mus"
genesis_homage: INCBIN"genesis_homage_2psg.mus"
genesis_microint: INCBIN"genesis_microint_2psg.mus"
