;   Copyright 2012 Francisco Javier Peña
;
;   Licensed under the Apache License, Version 2.0 (the "License");
;   you may not use this file except in compliance with the License.
;   You may obtain a copy of the License at
;
;       http://www.apache.org/licenses/LICENSE-2.0
;
;   Unless required by applicable law or agreed to in writing, software
;   distributed under the License is distributed on an "AS IS" BASIS,
;   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;   See the License for the specific language governing permissions and
;   limitations under the License.

; VDP routines

; BIOS definitions

VDPREG    	EQU  $F3DF          	; REG 0 of VDP
WRTVDP		EQU  $0047		; Write B to VDP register C
INIGRP  	EQU  $0072		; Set SCREEN 2 mode

; VRAM definitions

VRAMFRM     	EQU 	$0000          ; Tile Definitions
VRAMTILES   	EQU	$1800          ; Tile map (3x256 bytes)
VRAMATTR    	EQU	$1B00          ; Sprite attributes (Y,X,COL,NUM)
VRAMCOL     	EQU	$2000          ; Tile Colors
VRAMSPRITES 	EQU	$3800          ; Sprite definitions


; Initalize SCREEN 2 mode, with 16x16 sprites, no magnification, black backdrop

init_screen2:
		call INIGRP	; SCREEN 2
                ld bc, $E201
                call WRTVDP	; set 16x16 sprites, no magnification
                ld bc, $0107
;                call WRTVDP	; black backdrop
		nop
		nop
		nop		; FIXME
		ret

; Copy data from RAM to VRAM, safe version (could be a bit faster, though)
; INPUT:
;	HL: source address in RAM
;	DE: destination address in VRAM
;	BC: number of bytes
;
; Note: to be used during "normal" operations, i.e. not during the interrupt

LDIRVRM_safe:

		ld a, e
		di
		out ($99), a
		ld a, d
		or 01000000B
		out ($99), a	; Setup pointer in VRAM
		ei		; Is this safe????


LDIRVRM_pointersetup:
		ld d, b			; D has the high nibble
		ld b, c			; B has the low nibble
		ld c, $98
	
		ld a, b
		or a
		jr z, LDRVRMBC0	
		inc d
			
LDRVRMBC0:	
		outi
		jp nz, LDRVRMBC0	; inner loop (low nibble)
		dec d
		jp nz, LDRVRMBC0	; outer loop (high nibble)
		;ei
		ret 			; when both are zero, return

; Same as before, but assumes interrupts are disabled

LDIRVRM_safe_DI:

		ld a, e
		out ($99), a
		ld a, d
		or 01000000B
		out ($99), a	; Setup pointer in VRAM

LDIRVRM_DI_pointersetup:
		ld d, b			; D has the high nibble
		ld b, c			; B has the low nibble
		ld c, $98
	
		ld a, b
		or a
		jr z, LDRVRMBC0_DI
		inc d
			
LDRVRMBC0_DI:	
		outi
		jp nz, LDRVRMBC0_DI	; inner loop (low nibble)
		dec d
		jp nz, LDRVRMBC0_DI	; outer loop (high nibble)
		ret 			; when both are zero, return




; Copy a block of 512 bytes from RAM to VRAM, unsafe version (MUST happen during VBLANK,
; or screen corruption may happen in some MSX1 models with slow VDPs)
; INPUT:
;	HL: source address in RAM
;	DE: destination address in VRAM
;
; Note: Assumes interrupts are disabled

LDIRVRM_fast:
		ld a, e
		out ($99), a
		ld a, d
		or 01000000B
		out ($99), a	; Setup pointer in VRAM
		ld c, $98
REPT 512
		outi		; Do 512 outi's in a row!
ENDM
		ret



; Copy a block of 128 bytes from RAM to VRAM, unsafe version (MUST happen during VBLANK,
; or screen corruption may happen in some MSX1 models with slow VDPs)
; INPUT:
;	HL: source address in RAM
;	DE: destination address in VRAM
;
; Note: Assumes interrupts are disabled

LDIRVRM_fast_128:
		ld a, e
		out ($99), a
		ld a, d
		or 01000000B
		out ($99), a	; Setup pointer in VRAM
		ld c, $98
REPT 128
		outi		; Do 128 outi's in a row!
ENDM
		ret



; Copy data from RAM to VRAM, safe but faster version
; INPUT:
;	HL: source address in RAM
;	DE: destination address in VRAM
;	BC: number of bytes (must be a multiple of 16)
;
;	MODIFIES: A
;
; Note: Assumes interrupts are disabled

LDIRVRM_medium:

		ld a, e
		out ($99), a
		ld a, d
		or 01000000B
		out ($99), a	; Setup pointer in VRAM

		ld d, b			; D has the high nibble
		ld b, c			; B has the low nibble
		ld c, $98

		ld a, b
		or a
		jr z, ldirvrm_loop
		inc d

ldirvrm_loop:
REPT 15
		outi
		nop
		nop
ENDM
		outi			; we should be able to skip the NOPs here, since if we jump we take about 11 t-states, and 2 if we do not... but then we do not care
		jp nz, ldirvrm_loop	; this should be pretty compatible with all VDPs
		dec d
		jp nz, ldirvrm_loop
		ret


; Fill data in VRAM
;	 A: value
;	DE: destination address in VRAM
;	BC: number of bytes 
FILLVRM:
		ld h, a
		ld a, e
		out ($99), a
		ld a, d
		or 01000000B
		out ($99), a	; Setup pointer in VRAM
FILLVRM_loop:
		ld a, h
		out ($98), a
		dec bc
		ld a, c
		or b
		jr nz, FILLVRM_loop
		ret


;Activate/Deactivate VDP
;IN:N/A
;OUT:N/A

VDP_ActDeact:  	LD      A,(VDPREG+1)
                XOR     01000000B
		LD	B, A
                LD      C, 1
                CALL    WRTVDP
                RET


; Writes one byte in VRAM
; IN: 
;
;	HL - address in VRAM to write to
;       A  - value to write

WRTVRM:
		push af
		ld a, l
		di
		out ($99), a
		ld a, h
		or 01000000B
		ei
		out ($99), a	; Setup pointer in VRAM
		pop af
		out ($98), a
		ret

; Same as above, but assuming interrupts are disabled
WRTVRM_DI:
		push af
		ld a, l
		out ($99), a
		ld a, h
		or 01000000B
		out ($99), a	; Setup pointer in VRAM
		pop af
		out ($98), a	; write value
		ret

; Write value B to VDP register C, assuming interrupts are disabled
WRTVDP_DI:
		ld 	a, b
		OUT     (0x99),A
                LD      A,c
                OR      10000000B
                OUT     (0x99),A
                RET
