;   Copyright 2012 Francisco Javier Peña
;
;   Licensed under the Apache License, Version 2.0 (the "License");
;   you may not use this file except in compliance with the License.
;   You may obtain a copy of the License at
;
;       http://www.apache.org/licenses/LICENSE-2.0
;
;   Unless required by applicable law or agreed to in writing, software
;   distributed under the License is distributed on an "AS IS" BASIS,
;   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;   See the License for the specific language governing permissions and
;   limitations under the License.

org $6000

level5:
INCBIN "level5.map"
enemies5:               ; the enemy structure has just one byte detailing the number of enemies
                        ; and the locations in struct Enemy format

INCLUDE "level5_enemies.asm"

level6:
INCBIN "level6.map"
enemies6:               ; the enemy structure has just one byte detailing the number of enemies
                        ; and the locations in struct Enemy format

INCLUDE "level6_enemies.asm"

level7:
INCBIN "level7.map"
enemies7:               ; the enemy structure has just one byte detailing the number of enemies
                        ; and the locations in struct Enemy format

INCLUDE "level7_enemies.asm"


finalenemy5:
	db 4
	db 165, 0, 28,  FINAL5_UL,    MOVE_NONE,   45, 0, 0
	db 181, 0, 28,  FINAL5_UR,    MOVE_NONE,     0, 0, 2  	; 
	db 165, 0, 44,  FINAL5_DL,    MOVE_NONE,     0, 0, 2  	; 
	db 181, 0, 44,  FINAL5_DR,    MOVE_NONE,     0, 0, 2 	; 
finalenemy6:
	db 4
	db 165-32, 0, 28,  FINAL6_UL,    MOVE_NONE,   50, 0, 0
	db 181-32, 0, 28,  FINAL6_UR,    MOVE_NONE,     0, 0, 2  	; 
	db 165-32, 0, 44,  FINAL6_DL,    MOVE_NONE,     0, 0, 2  	; 
	db 181-32, 0, 44,  FINAL6_DR,    MOVE_NONE,     0, 0, 2 	; 
finalenemy7:
	db 4
	db 180, 0, 54,  FINAL7_UL,    MOVE_NONE,    55, 0, 0
	db 196, 0, 54,  FINAL7_UR,    MOVE_NONE,     0, 0, 2  	; 
	db 180, 0, 70,  FINAL7_DL,    MOVE_NONE,     0, 0, 2  	; 
	db 196, 0, 70,  FINAL7_DR,    MOVE_NONE,     0, 0, 2 	; 

; Movement definitions (copy from movement.h)

MOVE_NONE EQU 0
MOVE_LEFT EQU 1
MOVE_RIGHT EQU  2
MOVE_PINGPONG EQU 3
MOVE_EXPLOSION EQU 4
MOVE_FURBY  EQU 5		
MOVE_KAMIKAZE EQU 6
MOVE_WAVE EQU 7
MOVE_LEFTANIM EQU 8
MOVE_UPLEFT EQU 9
MOVE_UPRIGHT EQU 10
MOVE_DOWNLEFT EQU 11
MOVE_LEFT_EXPIRE EQU 12
MOVE_PINGPONG_FINAL4 EQU 13
MOVE_HOMING EQU 14
MOVE_DOWNRIGHT EQU 15
MOVE_UP EQU 16
MOVE_TARGET EQU 17
MOVE_KAMIKAZE_NOANIM EQU 18
MOVE_WAVE_NOGONELEFT  EQU 19
MOVE_MISSILE	EQU	20
MOVE_KAMIKAZE_2 EQU	21
MOVE_FOLLOW_RIGHT EQU 22
MOVE_FOLLOW_DOWN EQU 23
MOVE_FOLLOW_DOWNRIGHT EQU 24
MOVE_LEFT_ACCEL	EQU 25
MOVE_RIGHT_ACCEL	EQU 26
MOVE_LEFTANIM2	EQU 27

MOVE_SLOW	EQU 128	; only MSX!

; Enemy definitions. Each label is the sprite number where they will be loaded

ENEMY_EYE		EQU 0
ENEMY_CANNON_DOWN_LEFT	EQU 1
ENEMY_CANNON_DOWN_RIGHT EQU 2
ENEMY_CANNON_UP		EQU 3
ENEMY_1			EQU 4
ENEMY_KAMIKAZE		EQU 5
ENEMY_FURBY		EQU 6
ENEMY_FINAL4_EYE	EQU 7
ENEMY_POWERUP		EQU 8

ENEMY_SHIP1		EQU 9
ENEMY_SHIP2		EQU 10
ENEMY_SHIP3		EQU 11
ENEMY_SHIP4		EQU 12
ENEMY_TURRET		EQU 13

ENEMY_FINAL1_SHOOT	EQU 14

ENEMY_ASTEROID		EQU 15
ENEMY_TRASH1		EQU 16
ENEMY_TRASH2		EQU 17
ENEMY_TRASH3		EQU 18
ENEMY_TRASH4		EQU 19
ENEMY_TRASH5		EQU 20

ENEMY_SALTARIN		EQU 21
ENEMY_CASCO		EQU 22
ENEMY_EGG		EQU 23
ENEMY_UGLYGUY		EQU 24
ENEMY_1_BIS		EQU 25

ENEMY_SHIP2_BIS		EQU 26
ENEMY_SHIP3_BIS		EQU 27
ENEMY_SALTARIN_BIS	EQU 28
ENEMY_CASCO_BIS		EQU 29
ENEMY_EGG_BIS		EQU 30
ENEMY_UGLYGUY_BIS	EQU 31

ENEMY_SHIP1_LEVEL7	EQU	32
ENEMY_SHIP2_LEVEL7	EQU	33
ENEMY_SHIP3_LEVEL7	EQU	34
ENEMY_SHIP4_LEVEL7	EQU	35
ENEMY_TURRET_LEVEL7	EQU	36
ENEMY_PACOSHIP_1	EQU	37
ENEMY_PACOSHIP_2	EQU	38
ENEMY_FINAL1_LEVEL7_UL	EQU	39
ENEMY_FINAL1_LEVEL7_UR	EQU	40
ENEMY_FINAL1_LEVEL7_DL	EQU	41
ENEMY_FINAL1_LEVEL7_DR	EQU	42

; Final enemy definitions
FINAL1_UL	EQU 0
FINAL1_UR	EQU 1
FINAL1_DL	EQU 2
FINAL1_DR	EQU 3

FINAL2_UL	EQU 0
FINAL2_UR	EQU 1
FINAL2_DL	EQU 2
FINAL2_DR	EQU 3
FINAL2_SHIELD1	EQU 4
FINAL2_SHIELD2	EQU 5
FINAL2_SHIELD3	EQU 6

FINAL3_UL	EQU 0
FINAL3_UR	EQU 1
FINAL3_DL	EQU 2
FINAL3_DR	EQU 3

FINAL4_UP	EQU 0
FINAL4_DL	EQU 1
FINAL4_DM	EQU 2
FINAL4_DR	EQU 3

FINAL5_UL	EQU 0
FINAL5_UR	EQU 1
FINAL5_DL	EQU 2
FINAL5_DR	EQU 3

FINAL6_UL	EQU 0
FINAL6_UR	EQU 1
FINAL6_DL	EQU 2
FINAL6_DR	EQU 3
FINAL7_UL	EQU 0
FINAL7_UR	EQU 1
FINAL7_DL	EQU 2
FINAL7_DR	EQU 3
