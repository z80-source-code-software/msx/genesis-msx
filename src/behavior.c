//   Copyright 2012 Francisco Javier Peña
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "structs.h"
#include "movement.h"
#include "sprdefs.h"

#ifndef DUAL_PSG
	#define behav_asteroid	0x91A4
	#define behav_casco	0x9703
	#define behav_casco_bis	0x9752
	#define behav_egg	0x97A1
	#define behav_fenemy1	0x91B9
	#define behav_fenemy2	0x9364
	#define behav_fenemy3	0x953C
	#define behav_fenemy4	0x926A
	#define behav_fenemy5	0x97CE
	#define behav_fenemy6	0x98E0
	#define behav_fenemy7	0x9B49
	#define behav_final1_l7	0x9B04
	#define behav_follow	0x9AD4
	#define behav_none	0x8F70
	#define behav_powerup	0x90E3
	#define behav_saltarin	0x964D
	#define behav_saltarin_bis 0x96A8
	#define behav_shoot_left 0x8F71
	#define behav_shoot_left_Y 0x8F98
	#define behav_shoot_left_wait 0x8FD3
	#define behav_shoot_target 0x908F
	#define behav_shoot_target_left 0x8FFB
	#define behav_shoot_target_right 0x9045
	#define behav_turret	0x9115
	#define behav_uglyguy	0x97B9
#else
	#define behav_asteroid	0x92D9
	#define behav_casco	0x9838
	#define behav_casco_bis	0x9887
	#define behav_egg	0x98D6
	#define behav_fenemy1	0x92EE
	#define behav_fenemy2	0x9499
	#define behav_fenemy3	0x9671
	#define behav_fenemy4	0x939F
	#define behav_fenemy5	0x9903
	#define behav_fenemy6	0x9A15
	#define behav_fenemy7	0x9C7E
	#define behav_final1_l7	0x9C39
	#define behav_follow	0x9C09
	#define behav_none	0x90A5
	#define behav_powerup	0x9218
	#define behav_saltarin	0x9782
	#define behav_saltarin_bis 0x97DD
	#define behav_shoot_left 0x90A6
	#define behav_shoot_left_Y 0x90CD
	#define behav_shoot_left_wait 0x9108
	#define behav_shoot_target 0x91C4
	#define behav_shoot_target_left 0x9130
	#define behav_shoot_target_right 0x917A
	#define behav_turret	0x924A
	#define behav_uglyguy	0x98EE
#endif


void __FASTCALL__ *behavior_funcs[]={	behav_none,
					behav_shoot_left,
					behav_shoot_target,
					behav_shoot_target_left,
					behav_shoot_target_right,
					behav_powerup,
					behav_turret,
					behav_shoot_left_Y,
					behav_shoot_left_wait,
					behav_asteroid,
					behav_saltarin,
					behav_casco,
					behav_egg,
					behav_uglyguy,
					behav_saltarin_bis,
					behav_casco_bis,
					behav_follow,
					behav_final1_l7};
void __FASTCALL__  *fenemy_behavior_funcs[]={behav_fenemy1,behav_fenemy2,behav_fenemy3,behav_fenemy4,behav_fenemy5,behav_fenemy6,behav_fenemy7};
