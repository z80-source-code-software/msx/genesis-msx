;   Copyright 2012 Francisco Javier Peña
;
;   Licensed under the Apache License, Version 2.0 (the "License");
;   you may not use this file except in compliance with the License.
;   You may obtain a copy of the License at
;
;       http://www.apache.org/licenses/LICENSE-2.0
;
;   Unless required by applicable law or agreed to in writing, software
;   distributed under the License is distributed on an "AS IS" BASIS,
;   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;   See the License for the specific language governing permissions and
;   limitations under the License.

; All the engine functions
CreaTablaTiles EQU $676E
DrawMap_stage0	EQU $68C9
DrawMap_stage1	EQU $68F0
DrawMap_stage1_NTSC EQU $6916
DrawMap_stage2	EQU $6939
DrawMap_stage2_1_NTSC EQU $6964
DrawMap_stage2_2_NTSC EQU $6994
DrawMap_stage3	EQU $69B0
DrawMap_stage3_NTSC EQU $69DB
DrawMap_stage4	EQU $6A03
DrawMap_stage4_1_NTSC EQU $6A2C
DrawMap_stage4_2_NTSC EQU $6A5E
DrawMap_stage5	EQU $6A76
DrawMap_stage6	EQU $6AA0
DrawSprite   EQU $6DEA
EndSpriteDraw EQU $6E41
FILLVRM EQU $6650
INIT_ENGINE  EQU $6EA4
INSTALL_ISR  EQU $6093
LDIRVRM_fast EQU $60E3
LDIRVRM_fast_128 EQU $64EE
LDIRVRM_medium EQU $65F9
LDIRVRM_safe EQU $60AB
LDIRVRM_safe_DI EQU $60C8
LoadSprBlock EQU $6E86
PrintLargeNumber EQU $6F06
PrintLarge_2	EQU $6F2A
PrintLarge_5	EQU $6F45
PrintSmall_5	EQU $6F79
ROMbank1	  EQU $C04C
ROMbank2	  EQU $C04D
ROMbank3	  EQU $C04E
TransferSprites EQU $6E68
VDP_ActDeact EQU $6662
WRTVDP_DI EQU $668A
WRTVRM EQU $666E
WRTVRM_DI EQU $667D
curspr	   EQU $C05B
enableSLOT2  EQU $66C4
enableSLOT2_EI  EQU $66CC
get_joystick EQU $6009
get_keyboard EQU $6024
initROMbanks EQU $6693
msxHZ	EQU $C049
searchramnormal80 EQU $66D6
setROM2	  EQU $66A4
setROM2_DI	  EQU $66AD
setROM3	  EQU $66B4
setROM3_DI	  EQU $66BD
; And some defines

KEY_7			EQU $8000
KEY_6			EQU $4000
KEY_5			EQU $2000
KEY_4			EQU $1000
KEY_3			EQU $0800
KEY_2			EQU $0400
KEY_1			EQU $0200
KEY_0			EQU $0100
KEY_COLON		EQU $8001
KEY_CLOSEBRACKET	EQU $4001
KEY_OPENBRACKET		EQU $2001
KEY_BACKSLASH		EQU $1001
KEY_EQUAL		EQU $0501
KEY_DASH		EQU $0401
KEY_9			EQU $0201
KEY_8			EQU $0101
KEY_B			EQU $8002
KEY_A			EQU $4002
KEY_DEAD		EQU $2002
KEY_FORWARDSLASH	EQU $1002
KEY_DOT			EQU $8002
KEY_COMMA		EQU $4002
KEY_TILDE		EQU $2002
KEY_APOSTROPHE		EQU $1002	
KEY_J			EQU $8003
KEY_I			EQU $4003
KEY_H			EQU $2003
KEY_G			EQU $1003
KEY_F			EQU $0803
KEY_E			EQU $0403
KEY_D			EQU $0203
KEY_C			EQU $0103
KEY_R			EQU $8004
KEY_Q			EQU $4004
KEY_P			EQU $2004
KEY_O			EQU $1004
KEY_N			EQU $0804
KEY_M			EQU $0404
KEY_L			EQU $0204
KEY_K			EQU $0104
KEY_Z			EQU $8005
KEY_Y			EQU $4005
KEY_X			EQU $2005
KEY_W			EQU $1005
KEY_V			EQU $0805
KEY_U			EQU $0403
KEY_T			EQU $0205
KEY_S			EQU $0105
KEY_F3			EQU $8006
KEY_F2			EQU $4006
KEY_F1			EQU $2006
KEY_CODE		EQU $1006
KEY_CAPS		EQU $0806
KEY_GRAPH		EQU $0406
KEY_CTRL		EQU $0206
KEY_SHIFT		EQU $0106
KEY_RET			EQU $8007
KEY_SELECT		EQU $4007
KEY_BACKSPACE		EQU $2007
KEY_STOP		EQU $1007
KEY_TAB			EQU $0807
KEY_ESC			EQU $0407
KEY_F5			EQU $0207
KEY_F4			EQU $0107
KEY_RIGHT		EQU $8008
KEY_DOWN		EQU $4008
KEY_UP			EQU $2008
KEY_LEFT		EQU $1008
KEY_DEL			EQU $0808
KEY_INS			EQU $0408
KEY_HOME		EQU $0208
KEY_SPACE		EQU $0108
KEY_NUM4		EQU $8009
KEY_NUM3		EQU $4009
KEY_NUM2		EQU $2009
KEY_NUM1		EQU $1009
KEY_NUM0		EQU $0809
KEY_NUMSLASH		EQU $0409
KEY_NUMPLUS		EQU $0209
KEY_NUMASTERISK		EQU $0109
KEY_NUMDOT		EQU $800A
KEY_NUMCOMMA		EQU $400A
KEY_NUMMINUS		EQU $200A
KEY_NUM9		EQU $100A
KEY_NUM8		EQU $080A
KEY_NUM7		EQU $040A
KEY_NUM6		EQU $020A
KEY_NUM5		EQU $010A
