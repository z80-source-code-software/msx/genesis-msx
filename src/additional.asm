;   Copyright 2012 Francisco Javier Peña
;
;   Licensed under the Apache License, Version 2.0 (the "License");
;   you may not use this file except in compliance with the License.
;   You may obtain a copy of the License at
;
;       http://www.apache.org/licenses/LICENSE-2.0
;
;   Unless required by applicable law or agreed to in writing, software
;   distributed under the License is distributed on an "AS IS" BASIS,
;   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;   See the License for the specific language governing permissions and
;   limitations under the License.

org $6000+6144

include "maindefs.asm"
include "engine-functions.asm"

;enableSLOT2  		EQU	$66C4
;enableSLOT2_EI   	EQU	$66CC


DoShoot:
	; increment frames_fire_pressed, unless it is 16 already

	ld a, (_frames_fire_pressed)
	inc a
	cp 17
	jr nz, store_frames_fire_pressed
	dec a
store_frames_fire_pressed:
	ld (_frames_fire_pressed), a

	ld a, (_frames_to_shoot)
	and a
	ret nz


	ld a, 1
	ld (_dummy_b), a
	ld a, (_current_weapon)
	and a
	jp z, shot_case0
	dec a
	jp z, shot_case1
	dec a
	jp z, shot_case2
	dec a
	jp z, shot_case3
	dec a
	jp z, shot_case4
	dec a
	jp nz, shot_noshoot
shot_case5:			; Megashot
	call AvailShoots
	cp 4
	jp c, shot_noshoot	; less than 4 shoots available
	; Play sound
	ld a, (_slot2address)
	call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!
	ld hl, FX_DISPARO_MULTI
	push hl
        call _wyz_effect
        pop hl
    	ld  a,(_RAMbank)
	call enableSLOT2_EI	; Enable RAM in $8000 - $BFFF, re-enable interrupts

	ld a, SHOT_MEGA
	ld (_current_weapon_sprite), a
	ld a, (_ship_x)
	add a, 20
	ld h, 0
	ld l, a
	push hl
	ld a, (_ship_y)
	add a, 6
	ld l, a
	push hl
	call _NewShoot		;NewShoot(ship_x+20, ship_y)	
	pop de
	pop hl

	ld a, SHOT_MEGA
	ld (_current_weapon_sprite), a
	ld a, (_ship_x)
	add a, 20
	ld h, 0
	ld l, a
	push hl
	ld a, (_ship_y)
	sub 6
	jr c, case5_skip_shot2
	ld l, a
	push hl
	call _NewShoot		;NewShoot(ship_x+20, ship_y)	
	pop de
case5_skip_shot2:
	pop hl

	ld a, SHOT_MEGA
	ld (_current_weapon_sprite), a
	ld a, (_ship_x)
	add a, 20
	ld h, 0
	ld l, a
	push hl			; ship_x+20
	ld a, (_ship_y)
	sub 12
	jr c, case5_skip_shot3
	ld l, a			; ship_y-12
	push hl
	call _NewShoot		;NewShoot(ship_x+20, ship_y-12)
	ld a, l
	or h
	jr z, case5_shot3_not_created	; if the shot was not created, skip
	inc hl
	inc hl
	inc hl
	inc hl
	ld (HL), MOVE_UPRIGHT
case5_shot3_not_created:
	pop bc
case5_skip_shot3:
	pop bc

	ld a, (_ship_x)
	add a, 20
	ld h, 0
	ld l, a
	push hl			; ship_x+20
	ld a, (_ship_y)
	add a, 12		; ship_y +12
	ld l, a
	push hl
	call _NewShoot		;NewShoot(ship_x+20, ship_y+12)
	ld a, l
	or h
	jr z, case5_shot4_not_created	; if the shot was not created, skip
	inc hl
	inc hl
	inc hl
	inc hl
	ld (HL), MOVE_DOWNRIGHT
	inc hl
	inc hl
	inc hl
	ld (hl), 8		; e->param2=8
case5_shot4_not_created:
	pop de
	pop bc

	jp shot_endcase
shot_case0:	; Basic shot
	call AvailShoots
	and a
	jp z, shot_noshoot	; no available shoots
	ld a, (_ship_x)
	add a, 20
	ld h, 0
	ld l, a
	push hl
	ld a, (_ship_y)
	ld l, a
	push hl
	call _NewShoot		;NewShoot(ship_x+20, ship_y)
	pop hl
	pop hl
	; Play sound
	; Play sound
	ld a, (_slot2address)
	call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!
	ld hl, FX_SINGLE_SHOOT
	push hl
        call _wyz_effect
        pop hl
    	ld  a,(_RAMbank)
	call enableSLOT2_EI	; Enable RAM in $8000 - $BFFF	, re-enable interrupts
	jp shot_endcase
shot_case1:			; triple shoot
	call AvailShoots
	cp 3
	jp c, shot_noshoot	; less than 3 shoots available
	ld a, (_ship_x)
	add a, 20
	ld h, 0
	ld l, a
	push hl
	ld a, (_ship_y)
	sub 12
	jr c, case1_skip_shot1
	ld l, a
	push hl
	call _NewShoot		;NewShoot(ship_x+20, ship_y-12)
	pop hl
case1_skip_shot1:
;	pop hl

;	ld a, (_ship_x)
;	add a, 20
;	ld h, 0
;	ld l, a
;	push hl
	ld a, (_ship_y)
	add a, 12
	ld l, a
	push hl
	call _NewShoot		;NewShoot(ship_x+20, ship_y+12)
	pop hl
;	pop hl

;	ld a, (_ship_x)
;	add a, 20
;	ld h, 0
;	ld l, a
;	push hl
	ld a, (_ship_y)
	ld l, a
	push hl
	call _NewShoot		;NewShoot(ship_x+20, ship_y)
	pop hl
	pop hl
	; Play sound
	; Play sound
	ld a, (_slot2address)
	call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!
	ld hl, FX_TRIPLE_SHOOT
	push hl
        call _wyz_effect
        pop hl
    	ld  a,(_RAMbank)
	call enableSLOT2_EI	; Enable RAM in $8000 - $BFFF	, re-enable interrupts

	jp shot_endcase
shot_case2:		; Laser
	call AvailShoots			
	cp 2
	jp c, shot_noshoot	; less than 2 shoots available
	ld a, (_ship_x)
	add a, 20
	ld h, 0
	ld l, a
	push hl
	ld a, (_ship_y)
	ld l, a
	push hl
	call _NewShoot		;NewShoot(ship_x+20, ship_y)
	pop hl
	pop hl
	; Play sound
	; Play sound
	ld a, (_slot2address)
	call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!
	ld hl, FX_LASER
	push hl
        call _wyz_effect
        pop hl
    	ld  a,(_RAMbank)
	call enableSLOT2_EI	; Enable RAM in $8000 - $BFFF	, re-enable interrupts

	ld a, (_ship_x)
	add a, 36
	cp 208
	jr nc, case2_skip_shot2
	ld h, 0
	ld l, a
	push hl
	ld a, (_ship_y)
	ld l, a
	push hl
	call _NewShoot		;NewShoot(ship_x+36, ship_y)
	pop hl
	pop hl
case2_skip_shot2:
	jp shot_endcase
shot_case3:		; homing missile
	call AvailShoots
	and a
	jp z, shot_noshoot	; no available shoots
	ld a, (_ship_x)
	add a, 20
	ld h, 0
	ld l, a
	push hl
	ld a, (_ship_y)
	ld l, a
	push hl
	call _NewShoot		;NewShoot(ship_x+20, ship_y)
	pop de
	pop de			; HL holds the pointer to the new shoot
	ld de, 4
	add hl, de		; HL points to e->movement
	ld (hl), MOVE_HOMING
	; now go find the first available active enemy
	exx
	ld c, 0			; C will serve as counter for the enemies
	ld hl, _active_enemies+2 ; go for the sprnums
	ld de, 12		; sizeof (struct Entity)
loop_case3:
	ld a, (hl)
	and a
	jr nz, found_case3
	add hl, de
	inc c 
	ld a, MAX_ENEMIES
	cp c					
	jr nz, loop_case3
	ld c, 0			; we got out of the loop without finding any active enemies
found_case3:
	ld a, c
	exx
	inc hl
	inc hl
	inc hl
	ld (hl), a		; param2
	; Play sound
	; Play sound
	ld a, (_slot2address)
	call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!
	ld hl, FX_DISPARO_HOMMING
	push hl
        call _wyz_effect
        pop hl
    	ld  a,(_RAMbank)
	call enableSLOT2_EI	; Enable RAM in $8000 - $BFFF	, re-enable interrupts
	jp shot_endcase
shot_case4:				; bomb
	call AvailShoots
	cp 2
	jp c, shot_noshoot	; less than 2 shoots available
	; Play sound
	; Play sound
	ld a, (_slot2address)
	call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!
	ld hl, FX_DISPARO_HOMMING
	push hl
        call _wyz_effect
        pop hl
    	ld  a,(_RAMbank)
	call enableSLOT2_EI	; Enable RAM in $8000 - $BFFF	, re-enable interrupts

	ld a, SHOT_BASIC
	ld (_current_weapon_sprite), a
	ld a, (_ship_x)
	add a, 20
	ld h, 0
	ld l, a
	push hl
	ld a, (_ship_y)
	ld l, a
	push hl
	call _NewShoot		;NewShoot(ship_x+20, ship_y)
	pop de
	pop de

	ld a, SHOT_BOMB
	ld (_current_weapon_sprite), a
	ld a, (_ship_x)
	add a, 10
	ld h, 0
	ld l, a
	push hl
	ld a, (_ship_y)
	add a, 10
	ld l, a
	push hl
	call _NewShoot		;NewShoot(ship_x, ship_y+20)
	pop de
	pop de
	; HL now holds the received entity
        ld a, h
        or l
        jr z, shot_endcase	; If the new shoot is NULL, do not touch
	ld bc, 4
	add hl, bc
	ld (hl), MOVE_DOWNRIGHT
	inc hl
	inc hl
	ld (hl), 4
	inc hl
	ld (hl), 0
	jp shot_endcase
shot_noshoot:
	xor a
	ld (_dummy_b), a
shot_endcase:
	ld a, (_dummy_b)
	and a
	jp z, dummyb_zero
	ld a, (_joy)
	and 3		; JOY_UP | JOY_DOWN
	jr nz, dummyb_zero
	ld a, 6
	ld (_ship0spr),a 
dummyb_zero:
	ld a, (_frames_fire_pressed)
	cp 10
	jr c, lessthan10
	ld a, 10
	ld (_frames_to_shoot), a
	ret
lessthan10:
	ld a, 3
	ld (_frames_to_shoot), a
	ret		

; Calculate the number of available shoots

AvailShoots:
	ld hl, _my_active_shoots+2
	ld a, (_max_shoots)
	ld b, a
	ld c, 0
	ld de, 12	; sizeof (struct Entity)
availshoots_loop:
	ld a, (hl)
	and a
	jr nz, shoot_active
	inc c
shoot_active:
	add hl, de
	djnz availshoots_loop
	ld a, c
	ret
