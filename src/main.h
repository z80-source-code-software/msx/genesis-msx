//   Copyright 2012 Francisco Javier Peña
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MAIN_H
#define MAIN_H

extern unsigned char joystick_type;
extern unsigned char joy;
extern unsigned char RAMbank;
extern unsigned int dummy_i;
extern unsigned char dummy_b;
extern unsigned char i;
extern unsigned char new_enemy;
extern char shoot_xchar, shoot_ychar;

//load_level
extern unsigned char *level_pointer;
extern unsigned int *int_pointer;
extern unsigned int length_tiles;
extern unsigned int length_map;

// Level variables
extern unsigned char CurLevel_XLength;		// Length in X of the current level
extern unsigned char CurLevel_NTiles;		// Number of tiles for the current level
extern unsigned char map_xpos;
extern unsigned char map_displacement;  // Displacement in tile: 0000YYXX, where XX is the displacement in pixels (0, 1==2, 2==4, 3==6), and YY is the displacement in chars (0-2).
extern unsigned char current_level;	// level we are playing
extern unsigned char previous_level;	// level we were at before getting killed or moved to a new level
extern unsigned char credit_counter;		// Start with 3 credits!
extern unsigned char end_game;			// Game will be really over at this time

// Game loop state variables
extern unsigned char draw_stage;
extern unsigned char in_game;          // Boolean: if 1, process game during the ISR. If 0, do not!

// Game variables
extern unsigned char ship_x;
extern unsigned char ship_y;
extern char speed_x;
extern char speed_y;
extern unsigned char frames_to_shoot;		// Can we shoot now?
extern unsigned char frames_fire_pressed;	// Number of frames where the FIRE button is pressed: when pressed for 6 frames, we will launch the BFB (if available)
extern unsigned char current_weapon;		// What is our current weapon?
extern unsigned char current_weapon_sprite;	// Sprite for our current weapon
extern unsigned char current_weapon_energy;	// Energy for the current weapon
extern unsigned char available_superbombs;	// Number of available superbombs (kill everything on screen)
extern unsigned char current_screen;
extern unsigned char max_shoots; 	// Maximum number of shoots allowed with the current weapon
extern unsigned char mayday;		// We have been shot!
extern unsigned int score;		// Need we say more?
extern unsigned int hiscore;		// Highest score
extern unsigned int next_extralife;	// Score to get the next extra life
extern unsigned char life_counter;		// How many lifes do we have?
extern unsigned char respawn_xpos;		// xpos to respawn after dying
extern unsigned char border_color;		// For some silly effects
extern unsigned char update_score;	// We must update the score at the next frame or two
extern unsigned char update_superb;	// We must update the number of superbombs
extern unsigned char update_life;	// We must update the number of lifess

// Ship sprites, used for dying animation
extern unsigned char ship0spr;

// Array of existing enemies and shoots (max 8 enemies for now)
extern struct Entity active_enemies[MAX_ENEMIES];
extern struct Entity my_active_shoots[MAX_ENEMIES];
extern struct Entity enemy_active_shoots[MAX_ENEMIES];
extern struct Entity power_up;			// Only one powerup active at a time...

// Final enemy
extern unsigned char final_enemy_active;
extern unsigned char final_enemy_components;		// How many sprites in the enemy
extern unsigned char fenemy_defeat;		// Have we beaten it?
extern unsigned char fenemy_activation_counter;

// Music
extern unsigned char current_song;
extern unsigned char *numbers;	// 5 chars is the maximum number of digits we will print
extern unsigned char DELAY60Hz;
// Sprite colors
extern unsigned char *sprite_colors; // 64 bytes allocated
extern unsigned char blink_color;

#endif
