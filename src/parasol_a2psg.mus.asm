;   Copyright 2012 José Vicente Masó
;
;   Licensed under the Apache License, Version 2.0 (the "License");
;   you may not use this file except in compliance with the License.
;   You may obtain a copy of the License at
;
;       http://www.apache.org/licenses/LICENSE-2.0
;
;   Unless required by applicable law or agreed to in writing, software
;   distributed under the License is distributed on an "AS IS" BASIS,
;   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;   See the License for the specific language governing permissions and
;   limitations under the License.


PAUTA_1:	DB	$20+11,0,10,1,10,-1,9,0,9,0,8,0,129
PAUTA_2:	DB	8,0,74,0,11,0,43,0,10,0,72,0,8,0,40,0,8,0,132
PAUTA_3:	DB	4,0,71,0,8,0,40,0,8,0,70,0,5,0,37,0,5,0,69,0,132
PAUTA_4:	DB	$40+9,0,11,0,11,0,10,0,9,0,8,0,7,0,7,-1,7,0,7,0,7,1,7,0,7,0,7,1,7,0,7,-1,128+10
PAUTA_5:	DB	$40+7,0,7,0,6,0,6,0,5,-1,5,0,5,0,5,0,4,1,4,0,4,0,4,0,128+8
PAUTA_6:	DB	7,0,8,0,9,0,8,0,7,0,6,0,129
PAUTA_7:	DB	68,0,5,0,5,0,4,0,3,0,129

SONIDO1:	DB	209,62,0,69,171,0,255
SONIDO2:	DB	23,61,0,93,90,6,0,9,3,255
SONIDO3:	DB	0,10,1,0,6,1,255
SONIDO4:	DB	186,58,0,0,102,0,162,131,0,255
SONIDO5:	DB	12,63,0,186,108,6,186,185,4,255
