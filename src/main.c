//   Copyright 2012 Francisco Javier Peña, Jaime Tejedor, Dan Weiss
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#include "engine.h"
#include "structs.h"
#include "constants.h"
#include "movement.h"
#include "behavior.h"
#include "main.h"

extern unsigned char slot2address;

//#define CHEAT  // Uncomment to enable god mode
#define JUMPTOLEVEL // Uncomment to allow level selection from the main menu


// Indirect call to the address stored in IY
#asm
.IndCall
	jp (IY)


;Divide 8-bit values
;In: Divide E by divider D
;Out: A = result, D = rest
;
.Div8
    xor a
    ld b,8
.Div8_Loop
    rl e
    rla
    sub d
    jr nc,Div8_NoAdd
    add a,d
.Div8_NoAdd
    djnz Div8_Loop
    ld d,a
    ld a,e
    rla
    cpl
    ret
#endasm


int __FASTCALL__ read_joystick(int joytype)
{
#asm
	ld a, l
	call get_joystick
	ld h,0
	ld l,a
#endasm
}

// WYZ player functions

void wyz_load_music (unsigned char mzk_number)
{
#asm
	        ld hl, 2
		add hl, sp
		ld a, (hl)              ; A gets the song number
		ld (_current_song), a	; current song

		ld a, (ROMBank2)
		push af
		; Set the player bank 6 in 0x8000
		ld a, 6
		call setROM2	

		ld a, (_current_song)	; get the song number
		ld hl, _rombank_music
		ld c, a
		ld b, 0	
		add hl, bc
		ld a, (hl)
		call setROM3		; Set the music bank in $A000

		di
		ld a, (_current_song)
		call CARGA_CANCION	
		ei

		ld a, 3
		ld (SOUND_SFX), a	; by default we select sound and sfx, unless changed later

		ld a, (_current_song)
		ld c, a			; C gets the song number
		ld b, 0

		ld hl, _fxchannel_music
		add hl, bc
		ld a, (hl)
		ld (FX_CHANNEL),a
		call STOP_FX
		pop af
		call setROM2	
#endasm

}

void wyz_stop_music(void)
{
#asm
	ld a, (ROMBank2)
	push af
	; Set the player bank 6 in 0x8000
	ld a, 6
	call setROM2	
	di	
	CALL STOP_PLAYER
	ei
	halt

	pop af
	call setROM2
#endasm

}

void wyz_effect(unsigned char effect)
{
     #asm
	ld a, (ROMBank2)
	push af
	; Set the player bank 6 in 0x8000
	ld a, 6
	call setROM2	

        ld hl, 4
	add hl, sp
	ld b, (hl)              ; B gets the effect number
        call LOAD_FX

	pop af
	call setROM2
     #endasm
}

// Same as before, to be called during the ISR and avoid getting out at EI
void wyz_effect_noint(unsigned char effect)
{
     #asm
	ld a, (ROMBank2)
	push af
	; Set the player bank 6 in 0x8000
	ld a, 6
	call setROM2_DI

        ld hl, 4
	add hl, sp
	ld b, (hl)              ; B gets the effect number
        call LOAD_FX

	pop af
	call setROM2_DI
     #endasm
}

// Load a block of sprites to the current working set
// dstspr (0-63): first sprite to load
// sourcespr (0-448): source to load from
// numspr (1-64): number of sprites to load
// Interrupts are assumed to be disabled on entry

void LoadSprBlock(unsigned char dstspr, unsigned int sourcespr, unsigned char numspr)
{
#asm

;	ld a, (ROMBank2)
;	push af
	; Set the sprites bank 4 in 0x8000
	ld a, 4
	call setROM2_DI	

	ld hl, 6
	add hl, sp
	ld b, (hl)		; B gets dstspr
	dec hl
	dec hl
	ld e, (hl)
	dec hl
	ld d, (hl)		; DE gets sourcespr
	dec hl
	ld c, (hl)		; C gets numspr
	

	; We need to calculate sourcespr in HL, as $8000 + DE*32
	ld h, d
	ld l, e			; HL gets sourcespr now
	add hl, hl
	add hl, hl
	add hl, hl
	add hl, hl
	add hl, hl		; HL * 32
	ld de, $8000
	add hl, de		; HL = sourcespr*32+$8000
	
	call LoadSprBlock

; Load sprite colors
	ld hl, 6
	add hl, sp
	ld b, (hl)		; B gets dstspr
	dec hl
	dec hl
	ld e, (hl)
	dec hl
	ld d, (hl)		; DE gets sourcespr
	dec hl
	ld c, (hl)		; C gets numspr


	ld h, $9d
	ld a, e
	ld l, a			; HL points to the first color in the list
	ex de, hl		; DE points to the first color in the list
	ld hl, _sprite_colors
	ld a, c
	ld c, b
	ld b, 0
	add hl, bc		; HL points to the first destination color, A has numspr
	ex de, hl
	ld c, a
        ldir			; copy the colors to the destination



;	pop af
;	call setROM2
#endasm
}

// Generate the enemies starting from "first"... Useful for final enemies

struct Entity * NewEnemy(struct Enemy *e, unsigned char first)
{
#asm
	ld hl, 2
	add hl, sp
	ld b, (hl)		; B = first
	inc hl
	inc hl
	ld e, (hl)
	inc hl
	ld d, (hl)
	ld ixh, d
	ld ixl, e		; IX = e
	ld hl, _active_enemies-12
	ld de, 12
	ld a, b			; Save first in A
	inc b			
.gotofirst
	add hl, de
	djnz, gotofirst

	ld b, a
	inc hl
	inc hl
.findemptyenemy
	ld a, (hl)
	and a
	jp nz, enemynotempty
; Empty slot found, populate it!
	dec hl
	dec hl			; point to e->X

	push hl			; Save HL
	ld a, (_map_xpos)
	neg
	add a, (ix+0)		; A = e->x - map_xpos
	ld l, a
	ld h, 0
	xor a
	rl l
	rl h
	rl l
	rl h
	rl l
	rl h			; HL = HL*8
	ld d, h
	ld e, l
	add hl, de
	add hl, de		; HL = HL*24, (e->x - map_xpos) * 24)
	ld e, (ix+1)
	ld d, 0			; DE=e->e_xdesp
	add hl, de		; HL = (e->x - map_xpos) * 24) + e->x_desp
	ld a, (_map_displacement)
	rlca			; the highest bit of map_displacement is always 0, so in effect it is << 1
	ld e, a
	sbc hl, de		; HL = ((e->x - map_xpos) * 24) + e->x_desp - (map_displacement<<1), it should be less than 256
	ex de, hl
	pop hl
	ld a, e
	add a, 10
	ld (hl), a		;  active_enemies[i].x=((e->x - map_xpos) * 24) + e->x_desp - (map_displacement<<1) + 10;
	inc hl
	ld a, (ix+2)
	ld (hl), a		;  active_enemies[i].y=e->y;
	inc hl
	push hl
	ld hl, _enemy_sprites
	ld a, (ix+3)		; e->enemy_type
	ld e, a
	ld d, 0
	add hl, de
	ld a, (hl)		; A = enemy_sprites[e->enemy_type]
	pop hl
	ld (hl), a		; active_enemies[i].sprnum=enemy_sprites[e->enemy_type];
	inc hl
	ld a, (ix+3)		; e->enemy_type
	ld (hl), a		; active_enemies[i].type= e->enemy_type;
	inc hl
	ld a, (ix+4)		; e->movement
	ld (hl), a		; active_enemies[i].movement=e->movement;
	inc hl
	ld a, (ix+5)	
	ld (hl), a		; active_enemies[i].energy = e->energy;
	inc hl
	ld a, (ix+6)
	ld (hl), a		; active_enemies[i].param1=e->param1;
	inc hl
	ld a, (ix+7)
	ld (hl), a		; active_enemies[i].param1=e->param2;
	xor a
	inc hl
	ld (hl), a		; active_enemies[i].param3=0;
	inc hl
	ld (hl), a		; active_enemies[i].param4=0;
	inc hl
	ld a, (ix+3)		; e->enemy_type
	push hl
	ld hl, _behavior_types
	ld e, a
	add hl, de
	ld a, (hl)		; A = behavior_types[e->enemy_type];
	pop hl
	ld (hl), a		; active_enemies[i].behavior = behavior_types[e->enemy_type];
	inc hl
	xor a
	ld (hl), a		; active_enemies[i].behav_param=0;
	ld de, -11
	add hl, de
	ret			; return &(active_enemies[i])

.enemynotempty
	ld de, 12
	add hl, de
	inc b
	ld a, MAX_ENEMIES-1
	cp b
	jp nc, findemptyenemy

	ld hl, 0		; return 0 if no empty enemy was found
#endasm

}



void activate_final_enemy(void)
{
	dummy_b =  level_pages[current_level];


    if(!fenemy_activation_counter)
     {
#asm
	di
	ld a, (_dummy_b) 
	ld (ROMBank1), a
	ld ($6000), a		; Place compressed maps in $6000 - $7FFF
#endasm
	level_pointer=(unsigned char*)finalenemy_address[current_level];
	dummy_i = final_enemy_components = *(level_pointer)++;	// Load the basic final enemy data        
        dummy_i *= sizeof (struct Enemy); // Copy to enemy table (we are overwriting, which should not be a problem)

#asm
	ld de, (_enemy_locations)
	ld hl, (_level_pointer)
        ld bc, (_dummy_i)
        ldir

	ld a, (_final_enemy_components)
	ld b, a
	ld hl, _active_enemies
	ld de, (_enemy_locations)
.initfenemyloop
	ld a, (de)		; fenemy->x
	ld (hl), a		; active_enemies[i].x=fenemy->x;
	inc hl
	inc de
	inc de
	ld a, (de)		; fenemy->y
	ld (hl), a		; active_enemies[i].y=fenemy->y;
	inc hl
	inc de
	ld (hl), EXPLOSION_SPR	;active_enemies[i].sprnum=EXPLOSION_SPR;	
	inc hl
	ld a, (de)		; fenemy->enemy_type
	ld (hl),a		; active_enemies[i].type= fenemy->enemy_type;
	inc hl
	inc de	
	ld (hl), MOVE_EXPLOSION		; active_enemies[i].movement=MOVE_EXPLOSION;
	inc hl
	inc de
;	ld a, (de)		; 	fenemy->energy;
;	ld (hl), a		; active_enemies[i].energy = fenemy->energy;
	ld (hl), 0		; active_enemies[i].energy = 0, so the enemy is only vulnerable after the explosions
	inc hl
	inc de
	ld (hl), 4		;active_enemies[i].param1=4;  
	inc hl
	inc de
	ld a, (de)
	ld (hl), a		; active_enemies[i].param2=fenemy->param2;
	inc hl
	inc de			; DE now points to the next enemy
	ld (hl), 0		; active_enemies[i].param3=0;
	inc hl
	ld (hl), 0		; active_enemies[i].param4=0;
	inc hl
	ld (hl), BEHAV_DO_NOTHING ;	 active_enemies[i].behavior = BEHAV_DO_NOTHING;
	inc hl
	ld (hl), 0		; active_enemies[i].behav_param=0;
	inc hl			; HL now points to the next enemy
	djnz initfenemyloop

	ld a, 1
	ld (ROMBank1), a
	ld ($6000), a		; Place engine in $6000 - $7FFF
#endasm

 // Load the sprite block for the final enemy
#asm
	ld a, (_slot2address)
	call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!
#endasm

 LoadSprBlock(22,finalspr[current_level],finalspr_count[current_level]);
 wyz_stop_music();
 wyz_load_music(finalenemy_music[current_level]);
#asm
 ld a, (sound_selection)
 ld (SOUND_SFX), a
 ld  a,(_RAMbank)
 call enableSLOT2_EI	; Enable RAM in $8000 - $BFFF	
#endasm

    }

#asm
	ld a, (_fenemy_activation_counter)
	inc a 
	ld (_fenemy_activation_counter), a
	and $3
	jp nz, checkifcounteris60
	ld a, (_final_enemy_components)
	ld b, a
	ld ix, _active_enemies
	ld de, 12
.renewexplosion_counter
	ld (ix+2), EXPLOSION_SPR	; exploding
	ld (ix+4), MOVE_EXPLOSION
	ld (ix+6), 4			; 4 frames to explode
	add ix, de
	djnz renewexplosion_counter
.checkifcounteris60
#endasm

     if (fenemy_activation_counter == 20)
     {
#asm
	di
	ld a, 1
	ld (ROMBank1), a
	ld ($6000), a		; Place engine in $6000 - $7FFF
 	ld  a,(_RAMbank)
	call enableSLOT2	; Enable RAM in $8000 - $BFFF	
	ld a, (_dummy_b) 
	ld (ROMBank1), a
	ld ($6000), a		; Place compressed maps in $6000 - $7FFF
#endasm

#asm
	ld a, (_final_enemy_components)
	ld b, a
	ld hl, _active_enemies
	ld ix, (_enemy_locations)
.initfenemyloop_part2
	inc hl
	inc hl
	push hl
	ld hl, _finalenemy_sprites

	ld a, (ix+3)		; fenemy->enemy_type
	ld e, a
	ld d, 0
	add hl, de
	ld a, (hl)		; A = finalenemy_sprites[fenemy->enemy_type];
	pop hl
	ld (hl), a		; active_enemies[i].sprnum=finalenemy_sprites[fenemy->enemy_type];
	inc hl
	inc hl
	ld a, (ix+4)
	ld (hl), a		; active_enemies[i].movement=fenemy->movement;
	inc hl
	ld a, (ix+5)
	ld (hl), a		; active_enemies[i].energy=fenemy->energy;
	inc hl
	ld a, (ix+6)
	ld (hl), a		; active_enemies[i].param1=fenemy->param1;
	ld de, 8
	add ix, de		; enemy_locations[i+1]
	ld de, 6
	add hl, de		; active_enemies[i+1]
	djnz initfenemyloop_part2
#endasm

	final_enemy_active=1;
#asm
	ld a, 1
	ld (ROMBank1), a
	ld ($6000), a		; Place engine in $6000 - $7FFF
	ei
#endasm
     }
}

// Show the bomb bar
// frames_fire_pressed goes from 0 to 16



void ShowBombBar(void)
{
#asm
	; ẗhe position on VRAM is $1800+
	; The tile to store is 84 + loquesea
	ex af, af
	ld hl,$1ad9
	ld a, 128+84		
	ex af, af

	ld a, (_frames_fire_pressed)
	and a
	jr z, bar_off
	ld e, a
	ld d, 3
	call Div8			; A now holds the number of filled chars
	inc a


.bar_onloop
	ex af, af
	push hl
	push af
	call WRTVRM_DI
	pop af
	pop hl
	inc a
	inc hl
	ex af, af
	dec a
	jr nz, bar_onloop
.bar_off
	ld a, (_frames_fire_pressed)
	and a
	jr z, bar_all_off
	ld e, a
	ld d, 3
	call Div8			; A now holds the number of filled chars
	sub 5
	neg
	and a
	ret z				; return if no unfilled chars
	jr bar_off_cont
.bar_all_off
	ld a, 6
.bar_off_cont
	; ẗhe position on VRAM is $1800+
	; The tile to store is 84 + loquesea
	ex af, af
	add a, 6
	ex af, af
.bar_offloop
	ex af, af
	push hl
	push af
	call WRTVRM_DI
	pop af
	pop hl
	inc a
	inc hl
	ex af, af
	dec a
	jr nz, bar_offloop
#endasm
}


// Go to every enemy on screen and substract 5 to its energy
// Sould kill them all!

void Do_Superbomb(void)
{
#asm
	ld a, 15
	ld (_border_color), a		; set border effect ; FIXME 

	ld b, MAX_ENEMIES
	ld hl, _active_enemies+2
	ld de, 12			; sizeof (struct enemy)

.superbomb_loop
	ld a, (hl)
	and a
	jr z, continue_loop_superbomb

	inc hl
	inc hl
	inc hl				; energy			
	ld a, (hl)		
	and a				; If energy is 0, ignore (not killable or already dead)
	jr z, superbomb_shot_completed
	sub 5
	jr c, superbomb_enemy_dead
	jr z, superbomb_enemy_dead	; if the energy is 0 or < 0, it is dead
	ld (hl), a			; new energy
	jr superbomb_shot_completed
.superbomb_enemy_dead
	ld (hl), 0			; enemy is dead
        inc hl                               ; param1
        ld (hl), 4
	dec hl
	dec hl					; movement
        ld (hl), MOVE_EXPLOSION
	dec hl
	dec hl                             ; sprnum
        ld (hl), EXPLOSION_SPR
.continue_loop_superbomb
	add hl, de
	djnz superbomb_loop
	ret
.superbomb_shot_completed
	dec hl
	dec hl
	dec hl
	jr continue_loop_superbomb		; make sure we are at energy before adding 12
#endasm
}

void gameISR(void)
{
#asm
        ld a, (_in_game)
        and a
        jp z, not_in_game

/* Tried to do a screen split... but it took too much CPU time :( */

/*	; First thing, set palette for MSX2 machines
	ld c, 7
	ld b, 2
	call WRTVDP_DI
	ld a, (_current_level)
	call SetLevelPalette
	ld c, 7
	ld b, 0
	call WRTVDP_DI*/

    	ld  a,(_RAMbank)
	call enableSLOT2	; Enable RAM in $8000 - $BFFF

        ; As a first measure at all, draw sprites!
      	call TransferSprites	; Transfer sprites to VRAM

	ld a, (_CurLevel_XLength)
	ld h,0
	ld l,a
	ld d, $80
	ld a, (_map_xpos)
	ld e, a				; the map will always start at $8000, so the displacement will always be in E
	ld a, (_map_displacement)
	and $03
	ld c, a				; displacement in pixels within tile
	ld a, (_map_displacement)
	and $0C
	rrca
	rrca
	ld b, a				; displacement in chars within tile


	ld a, (msxHZ)
	cp 60
	jp nz, mapdraw50hz		; this is a 50 Hz MSX, go to the other side

.mapdraw60hz				; for NTSC machines, we draw the map in 9 steps (60/9 == 6.7fps)
					; to continue moving enemies and ship at the same rate (approx 21.5 fps) we will not do any processing in 2 steps
					; this will make processing for ship and enemies happen at 20 fps
        ld a, (_draw_stage)
        and a
        jp z, stage0_ntsc
        dec a
        jp z, stage1_ntsc
        dec a
        jp z, stage2_ntsc
        dec a
        jp z, stage3_ntsc
        dec a
        jp z, stage4_ntsc
        dec a
        jp z, stage5_ntsc
	dec a
	jp z, stage6_ntsc
	dec a
	jp z, stage7_ntsc 

.stage8_ntsc	; switch screen
         call DrawMap_stage6
         xor a
         ld (_draw_stage), a

	 ; This is a special frame. Here we will process the background movement and new enemy creation
	 call _UpdateScore		; Update score table
	 call _ShowBombBar		; Show superbomb bar

         jp draw_finished
.stage7_ntsc

	jp stage5
.stage6_ntsc 

         call DrawMap_stage4_2_NTSC		
         ld a, (_draw_stage)
         inc a
         ld (_draw_stage), a

	 call _ProcessShipActions

         jp draw_finished

.stage5_ntsc

         call DrawMap_stage4_1_NTSC
         ld a, (_draw_stage)
         inc a
         ld (_draw_stage), a

         jp draw_finished
.stage4_ntsc

         call DrawMap_stage3_NTSC
         ld a, (_draw_stage)
         inc a
         ld (_draw_stage), a
	 call _ProcessEnemyActions
         jp draw_finished

.stage3_ntsc

         call DrawMap_stage2_2_NTSC
         ld a, (_draw_stage)
         inc a
         ld (_draw_stage), a
	call _ProcessShipActions
         jp draw_finished	

.stage2_ntsc

         call DrawMap_stage2_1_NTSC
         ld a, (_draw_stage)
         inc a
         ld (_draw_stage), a

         jp draw_finished
.stage1_ntsc
         call DrawMap_stage1_NTSC
         ld a, (_draw_stage)
         inc a
         ld (_draw_stage), a
 	call _ProcessEnemyActions
         jp draw_finished
.stage0_ntsc
	 jp stage0

.mapdraw50hz				; for PAL machines, we draw the map in 7 steps (50/7 == 7.1fps)
        ld a, (_draw_stage)
        and a
        jp z, stage0
        dec a
        jp z, stage1
        dec a
        jp z, stage2
        dec a
        jp z, stage3
        dec a
        jp z, stage4
        dec a
        jp z, stage5
.stage6 ; switch screen
         call DrawMap_stage6
         xor a
         ld (_draw_stage), a

	 ; This is a special frame. Here we will process the background movement and new enemy creation
	 call _ProcessShipActions
	 call _ProcessEnemyActions
	 ;call _ProcessSlowEnemies	; Process enemies that move at the same speed as the background
	 call _UpdateScore		; Update score table
	 call _ShowBombBar		; Show superbomb bar

         jp draw_finished
.stage5

         call DrawMap_stage5
         ld a, (_draw_stage)
         inc a
         ld (_draw_stage), a
	 call _ProcessSlowEnemies	; Process enemies that move at the same speed as the background
	 call _ProcessEnemyActions
         jp draw_finished
.stage4

         call DrawMap_stage4
         ld a, (_draw_stage)
         inc a
         ld (_draw_stage), a
;	 call _ProcessShipActions
         jp draw_finished
.stage3

         call DrawMap_stage3
         ld a, (_draw_stage)
         inc a
         ld (_draw_stage), a

	 call _ProcessShipActions
         jp draw_finished
.stage2						; puede ser este

         call DrawMap_stage2
         ld a, (_draw_stage)
         inc a
         ld (_draw_stage), a
;	 call _ProcessShipActions


         jp draw_finished
.stage1	

         call DrawMap_stage1
         ld a, (_draw_stage)
         inc a
         ld (_draw_stage), a
 	 call _ProcessEnemyActions


         jp draw_finished
.stage0
         call DrawMap_stage0
         ld a, (_draw_stage)
         inc a
         ld (_draw_stage), a
	 call _ProcessShipActions

.draw_finished
	; When everything is processed, draw sprites
	xor a
	ld (curspr), a
	call _DrawEnemies	; draw enemies and enemy shoots (also valid for final enemies and their shoots)
	call _DrawShipStuff	; all ship stuff but the ship itself
	call EndSpriteDraw      ; hide remaining sprites
	call _DrawShip		; draw ship at the end, to ensure no flicker
;					ld c, 7
;					ld b, 0
;					call WRTVDP_DI
#endasm





#asm
.not_in_game
; We should process music here, and any other stuff non-dependant on being in game

	ld a, (_slot2address)
	call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!

	ld a, (msxHZ)
	cp 60
	jr nz, callmusic	; for PAL MSXs, no changes
	ld hl, _DELAY60Hz
	dec (hl)
	jr nz, callmusic	; DELAY60Hz is 6 by default, so 1 out of 6 ints will be skipped
	ld a, 6
	ld (_DELAY60Hz), a	; if we skip the int, reset to 6
	jr nomusic
.callmusic
	ld a, (ROMBank2)
	push af
	ld a, (ROMBank3)
	push af
	; Set the player bank 6 in 0x8000
	ld a, 6
	call setROM2	

	ld a, (_current_song)	; get the song number
	ld hl, _rombank_music
	ld c, a
	ld b, 0	
	add hl, bc
	ld a, (hl)
	call setROM3		; Set the music bank in $A000	
	call WYZ_PLAY
	pop af
	call setROM3
	pop af
	call setROM2
.nomusic	
#endasm

}

// Update score table, if needed

void UpdateScore(void)
{
#asm
	ld a, (_update_score)
	and a
	jr z, noupdatescore
	dec a
	ld (_update_score), a
	ld b, 15
	ld c, 18
	ld hl, (_score)
	call PrintLarge_5
.noupdatescore
	; Do we need to update the number of superbombs?
	ld a, (_update_superb)
	and a
	jr z, noupdatesuperb
	dec a
	ld (_update_superb), a
	ld b, 9
	ld c, 19
	ld a, (_available_superbombs)
	call PrintLarge_2
.noupdatesuperb
	; Do we need to update the number of lifes?
	ld a, (_update_life)
	and a
	ret z
	dec a
	ld (_update_life), a
	ld b, 9
	ld c, 17
	ld a, (_life_counter)
	call PrintLarge_2
#endasm
}



// Process all ship-related actions:
//  - Move my shoots
//  - Calculate collisions between my shoots and the enemies or background
//  - Calculate collisions between the ship and the background
//  - Calculate collisions between the ship and a power up

void ProcessShipActions(void)
{
#asm
	ld a, (_slot2address)
	call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!
	ld a, (ROMBank2)
	push af
	ld a, (ROMBank3)
	push af
	; Set the player bank 6 in 0x8000
	ld a, 6
	call setROM2_DI


	ld b, MAX_ENEMIES
        ld iy, _my_active_shoots
.loop_move_shoot
	push bc
	ld a, (iy+2)
	and a
	jr z, loop_procesship_continue

	ld hl, _movement_funcs
	ld c, (iy+4)
        ld b, 0                    ; BC ==     my_active_shoots[i].movement
        add hl, bc
        add hl, bc                 ; HL ==     &movement_funcs[my_active_shoots[i].movement]
        ld c, (hl)
        inc hl
        ld b, (hl)
        push iy
        pop hl                     ; HL == my_active_shoots[i]

        push iy
	ld iyh, b
	ld iyl, c
.call_moveshoot
        call IndCall		; indirect call
        pop iy

.loop_procesship_continue
	ld bc, 12		; 12 = sizeof(struct Entity)
	add iy, bc		; go to the next entity
	pop bc
	dec b
	jp nz, loop_move_shoot	; continue loop


	pop af
	call setROM3_DI
	pop af
	call setROM2_DI
    	ld  a,(_RAMbank)
	call enableSLOT2	; Enable RAM in $8000 - $BFFF

; Check collisions for my_active_shoots and background

    ld ix, _my_active_shoots    ; pointer to the structure
    ld a, (_max_shoots)
    ld iyh, a                     ; IYh will be the counter
    call checkshootloop

; Check collisions for shoots and enemies
	ld a, (_max_shoots)
	ld b, a
	ld ix, _my_active_shoots

.checkshoots_loop
	push bc
	ld a, (ix+2)
	and a
	jp z, continue_loop_shoots        ; This shoot is active, check collisions
	push ix
	call _shoot_hits_enemy            ; HL will hold the result,
	pop ix
        ld a, 255
        cp l
        jp z, continue_loop_shoots        ; if != 255, we have found a collision

        ld a, l
        inc a
        ld bc, 12
        ld hl, _active_enemies - 12
.loop_multiply_12
        add hl, bc
        dec a
        jr nz, loop_multiply_12           ; HL is now active_enemies[dummy_n]

	; we need to substract the current weapon energy from the enemy energy
		
	ld bc, 5
	add hl, bc			; energy
	ld a, (_current_weapon_energy)
	ld c, a				
	ld a, (hl)		
	and a				; If energy is 0, ignore (not killable or already dead)
	jr z, shot_completed
	sub c
	jr c, enemy_dead
	jr z, enemy_dead		; if the energy is 0 or < 0, it is dead
	ld (hl), a			; new energy
; Play enemy hit sound
	; Play sound
	ld a, (_slot2address)
	call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!
	ld hl, FX_DAMAGE
	push hl
        call _wyz_effect_noint
        pop hl
    	ld  a,(_RAMbank)
	call enableSLOT2	; Enable RAM in $8000 - $BFFF	
	jr shot_completed
.enemy_dead
	ld (hl), 0			; enemy is dead
        inc hl                               ; param1
        ld (hl), 4
	dec hl
	dec hl					; movement
        ld (hl), MOVE_EXPLOSION
	dec hl
	ld a, (hl)			  ; type
	dec hl                             ; sprnum
        ld (hl), EXPLOSION_SPR

; Play enemy explosion sound
	push af

	; Play sound
	ld a, (_slot2address)
	call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!
	ld hl, FX_ORGANIC_EXPL
	push hl
        call _wyz_effect_noint
        pop hl
    	ld  a,(_RAMbank)
	call enableSLOT2	; Enable RAM in $8000 - $BFFF	
	pop af

; increment score according to enemy killed
	ld hl, _enemy_score
	ld e, a
	ld d, 0
	add hl, de			; DE points to the score earned by killing this enemy
	ld e, (hl)
;	ld d, 0
	ld hl, (_score)
	add hl, de	
	ld (_score), hl			; increment score and store
	ld a, 1
	ld (_update_score), a		; We need yo update the score
	; check if we get an extra life
	ld de, (_next_extralife)
	sbc hl, de			; if there is no carry, we get an extra life
	jr c, shot_completed
.extralife
	ld a, (_life_counter)
	inc a
	ld (_life_counter), a
	ld a, 1
	ld (_update_life), a		; extra life, and update scoreboard
	ld hl, (_next_extralife)
	ld de, 200
	add hl, de
	ld (_next_extralife), hl	; update the score to get the next extra life

.shot_completed
        xor a
        ld (ix+2), a                        ;     my_active_shoots[i].sprnum = 0;

.continue_loop_shoots
	ld bc, 12		; 12 = sizeof(struct Entity)
	add ix, bc
	pop bc
	dec b
	jp nz, checkshoots_loop	; continue loop


; Check collisions with the background
	ld a, (_mayday)
	and a
	ret nz		; skip the rest if already dying
	ld a, (_ship_y)
	add a, 2
	ld d, a
	ld a, (_ship_x)
	add a, 0
	ld e, a
	call checkship_vs_bkg
	ld a, (_ship_y)
	add a, 2
	ld d, a
	ld a, (_ship_x)
	add a, 12
	ld e, a
	call checkship_vs_bkg

; Now, check if the power up is colliding with the ship. If so, get power up!
.check_powerup
	ld hl, _power_up+2
	ld a, (hl)
	and a
	jp z, no_powerup


	ld a, (_ship_x)
	rrca
	rrca
	rrca
	and $1f
	ld (_shoot_xchar), a 		   ; shoot_xchar = ship_x / 8
	ld a, (_ship_y)
	rrca
	rrca
	rrca
	and $1f
	ld (_shoot_ychar), a 		   ; shoot_ychar = ship_y / 8

	ld hl, _power_up
	ld a, (hl)
	rrca
	rrca
	rrca
	and $1f
	ld hl, _shoot_xchar
	sub (hl)
        ld d, a     ; D =    power_up->x / 8 -   shoot_xchar
	ld hl, _power_up+1
	ld a, (hl)
	rrca
	rrca
	rrca
	and $1f
	ld hl, _shoot_ychar
    	sub (hl)
        ld e, a     ; E =    power_up->y / 8 -   shoot_ychar

	; we want to check if abs(D) is -1, 0, 1, 2 or 3, and abs(E) is 0 or 1. 
	; So we add one to E, and if the value as unsigned is less than 3, we got it
	; and add one to D, and if the value as unsigned is less than 5, we got it
	inc d
	inc e
	ld a, 4
	cp d
	jr c, no_powerup
	ld a, 2
	cp e
	jp c, no_powerup

	; Not ignoring, so... We got the power up!!!!!!! 
	; Play power up sound
	ld a, (_slot2address)
	call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!
	ld hl, FX_POWERUP
	push hl
        call _wyz_effect_noint
        pop hl
    	ld  a,(_RAMbank)
	call enableSLOT2	; Enable RAM in $8000 - $BFFF	

        ; Increase score
	ld hl, (_score)
        ld de, 20                       ; 200 more points
	add hl, de
	ld (_score), hl			; increment score and store
	ld a, 1
	ld (_update_score), a		; We need yo update the score

	ld hl, _power_up + 2
	ld (hl), 0
	ld bc, 5
	add hl, bc
	ld a, (hl)		; A holds the power up + 57			
	sub 57			; get the right one				
	and a
	jr z, powerup_more_superbombs
	dec a
	jr z, powerup_more_firepower

	ld (_current_weapon), a			; This is a normal powerup, starting at 59
	ld hl, _shoot_sprites
	ld c, a
	ld b, 0
	add hl, bc
	ld a, (hl)
	ld (_current_weapon_sprite), a 		; current_weapon_sprite = shoot_sprites[current_weapon];
	ld hl, _shoot_energy
	add hl, bc
	ld a, (hl)
	ld (_current_weapon_energy), a		;  current_weapon_energy = shoot_energy[current_weapon];
	ld hl, _shoot_max_number
	add hl, bc
	ld a, (hl)
	ld (_max_shoots), a			; max_shoots=shoot_max_number[current_weapon];
	jr no_powerup
.powerup_more_firepower
	ld a, (_max_shoots)
	rlca					; max_shoots * 2
	cp 9
	jr nc, no_powerup		; if the upgraded number of shoots is greater than 8, ignore
	ld (_max_shoots), a
	jr no_powerup
.powerup_more_superbombs
	ld a, (_available_superbombs)
	inc a
	ld (_available_superbombs), a
	ld a, 1
	ld (_update_superb), a
.no_powerup



#endasm

}


#asm
.checkshootloop
    ld a, (ix+2)		; get sprnum
    and a
    jp z, continue_collisionloop 	; ignore non-existing shoots

    ld a, (ix+0)                 
    add a, 2
    ld e, a			; E == shoot_x
    ld d, 24
    call Div8			; A holds x /24, D holds x % 24
    ld e, a
    ld a, (_map_xpos)
    add a, e			
    ld c, a			; C = x_tile = map_xpos + x/24 
    ld a, (_map_displacement)
    add a, a			; a * 2
    add a, d			; a = map_disp + x %24
    cp 24
    jp c, no_overflow		; a < 24
    inc c			; if a >= 24, increment x_tile
.no_overflow
    ld a, (ix+1)                ; a = shoot_y, c = shoot_x (in tile terms)
    add a, 4			; middle of the shoot
    rra 
    rra
    rra
    and $1f			; A/8, faster than sra a * 3
				; a = shoot_y (in tile terms), c = shoot_x (in tile terms)
    ld h, $80
    ld l, c			; hl holds the position of shoot_x (in tile terms) in the map
    ex af, af
    ld a, (_CurLevel_XLength)
    ld e,a
    ld d, 0
    ex af, af			; we need to add DE shoot_y times
    and a
    jp z, endloop_addde		; if a is zero, no need to add

.loop_addde
    add hl, de
    dec a
    jp nz, loop_addde		; at the end, HL has the tile address
.endloop_addde
    ld a, (hl)
    and a
    jp z, noshootcollision1
    ld (ix+2), 0                ; my_active_shoots[i].sprnum=0
    jp continue_collisionloop

.noshootcollision1		; now check again, for the lower side of the shoot
    ld a, (_CurLevel_XLength)	; just go one tile down
    ld e,a
    ld d, 0
    add hl, de

/*
    ld a, (ix+1)                ; a = shoot_y, c = shoot_x (in tile terms)
    add a, 12			; middle of the shoot
    sra a
    sra a
    sra a			; a = shoot_y (in tile terms), c = shoot_x (in tile terms)
    ld h, $80
    ld l, c			; hl holds the position of shoot_x (in tile terms) in the map
    ex af, af
    ld a, (_CurLevel_XLength)
    ld e,a
    xor a
    ld d, a
    ex af, af			; we need to add DE shoot_y times
    and a
    jp z, endloop_addde2		; if a is zero, no need to add

.loop_addde2
    add hl, de
    dec a
    jr nz, loop_addde2		; at the end, HL has the tile address
.endloop_addde2*/
    ld a, (hl)
    and a

    jp z, continue_collisionloop
    ld (ix+2), 0                ; my_active_shoots[i].sprnum=0
.continue_collisionloop
    ld bc, 12			; 12 == sizeof (struct Entity)
    add ix, bc
    dec iyh
    jp nz, checkshootloop
    ret
#endasm

#asm

; Check collisions between the ship and background
; Entry:
;	E: ship_x
;	D: ship_y

.checkship_vs_bkg

    push de
    ; E == ship_x
    ld d, 24
    call Div8			; A holds x /24, D holds x % 24
    ld e, a
    ld a, (_map_xpos)
    add a, e			
    ld c, a			; C = x_tile = map_xpos + x/24 
    ld a, (_map_displacement)
    add a, a			; a * 2
    add a, d			; a = map_disp + x %24

    cp 24
    jp c, no_overflow2		; a < 24
    inc c			; if a >= 24, increment x_tile
    dec 24			; a -24, to save it
.no_overflow2
    ld (_dummy_b), a		; save for later use (hacky I know...)
    pop de
    push de
    ld a, d             	; a = ship_y, c = ship_x (in tile terms)
    rra
    rra
    rra
    and $1f			; A/8
				; a = ship_y (in tile terms), c = ship_x (in tile terms)

    ld h, $80
    ld l, c			; hl holds the position of ship_x (in tile terms) in the map
    ex af, af
    ld a, (_CurLevel_XLength)
    ld e,a
    ld d, 0
    ex af, af			; we need to add DE ship_y times
    and a
    jp z, endloop_addde_s2		; if a is zero, no need to add

.loop_addde_s2
    add hl, de
    dec a
    jr nz, loop_addde_s2		; at the end, HL has the tile address
.endloop_addde_s2
    ld a, (hl)
    and a
    jr z, noshipcollision1
  
    ; there is -apparently- a collision. Add a shameless hack for level 3, were asteroids
    ; only use the middle third of the tile
    ld a, (_current_level)
    cp 2
    jr nz, vsbkg_nolevel3
    ; ok, for level 3, just check if the displacement in the tile is <8 or >=16
    ; in those cases, there is no collision
    ld a, (_dummy_b)			; this is why we saved it earlier!
    cp 4
    jr c, noshipcollision1
    cp 24
    jr nc, noshipcollision1


.vsbkg_nolevel3
#ifndef CHEAT
    ld a, 1
#else
    ld a, 0
#endif
    ld (_mayday), a                ; we are hitting the background
    pop de
    ret

.noshipcollision1		; now check again, for the lower side of the ship 
    pop de
    ld a, d                	; a = ship_y, c = ship_x (in tile terms)
    add a, 10
    rra
    rra
    rra
    and $1f			; A/8
				;a = ship_y (in tile terms), c = ship_x (in tile terms)
    ld h, $80
    ld l, c			; hl holds the position of ship_x (in tile terms) in the map
    ex af, af
    ld a, (_CurLevel_XLength)
    ld e,a
    ld d, 0
    ex af, af			; we need to add DE shoot_y times
    and a
    jp z, endloop_addde2_s2		; if a is zero, no need to add

.loop_addde2_s2
    add hl, de
    dec a
    jp nz, loop_addde2_s2		; at the end, HL has the tile address
.endloop_addde2_s2
    ld a, (hl)
    and a
;    jr z, exit_no_hit
    ret z
    ; there is -apparently- a collision. Add a shameless hack for level 3, were asteroids
    ; only use the middle third of the tile
    ld a, (_current_level)
    cp 2
    jr nz, vsbkg_nolevel3_2
    ; ok, for level 3, just check if the displacement in the tile is <8 or >=16
    ; in those cases, there is no collision
    ld a, (_dummy_b)			; this is why we saved it earlier!
    cp 4
    jr c, exit_no_hit
    cp 24
    jr nc, exit_no_hit
.vsbkg_nolevel3_2
#ifndef CHEAT
    ld a, 1
#else
    ld a, 0
#endif
    ld (_mayday), a                ; we are hitting the background
.exit_no_hit
    ret
#endasm


// Process all enemy-related actions
//  - Move enemies / power ups
//  - Process enemy behavior
//  - Move enemy shoots
//  - Calculate collisions between my shoots and the enemies

void ProcessEnemyActions(void)
{
 #asm
	ld a, (_slot2address)
	call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!
	ld a, (ROMBank2)
	push af
	ld a, (ROMBank3)
	push af
	; Set the player bank 6 in 0x8000
	ld a, 6
	call setROM2_DI

	ld b, MAX_ENEMIES
	ld ix, _active_enemies
	ld de, _enemy_active_shoots 
.moveenemy_loop
	push bc
	ld a, (ix+2)
	and a
	jp z, loop_move_enemyshoot


	ld a, (ix+4)			; If movement has the highest bit active, it is MOvE_SLOW
	and $80
	jp nz, loop_move_enemyshoot

	ld hl, _movement_funcs
	ld a, (ix+4)
	and $7f
	ld c, a
        ld b, 0                    ; BC ==     my_active_shoots[i].movement
        add hl, bc
        add hl, bc                 ; HL ==     &movement_funcs[my_active_shoots[i].movement]
        ld c, (hl)
        inc hl
        ld b, (hl)
        push ix
        pop hl                     ; HL == active_enemies[i]

        push ix
        push de
	ld iyh, b
	ld iyl, c
.call_moveenemy
        call IndCall			; indirect call
        pop de
        pop ix

.loop_move_enemyshoot
        push ix
        ld a, d
        ld ixh, a
        ld a, e
        ld ixl, a
	ld a, (ix+2)
	and a
	jr z, loop_move_continue

	ld hl, _movement_funcs
	ld a, (ix+4)
	ld c, a
        ld b, 0                    ; BC ==     enemy_active_shoots[i].movement
        add hl, bc
        add hl, bc                 ; HL ==     &movement_funcs[enemy_active_shoots[i].movement]
        ld c, (hl)
        inc hl
        ld b, (hl)
        push ix
        pop hl                     ; HL == enemy_active_shoots[i]

	ld iyh, b
	ld iyl, c
        push de
.call_moveenemyshoot
        call IndCall			; indirect call
        pop de 
.loop_move_continue
        pop ix
	ld bc, 12		; 12 = sizeof(struct Entity)
	add ix, bc		; go to the next entity
	ld hl, 12
	add hl, de
	ex de, hl
	pop bc
	dec b
	jp nz, moveenemy_loop	; continue loop

#endasm
// Move enemies with follow_* movement types, only if current_level is 6
// Actually we are executing this twice, but it is not a big issue. This just
// ensures that, if the reference enemy is located later in the active_enemies
// table, continuity is done properly, as the one following must be processed later

#asm
	ld a, (_current_level)
	cp 6
	jp nz, nolevel7
	ld b, MAX_ENEMIES
	ld ix, _active_enemies

.moveenemyfollow_loop
	push bc
        push ix
	ld a, (ix+2)
	and a
	jp z, loop_movefollow_continue

	ld hl, _movement_funcs
	ld a, (ix+4)
	cp 22			;MOVE_FOLLOW_RIGHT
	jr z, do_follow_movement
	cp 23			;	MOVE_FOLLOW_DOWN
	jr z, do_follow_movement
	cp 24			; MOVE_FOLLOW_DOWNRIGHT
	jr nz, loop_movefollow_continue

.do_follow_movement
	ld c, (ix+4)
        ld b, 0                    ; BC ==     my_active_shoots[i].movement
        add hl, bc
        add hl, bc                 ; HL ==     &movement_funcs[my_active_shoots[i].movement]
        ld c, (hl)
        inc hl
        ld b, (hl)
        ld (call_moveenemyfollow + 1), bc

        pop hl                     ; HL == active_enemies[i]
        push ix
	ld iyh, b
	ld iyl, c
.call_moveenemyfollow
        call IndCall			; indirect call
.loop_movefollow_continue
        pop ix
	ld bc, 12		; 12 = sizeof(struct Entity)
	add ix, bc		; go to the next entity
	pop bc
	dec b
	jp nz, moveenemyfollow_loop	; continue loop
.nolevel7
#endasm


#asm

; Process enemy behavior, only if the final enemy is not active

	ld a, (_final_enemy_active)
	and a
	jp nz, finalenemy_behavior
	ld b, MAX_ENEMIES
	ld ix, _active_enemies

.behavenemy_loop
	push bc
	ld a, (ix+2)
	and a
	jr z, loop_behav_continue	; continue if sprnum = 0
	; skip explosion detection if this is a power up
	ld a, (ix+3)
	cp 8				; ENEMY_POWERUP
	jr z, behav_skip_expl
	ld a, (ix+4)
	cp MOVE_EXPLOSION		
	jr z, loop_behav_continue	; and also if the enemy is exploding
.behav_skip_expl
	ld hl, _behavior_funcs
	ld c, (ix+10)
        ld b, 0                    ; BC ==     my_active_shoots[i].behavior
        add hl, bc
        add hl, bc                 ; HL ==     &behavior_funcs[my_active_enemies[i].behavior]
        ld c, (hl)
        inc hl
        ld b, (hl)

        push ix
        pop hl                     ; HL == active_enemies[i]

        push ix
	ld iyh, b
	ld iyl, c
.call_behav
        call IndCall                       ; Indirect call
        pop ix

.loop_behav_continue
	ld bc, 12		; 12 = sizeof(struct Entity)
	add ix, bc		; go to the next entity
	pop bc
	dec b
	jp nz, behavenemy_loop	; continue loop
	jp behav_complete

; Process final enemy behavior, when it is active
.finalenemy_behavior
	ld a, (_current_level)
	add a, a
	ld c, a
	ld b, 0
	ld hl, _fenemy_behavior_funcs
	add hl, bc		; HL ==     &fenemy_behavior_funcs[current_level]
	ld c, (hl)
	inc hl
	ld b, (hl)
        ld (call_fenemy_behav + 1), bc
	ld hl, _active_enemies	; Pass the active_enemies array, so we have some variables to play with
	ld iyh, b
	ld iyl, c
.call_fenemy_behav
	call IndCall 

	; check if we have beaten the final enemy
	ld a, (_fenemy_defeat)
	cp 10
	jr c, behav_complete
	cp 110
	jr nc, end_behav

	ld de, 1
	ld hl, (_score)
	add hl, de
	ld (_score), hl			; increment score and store
        ; Play effect
        ld hl, FX_SCORE
        push hl
        call _wyz_effect_noint
        pop hl


	ld a, 1
	ld (_update_score), a		; We need yo update the score 
	; check if we get an extra life
	ld de, (_next_extralife)
	sbc hl, de			; if there is no carry, we get an extra life
	jr c, behav_complete
.endlevel_extralife
	ld a, (_life_counter)
	inc a
	ld (_life_counter), a
	ld a, 1
	ld (_update_life), a		; extra life, and update scoreboard
	ld hl, (_next_extralife)
	ld de, 200
	add hl, de
	ld (_next_extralife), hl	; update the score to get the next extra life
.end_behav
	; we won!
	ld a, (_current_level)
	inc a
	ld (_current_level), a
	ld a, 7
	ld (_mayday), a		; end current level
	xor a
	ld (_respawn_xpos), a	; next level has to start at X=0
	
.behav_complete

	pop af
	call setROM3_DI
	pop af
	call setROM2_DI
    	ld  a,(_RAMbank)
	call enableSLOT2	; Enable RAM in $8000 - $BFFF

	ld ix, _enemy_active_shoots
	ld a, MAX_ENEMIES
	ld iyh, a
	call checkshootloop		; check collisions between enemy shoots and the background



; Check collisions with enemies
	ld a, (_mayday)
	and a
	ret nz	; no checks when already dying
.checkcol
	ld a, (_ship_y)
	ld e, a
	ld a, (_ship_x)
	ld d, a
	ld hl, _active_enemies
	call ship_hits_enemy
	inc a
	jr z, check_col_enemyshoots
#ifndef CHEAT
    	ld a, 1
#else
    	ld a, 0
#endif
	ld (_mayday), a
	ret

/*
.checkcol
	ld a, (_ship_y)
	add a, 2
	ld e, a
	ld a, (_ship_x)
	add a, 4
	ld d, a
	ld hl, _active_enemies
	call ship_hits_enemy
	inc a
	jr z, check2
#ifndef CHEAT
    	ld a, 1
#else
    	ld a, 0
#endif
	ld (_mayday), a
	ret
.check2
	ld a, (_ship_y)
	add a, 2
	ld e, a
	ld a, (_ship_x)
	add a, 12
	ld d, a
	ld hl, _active_enemies
	call ship_hits_enemy
	inc a
	jr z, check_col_enemyshoots
#ifndef CHEAT
    	ld a, 1
#else
    	ld a, 0
#endif
	ld (_mayday), a
	ret
*/
.check_col_enemyshoots
	ld a, (_ship_y)
	ld e, a
	ld a, (_ship_x)
	ld d, a
	ld hl, _enemy_active_shoots
	call ship_hits_shoot
	inc a
	ret z
#ifndef CHEAT
    	ld a, 1
#else
    	ld a, 0
#endif
	ld (_mayday), a
	ret
#endasm

}



// Process movement for enemies that move at the same speed as the background (2)
// - This includes power ups

void ProcessSlowEnemies(void)
{
  // Move power up
 #asm
	ld a, (_slot2address)
	call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!
	ld a, (ROMBank2)
	push af
	ld a, (ROMBank3)
	push af
	; Set the player bank 6 in 0x8000
	ld a, 6
	call setROM2_DI

	ld de, _power_up
	inc de
	inc de
	ld a, (de)
	and a
	jp z, ignore_move_powerup

	ld hl, _movement_funcs
	inc de
	inc de
	ld a, (de)
	ld c, a
        ld b, 0                    ; BC ==     my_active_shoots[i].movement
        add hl, bc
        add hl, bc                 ; HL ==     &movement_funcs[power_up.movement]
        ld c, (hl)
        inc hl
        ld b, (hl)
	ld hl, _power_up
	ld iyh, b
	ld iyl, c
.call_powerup
        call IndCall			; indirect call	
.ignore_move_powerup
 #endasm

 // Move slow enemies

#asm
	ld b, MAX_ENEMIES
	ld ix, _active_enemies
.moveenemy_loop_slow
	push bc
	ld a, (ix+2)
	and a
	jp z, loop_move_slow_continue
	ld a, (ix+4)			; If movement has the highest bit active, it is MOvE_SLOW
	and $80
	jp z, loop_move_slow_continue

	ld hl, _movement_funcs
	ld a, (ix+4)
	and $7f
	ld c, a
        ld b, 0                    ; BC ==     my_active_shoots[i].movement
        add hl, bc
        add hl, bc                 ; HL ==     &movement_funcs[my_active_shoots[i].movement]
        ld c, (hl)
        inc hl
        ld b, (hl)
        push ix
        pop hl                     ; HL == active_enemies[i]

        push ix
	ld iyh, b
	ld iyl, c
.call_moveenemy_slow
        call IndCall			; indirect call
        pop ix

.loop_move_slow_continue
	ld bc, 12		; 12 = sizeof(struct Entity)
	add ix, bc		; go to the next entity
	pop bc
	dec b
	jp nz, moveenemy_loop_slow	; continue loop


	pop af
	call setROM3_DI
	pop af
	call setROM2_DI
    	ld  a,(_RAMbank)
	call enableSLOT2	; Enable RAM in $8000 - $BFFF
#endasm
}


struct Entity * NewShoot(unsigned char x, unsigned char y)
{
#asm
	ld hl, 4
	add hl, sp
	ld a, (hl)
	and a
	jp z, nonewshoot		; Cannot create a shoot at X=0, it means we are at the end of the screen

	ld hl, 2
	add hl, sp
	ld a, (hl)
	cp 113
	jp nc, nonewshoot		; If Y > 112, also skip the shoot creation


	ld de, _my_active_shoots+2
	ld a, (_max_shoots)
	ld b, a

.searchshoot_loop
	ld a, (de)
	and a
	jr nz, next_searchshoot

	; sprnum is 0, so a candidate was found
	ld a, (_current_weapon_sprite)
	ld (de), a		;current_e->sprnum = current_weapon_sprite;
	dec de
	dec de
	
	ld hl, 4
	add hl, sp
	ld a, (hl)
	ld (de), a		; current_e->x = x;
	inc de
	dec hl
	dec hl			; hl points to Y
	ld a, (hl)
	ld (de), a		; current_e->y = y

	inc de
	inc de
	inc de
	ld a, MOVE_RIGHT
	ld (de), a		;  current_e->movement = MOVE_RIGHT;

	ld a, (_current_weapon)

	ld hl, _shoot_speed

	ld c, a
	ld b, 0

	add hl, bc
	ld a, (hl)		; A= shoot_speed[current_weapon]


;	ld a, 8
	inc de
	inc de
	ld (de), a		; current_e->param1 = shoot_speed[current_weapon];   // Fast for now, let's see the rest
	ld h, d
	ld l, e			; HL points to the entity created
	ld de, -6
	add hl, de
	ret
.next_searchshoot
	ld hl, 12
	add hl, de
	ex de, hl
	djnz searchshoot_loop
.nonewshoot
	ld hl, 0		; return NULL if all shoots are in use

#endasm
}
/*
struct Entity * NewShoot(unsigned char x, unsigned char y)
{
#asm
	ld hl, 4
	add hl, sp
	ld a, (hl)
	and a
	jp z, nonewshoot		; Cannot create a shoot at X=0, it means we are at the end of the screen

	ld hl, 2
	add hl, sp
	ld a, (hl)
	cp 113
	jp nc, nonewshoot		; If Y > 112, also skip the shoot creation


	ld ix, _my_active_shoots
	ld a, (_max_shoots)
	ld b, a

.searchshoot_loop
	ld a, (ix+2)
	and a
	jr nz, next_searchshoot
	; sprnum is 0, so a candidate was found
	ld a, (_current_weapon_sprite)
	ld (ix+2), a		;current_e->sprnum = current_weapon_sprite;

	ld hl, 4
	add hl, sp
	ld a, (hl)
	ld (ix+0), a		; current_e->x = x;
	dec hl
	dec hl			; hl points to Y
	ld a, (hl)
	ld (ix+1), a		; current_e->y = y
	ld a, MOVE_RIGHT
	ld (ix+4), a		;  current_e->movement = MOVE_RIGHT;

	ld a, (_current_weapon)
	ld hl, _shoot_speed

	ld e, a
	ld d, 0
	add hl, de
	ld a, (hl)		; A= shoot_speed[current_weapon]


;	ld a, 8
	ld (ix+6), a		; current_e->param1 = shoot_speed[current_weapon];   // Fast for now, let's see the rest
	ld a, ixh
	ld h, a
	ld a, ixl
	ld l, a			; HL points to the entity created
	ret
.next_searchshoot
	ld de, 12
	add ix, de
	djnz searchshoot_loop
.nonewshoot
	ld hl, 0		; return NULL if all shoots are in use
#endasm
}*/


#asm 
._NewEnemyShoot_FX
        ld hl, FX_DOUBLE_SHOOT
        push hl
        call _wyz_effect_noint
        pop hl
#endasm

struct Entity * NewEnemyShoot(unsigned char x, unsigned char y, unsigned char movement, unsigned char sprite)
{
#asm
	ld hl, 8
	add hl, sp
	ld a, (hl)
	and a
	jr z, nonewenemy		; Cannot create a shoot at X=0, it means we are at the end of the screen

       	ld hl, 6
       	add hl, sp
	ld a, (hl)
	cp 113
	jr nc, nonewenemy		; If Y > 112, also skip the shoot creation

	ld ix, _enemy_active_shoots
	ld a, MAX_ENEMIES
	ld b, a

.searchenemyshoot_loop
	ld a, (ix+2)
	and a
	jr nz, next_searchenemyshoot
	; sprnum is 0, so a candidate was found

	ld hl, 8
	add hl, sp
	ld a, (hl)
	ld (ix+0), a		; current_e->x = x;
	dec hl
	dec hl			; hl points to Y
	ld a, (hl)
	ld (ix+1), a		; current_e->y = y
	dec hl
	dec hl
	ld a, (hl)		;
	ld (ix+4), a		;  current_e->movement = movement;
	ld a, 8
	ld (ix+6), a		; current_e->param1 = 8;   // Fast for now, let's see the rest
	dec hl
	dec hl
	ld a, (hl)
	ld (ix+2), a		; current_e->sprnum = sprnum;
	ld a, ixh
	ld h, a
	ld a, ixl
	ld l, a			; HL points to the entity created
	ret
.next_searchenemyshoot
	ld de, 12
	add ix, de
	djnz searchenemyshoot_loop
.nonewenemy	
	ld hl, 0		; return NULL if all shoots are in use
#endasm
}


unsigned char shoot_hits_enemy(struct Entity *shoot)
{
#asm
    pop bc
    pop hl
    push hl
    push bc                           ; HL holds the pointer to the shoot struct

    ld a, (hl)
    ld d, a				; shoot->x
    inc hl
    ld a, (hl)
    ld e, a				; shoot->y
  
    ld hl,_active_enemies
    ld b, MAX_ENEMIES
    ld c, 0              ; C will serve as an enemy counter
.loop_hitenemy
    push bc
    inc hl
    inc hl
    ld a, (hl)
    dec hl
    dec hl
    and a
    jr z, cont_loop_hitenemy
    ; not zero, so this is an active enemy
    ld a, (hl)  ; current_e->x
    add a, 15
    sub d				; if distance_X < 0, not hitting
    jr c, cont_loop_hitenemy
    cp a, 30
    jr nc, cont_loop_hitenemy	; if distance_X > 30, not hitting
    inc hl
    ld a, (hl)  ; current_e->y
    add a, 15
    dec hl
    sub e				; if distance_y < 0, not hitting
    jr c, cont_loop_hitenemy
    cp a, 30
    jr nc, cont_loop_hitenemy	; if distance_y > 30, not hitting

    ; our shoot has hit an enemy, so we exit with the enemy value
    pop bc          ; to restore the stack
    ld h, 0
    ld l, c
    ret

.cont_loop_hitenemy
    ld bc, 12
    add hl, bc
    pop bc
    inc c     ; next enemy
    djnz loop_hitenemy
    ld hl, 255
    ret
#endasm
}

/*

unsigned char shoot_hits_enemy(struct Entity *shoot)
{
#asm
    pop bc
    pop hl
    push hl
    push bc                           ; HL holds the pointer to the shoot struct

    ld a, (hl)
    rrca
    rrca
    rrca
    and $1f
    ld (_shoot_xchar), a                ; shoot_xchar = shoot->x / 8
    inc hl
    ld a, (hl)
    rrca
    rrca
    rrca
    and $1f
    ld (_shoot_ychar), a                ; shoot_ychar = shoot->y / 8

    ld hl,_active_enemies
    ld b, MAX_ENEMIES
    ld c, 0              ; C will serve as an enemy counter
.loop_hitenemy
    push bc
    inc hl
    inc hl
    ld a, (hl)
    dec hl
    dec hl
    and a
    jr z, cont_loop_hitenemy
    ; not zero, so this is an active enemy

    ld a, (hl)  ; current_e->x
    rrca
    rrca
    rrca
    and $1f
    ld ix, _shoot_xchar
    sub (ix+0)
    ld d, a     ; D =    (current_e->x) / 8 -   shoot_xchar
    inc hl      ; current_e->y
    ld a, (hl)  ; current_e->y
    dec hl
    rrca
    rrca
    rrca
    and $1f
    ld ix, _shoot_ychar
    sub (ix+0)
    ld e, a     ; E =    (current_e->y) / 8 -   shoot_ychar

    ; we want to check if abs(D) and abs(E) is 0 or 1. So we add one, and if the value as
    ; unsigned is less than 3, we got it
    inc d
    inc e
    ld a, 2
    cp d
    jr c, cont_loop_hitenemy
    cp e
    jr c, cont_loop_hitenemy

    ; our shoot has hit an enemy, so we exit with the enemy value
    pop bc          ; to restore the stack
    ld h, 0
    ld l, c
    ret

.cont_loop_hitenemy
    ld bc, 12
    add hl, bc
    pop bc
    inc c     ; next enemy
    djnz loop_hitenemy
    ld h, 0
    ld l, 255
    ret
#endasm
}
*/

#asm
; Check if ship is hitting an enemy
; Input:
;	D: ship_x
;	E: ship_y
; 	HL: active_enemies
; Returns:
;	A: number of the enemy/shot hit, or 255 if nothing found

.ship_hits_enemy
    ld b, MAX_ENEMIES
    ld c, 0              ; C will serve as an enemy counter
.loop_ship_hitenemy
    push bc
    inc hl
    inc hl
    ld a, (hl)
    dec hl
    dec hl
    and a
    jr z, cont_loop_ship_hitenemy
    ; not zero, so this is an active enemy

    ld a, (hl)  ; current_e->x
    add a, 10
    sub d				; if distance_X < 0, not hitting
    jr c, cont_loop_ship_hitenemy
    cp a, 37
    jr nc, cont_loop_ship_hitenemy	; if distance_X > 36, not hitting
    inc hl
    ld a, (hl)  ; current_e->y
    add a, 13
    dec hl
    sub e				; if distance_y < 0, not hitting
    jr c, cont_loop_ship_hitenemy
    cp a, 25
    jr nc, cont_loop_ship_hitenemy	; if distance_y > 24, not hitting

    ; our ship has hit an enemy, so we exit with the enemy value
    pop bc          ; to restore the stack
    ld a, c
    ret

.cont_loop_ship_hitenemy
    ld bc, 12
    add hl, bc
    pop bc
    inc c     ; next enemy
    djnz loop_ship_hitenemy
    ld a, 255
    ret

/*
; Check if ship is hitting an enemy
; Input:
;	D: ship_x
;	E: ship_y
; 	HL: active_enemies
; Returns:
;	A: number of the enemy/shot hit, or 255 if nothing found

.ship_hits_enemy

    ld a, d
    rrca
    rrca
    rrca
    and $1f
    ld (_shoot_xchar), a                ; shoot_xchar = ship_x / 8
    ld a, e
    rrca
    rrca
    rrca
    and $1f
    ld (_shoot_ychar), a                ; shoot_ychar = ship_y / 8


    ld b, MAX_ENEMIES
    ld c, 0              ; C will serve as an enemy counter
.loop_ship_hitenemy
    push bc
    inc hl
    inc hl
    ld a, (hl)
    dec hl
    dec hl
    and a
    jr z, cont_loop_ship_hitenemy
    ; not zero, so this is an active enemy

    ld a, (hl)  ; current_e->x
    rrca
    rrca
    rrca
    and $1f
    ld ix, _shoot_xchar
    sub (ix+0)
    ld d, a     ; D =    (current_e->x) / 8 -   shoot_xchar
    inc hl      ; current_e->y
    ld a, (hl)  ; current_e->y
    dec hl
    rrca
    rrca
    rrca
    and $1f
    ld ix, _shoot_ychar
    sub (ix+0)
    ld e, a     ; E =    (current_e->y) / 8 -   shoot_ychar

    ; we want to check if abs(D) and abs(E) is 0 or 1. So we add one, and if the value as
    ; unsigned is less than 3, we got it
    inc d
    inc e
    ld a, 2
    cp d
    jr c, cont_loop_ship_hitenemy
    cp e
    jr c, cont_loop_ship_hitenemy

    ; our ship has hit an enemy, so we exit with the enemy value
    pop bc          ; to restore the stack
    ld a, c
    ret

.cont_loop_ship_hitenemy
    ld bc, 12
    add hl, bc
    pop bc
    inc c     ; next enemy
    djnz loop_ship_hitenemy
    ld a, 255
    ret

*/
; Check if ship is hitting an enemy shoot
; Input:
;	D: ship_x
;	E: ship_y
; 	HL: enemy_shoots
; Returns:
;	A: number of the enemy hit, or 255 if nothing found

.ship_hits_shoot

    ld b, MAX_ENEMIES
    ld c, 0              ; C will serve as an enemy counter
.loop_ship_hitshoot
    push bc
    inc hl
    inc hl
    ld a, (hl)
    dec hl
    dec hl
    and a
    jr z, cont_loop_ship_hitshoot
    ; not zero, so this is an active shoot

    ld a, (hl)  ; current_e->x
    add a, 6
    sub d				; if distance_X < 0, not hitting
    jr c, cont_loop_ship_hitshoot
    cp a, 28
    jr nc, cont_loop_ship_hitshoot	; if distance_X > 27, not hitting
    inc hl
    ld a, (hl)  ; current_e->y
    add a, 8
    dec hl
    sub e				; if distance_y < 0, not hitting
    jr c, cont_loop_ship_hitshoot
    cp a, 15
    jr nc, cont_loop_ship_hitshoot	; if distance_y > 14, not hitting

    ; our ship has hit an enemy, so we exit with the enemy value
    pop bc          ; to restore the stack
    ld a, c
    ret

.cont_loop_ship_hitshoot
    ld bc, 12
    add hl, bc
    pop bc
    inc c     ; next enemy
    djnz loop_ship_hitshoot
    ld a, 255
    ret
#endasm





// Levels have compressed data, let's see how it goes when loading them

void load_level(unsigned char level)
{

	dummy_b =  level_pages[level];
#asm
	di

	ld  a,(_RAMbank)	
	call enableSLOT2	; Enable RAM in $8000 - $BFFF
	
	ld a, (_dummy_b) 
	ld (ROMBank1), a
	ld ($6000), a		; Place compressed maps in $6000 - $7FFF
#endasm

	level_pointer=(unsigned char*)level_address[level];
	CurLevel_NTiles = *(level_pointer)++;
	CurLevel_XLength = *(level_pointer)++;	// Load the basic level data
	int_pointer=level_pointer;
	length_tiles = *(int_pointer);
	level_pointer+=2;
	int_pointer=level_pointer;
	length_map = *(int_pointer);
	level_pointer+=2;
	
	// Prepare tiles, preshift them, store them in their final location
	// We will use the temp area as decompression buffer
#asm	
	ld de, $d600
	ld hl, (_level_pointer)
	call depack

	ld a, 1
	ld (ROMBank1), a
	ld ($6000), a		; we need the engine back during the tile table creation

	ld de, $d600
	ld a, (_CurLevel_NTiles)
	ld b,a
	call CreaTablaTiles

	ld a, (_dummy_b)
	ld (ROMBank1), a
	ld ($6000), a

	; Now copy the colors to the right places
	ld a, (_CurLevel_NTiles)
	ld l, a
	ld h, 0				; we need to go to D600 + CurLevel_NTiles * 24 to get the colors
	add hl, hl
	add hl, hl
	add hl, hl			; CurLevel_Ntiles * 8
	ld d, h
	ld e, l
	add hl, hl			; CurLevel_NTiles *16
	add hl, de			; CurLevel_NTiles * 24
	ld de, $d600
	add hl, de			; hl now has the right pointer



	push hl
	ld de, $A200		; Colors, part 0
				; HL points tile 0 
	ld a, (_CurLevel_NTiles)
loop_copytile0:
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi			; Copy 8 bytes
	ld bc, 16		; to increment hl
	add hl, bc		; skip till the next tile 0
	dec a
	jr nz, loop_copytile0
	pop hl

	push hl
	ld de, 16
	add hl, de		; HL points to tile 2
	ld de, $A300		; Colors, part 2		
	ld a, (_CurLevel_NTiles)
loop_copytile2:
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi			; Copy 8 bytes
	ld bc, 16		; to increment hl
	add hl, bc		; skip till the next tile 2
	dec a
	jr nz, loop_copytile2
	pop hl
	

	ld de, $A000		; Colors, part 0 and 1
	ld a, (_CurLevel_NTiles)
loop_copytile01:
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi			; Copy 16 bytes
	ld bc, 8		; to increment hl
	add hl, bc		; skip till the next tile 0
	dec a
	jr nz, loop_copytile01
#endasm
	// now, copy the map (uncompress) to its final location
	level_pointer += length_tiles;
#asm
	ld de, $8000
	ld hl, (_level_pointer)
	call depack
#endasm
        // Copy enemy table
        level_pointer=(unsigned char*)enemy_address[level];
        dummy_i = *level_pointer; // Number of enemies in level
        level_pointer++;
        dummy_i *= sizeof (struct Enemy);

#asm
	ld de, (_enemy_locations)
	ld hl, (_level_pointer)
        ld bc, (_dummy_i)
        ldir


	ld a, 1
	ld (ROMBank1), a
	ld ($6000), a		; Place engine in $6000 - $7FFF
	ld a, (_slot2address)
	call enableSLOT2_EI	; Enable ROM in $8000 - $BFFF and re-enable interrupts
#endasm
}



void DrawShip(void)
{
#asm
	ld a, (_ship_x)
	ld b, a
	ld a, (_ship_y)
	ld c,a
	ld a, (_ship0spr)	
	ld d, 15
	ld e, a		; _ship01
	call DrawShipSprite

	ld a, (flickerptr)
	inc a
	and $1f
	ld (flickerptr),a
	ld a, (_ship_x)
	add a,16
	ld b, a
	ld a, (_ship_y)
	ld c,a
	ld a, (_ship0spr)
	inc a
	ld d, 15
	ld e, a		;_ship02
	call DrawShipSprite
	ld a, (flickerptr)
	dec a
	and $1f
	ld (flickerptr),a
#endasm
}

void DrawShipStuff(void)
{
#asm
	ld a, (_max_shoots)
	ld b, a
	ld hl, _my_active_shoots

.drawshoots_loop
	push bc
	inc hl
	inc hl
	ld a, (hl)
	and a
	jr z, loop_shoots

	ld e, (hl)
	push hl
	ld hl, _sprite_colors
	ld c, e
	ld b, 0
	add hl, bc
	ld d, (hl)	; get the sprite color
	pop hl


	dec hl
	ld c, (hl)
	dec hl
	ld b, (hl); bc = (x << 8) | y

	push hl
	call DrawSprite	
	pop hl
	inc hl
	inc hl
.loop_shoots
	ld bc, 10		; 12 = sizeof(struct Entity)	
	add hl, bc
	pop bc
	djnz drawshoots_loop	; continue loop


.draw_powerups
	ld hl, _power_up
	inc hl
	inc hl
	ld a, (hl)
	and a
	ret z			; if the power up is not active, just quit

	ld d, 4
	ld e, (hl)
	dec hl
	ld c, (hl)
	dec hl
	ld b, (hl); bc = (x << 8) | y

	call DrawSprite		; draw the power up, and that is it
#endasm
}



void DrawEnemies(void)
{
// Now, display all enemies, shoots, etc
#asm
	ld b, MAX_ENEMIES
	ld ix, _active_enemies
	ld iy, _enemy_active_shoots

.drawenemy_loop
	push bc
	ld a, (ix+2)
	and a
	jr z, loop_enemy_shoot


	ld e, (ix+2)

	ld hl, _sprite_colors
	ld c, e
	ld b, 0
	add hl, bc
	ld d, (hl)	; get the sprite color

	ld b, (ix+0)
	ld c, (ix+1)	; bc = (x << 8) | y


	call DrawSprite	

.loop_enemy_shoot
	ld a, (iy+2)
	and a
	jr z, loop_enemy_continue

	ld e, (iy+2)
	ld hl, _sprite_colors
	ld c, e
	ld b, 0
	add hl, bc
	ld d, (hl)	; get the sprite color
	ld b, (iy+0)
	ld c, (iy+1)	; bc = (x << 8) | y
	call DrawSprite	 
.loop_enemy_continue
	ld bc, 12		; 12 = sizeof(struct Entity)	
	add ix, bc
	add iy, bc		; go to the next entity
	pop bc
	djnz drawenemy_loop	; continue loop
#endasm
}

// Load background, as optimized tiles
// Note: the current version has 90 tiles used, 166 free.

void load_background(void)
{
#asm
	;  Set screens 1 bank 11 at $A000	
	ld a, 11
	call setROM3

	; First, uncompress pattern data an copy to VRAM
	ld hl, background
	ld de, $D600			; Safe buffer!
	call depack

	ld de, $1000
	ld bc, 2048
	ld hl, $d600
	call LDIRVRM_safe		; tile definition

	ld de, $1A00
	ld bc, 256
	ld hl, $d600+2048
	call LDIRVRM_safe		; tile map

	ld de, $3000
	ld bc, 2048
	ld hl, $d600+2048+256
	call LDIRVRM_safe		; color definition

	; Load font for numbers in score table
	;  Set screens 2 bank 11 at $A000	
	ld a, 12
	call setROM3

	ld de, $1400
	ld bc, 768		; 96 tiles, starting at tile 128
	ld hl, font_tiles
	call LDIRVRM_safe

	ld de, $3400
	ld bc, 768		; 96 tiles, starting at tile 128
	ld hl, font_colors
	call LDIRVRM_safe	
	

	; Print number of remaining lives, current score, high score and current level
	
	ld b, 9
	ld c, 17
	ld a, (_life_counter)
	call PrintLarge_2

	ld b, 9
	ld c, 19
	ld a, (_available_superbombs)
	call PrintLarge_2

	
	ld b, 15
	ld c, 18
	ld hl, (_score)
	call PrintLarge_5

	
	ld b, 23
	ld c, 19
	ld hl, (_hiscore)
	ld d, 10
	call PrintSmall_5

	ld b, 10
	ld c, 22
	ld a, (_current_level)
	inc a
	ld d, 0
	call PrintNumber

#endasm
}

// Clean upper two thirds of the screen (both tiles and color definitions, just in case)

void clean_screen(void)
{
#asm
	xor a
	ld de, $0000
	ld bc, $1000
	call FILLVRM

	xor a
	ld de, $2000
	ld bc, $1000
	call FILLVRM

	xor a
	ld de, $1800
	ld bc, 512
	call FILLVRM
#endasm
}

// Clean sprites from VDP
void clean_sprites(void)
{
#asm
	ld a, 0
	ld de, $1b00
	ld  bc, 128
	call FILLVRM
#endasm
}

void init_level(void)
{
 
 wyz_load_music(level_music[current_level]);
#asm
 ld a, (sound_selection)
 ld (SOUND_SFX), a
#endasm
 load_level(current_level);
 // Load specific sprites for this level: FIXME this must be done using tables
#asm
  di
#endasm
 LoadSprBlock(22,levelsprites[current_level],28);	// The sprite block for each level will always be placed starting on sprite number 22, with a length of 28 sprites

 if(current_level == 6) LoadSprBlock(0,193,10);		// For the last level, load the GENESIS sprites
#asm
 ei
#endasm

 // init everything
 ship_x=8;
 map_xpos=respawn_xpos;
 map_displacement=4;
 draw_stage=0;
 ship_y=64;
 speed_x=speed_y=frames_to_shoot=frames_fire_pressed=0;
 available_superbombs=1;
 final_enemy_active=0;
 fenemy_defeat=0;
 fenemy_activation_counter=0;


 load_background();
 clean_screen();

 #asm
	ld a, (_current_level)
	di
	call SetLevelPalette		; SetLevelPalette assumes DI on entry
	ei
 #endasm

 mayday=0; // not dying... yet
 current_weapon = 0;
 current_weapon_sprite = shoot_sprites[current_weapon];
 current_weapon_energy = shoot_energy[current_weapon];
 max_shoots=shoot_max_number[current_weapon];


 ship0spr=0;

// for (i=0;i<MAX_ENEMIES;i++) active_enemies[i].sprnum=my_active_shoots[i].sprnum=enemy_active_shoots[i].sprnum=0;
#asm
	ld de, _active_enemies+1
	ld hl, _active_enemies
	ld bc, 287
	ld (hl), 0
	ldir
#endasm


 power_up.sprnum=0;

 // Activate enemies
 new_enemy=0;

#asm
	di
	ld  a,(_RAMbank)	
	call enableSLOT2	; Enable RAM in $8000 - $BFFF
#endasm
 while((map_xpos+10) > enemy_locations[new_enemy].x) new_enemy++;

// Clean sprites
#asm
	xor a
	ld hl, $b000
	ld (hl),a
	ld de, $b001
	ld bc, 255
	ldir
#endasm

#asm
	ld a, (_slot2address)
	call enableSLOT2_EI	; Enable ROM in $8000 - $BFFF and re-enable interrupts
#endasm

}

void mainmenu(void)
{
#asm
	; Set the menu bank 5 in 0x8000
	ld a, 5
	call setROM2
	;  And screens 1 bank 11 at $A000	
	ld a, 11
	call setROM3
	ld de, (_score)		; get the score, to see if we have a high score!
	call menu
#endasm
}

char press_f5[]={'p','r','e','s','s',124,'c',124,'t','o',124,'c','o','n','t','i','n','u','e',0};
char press_fire[]={'p','r','e','s','s',124,'f','i','r','e',0};
char credits[]={'c','r','e','d','i','t','s',0};

unsigned char gameover(void)
{
#asm
	;  Set screens 2 bank 12 at $A000	
	ld a, 12
	call setROM3

	; First, uncompress pattern data and copy to VRAM
	ld hl, gameover_pattern
	ld de, $D600			; Safe buffer!
	call depack

	call VDP_ActDeact
	ld de, 0
	ld bc, 6912
	ld hl, $d600
	call LDIRVRM_safe		; forms + tile definition

	; Second, uncompress color data and copy to VRAM
	ld hl, gameover_color
	ld de, $D600			; Safe buffer!
	call depack	

	ld de, $2000
	ld bc, 6144
	ld hl, $d600
	call LDIRVRM_safe		; color definition


	ld de, $0400
	ld bc, 984		; 123 tiles, starting at tile 128, first third
	ld hl, font_tiles
	call LDIRVRM_safe

	ld de, $2400
	ld bc, 984		; 123 tiles, starting at tile 128, first third
	ld hl, font_colors
	call LDIRVRM_safe	

	ld de, $1400
	ld bc, 984		; 123 tiles, starting at tile 128, last third
	ld hl, font_tiles
	call LDIRVRM_safe

	ld de, $3400
	ld bc, 984		; 123 tiles, starting at tile 128, last third
	ld hl, font_colors
	call LDIRVRM_safe	

	; Print credits counter	
	ld b, 11
	ld c, 23
	ld ix,_credits
	call PrintString


	ld b, 19
	ld c, 23
	ld d, 10
	ld a, (_credit_counter)
	call PrintNumber

	; Enable VDP and continue...
	call VDP_ActDeact
#endasm

	if(credit_counter)
	{
		// Print "Press C to Continue"
#asm
	ld b, 6
	ld c, 20
	ld ix,_press_f5
	call PrintString
#endasm
	}
	else
	{
		// Print "Press Fire"
#asm
	ld b, 11
	ld c, 20
	ld ix,_press_fire
	call PrintString
#endasm
	}

	wyz_load_music(MUSIC_GAMEOVER);

	if(credit_counter)
	{
	#asm
		ld a, 150			; will wait for 15 seconds
	.loop_wait_credit
		halt
		halt
		halt
		halt
		halt				; this is 1/10 of a second
		push af
		ld e, a
		ld d, 10
		call Div8		; A = number /10 (first digit), D = remainder (second digit)	
		ld b, 15
		ld c, 1
		call PrintLarge_2
		ld hl, KEY_C
		call get_keyboard
		and a
		jr z, end_check_continue
		ld a, (_credit_counter)
		dec a
		ld (_credit_counter), a
		call _wyz_stop_music
		; Play sound

		ld a, (_slot2address)
		call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!
		ld hl, FX_START_GAME
		push hl
	        call _wyz_effect
	        pop hl
	    	ld  a,(_RAMbank)
		call enableSLOT2_EI	; Enable RAM in $8000 - $BFFF, re-enable interrupts

		ld b, 50
	.delay_continue
		halt
		djnz delay_continue
		pop af
		ld hl, 0
		ld (_score), hl
		ret				; return 0, meaning that we chose to continue
	.end_check_continue
		pop af
		dec a
		jr nz, loop_wait_credit
	#endasm		
	}
	else
	{
#asm
	 call waitfirepress
#endasm
	}
 wyz_stop_music();
 return 1;
}

#asm
.waitfirepress
	ld a, (_joy)
	and a, JOY_FIRE
	jr nz, waitfirerelease
	halt
	ld a, (_joystick_type)
	ld l, a
	ld h, 0
	push hl
	call _read_joystick
	ld a, l
	ld (_joy), a
	pop hl
	jr waitfirepress
.waitfirerelease
	ld a, (_joy)
	and a, JOY_FIRE
	jr z, waitcompleted
	halt
	ld a, (_joystick_type)
	ld l, a
	ld h, 0
	push hl
	call _read_joystick
	ld a, l
	ld (_joy), a
	pop hl
	jr waitfirerelease
.waitcompleted
	ret
#endasm

unsigned char happyend_string[]=
{
'c','o','n','g','r','a','t','u','l','a','t','i','o','n','s',125,0,
124,124,124,124,124,124,124,124,124,124,124,124,124,124,124,124,0,
'y','o','u',128,'v','e',124,'s','a','v','e','d',124,'u','s',124,0,
'f','r','o','m',124,'t','h','e',124,'d','o','r','k',124,124,124,0,
'a','t','t','a','c','k',127,124,'y','o','u',124,'c','a','n',124,0,
'n','o','w',124,'g','o',124,'h','o','m','e',124,'a','n','d',124,0,
'h','a','v','e',124,'s','o','m','e',124,'r','e','s','t',127,124,0,
124,124,124,124,124,124,124,124,124,124,124,124,124,124,124,124,0,
'b','u','t',124,'d','o','n',128,'t',124,'g','o',124,'t','o','o',0,
'f','a','r',124,'f','r','o','m',124,'t','h','e',124,124,124,124,0,
'b','a','s','e',126,124,'t','h','e','r','e',124,'a','r','e',124,0,
'o','t','h','e','r',124,'e','n','e','m','i','e','s',124,124,124,0,
'g','e','t','t','i','n','g',124,'r','e','a','d','y',124,'t','o',0,
'a','t','t','a','c','k',127,127,127,124,124,124,124,124,124,124,0,
124,124,124,124,124,124,124,124,124,124,124,124,124,124,124,124,0,
124,124,124,124,'t','h','e',124,'e','n','d',124,124,124,124,124,0
};

unsigned char cheater1[]=
{'r','e','a','l',124,'m','e','n',124,'a','c','c','e','p','t',0};
unsigned char cheater2[]=
{'t','h','e',124,'l','a','w','s',124,'o','f',124,'p','h','y','s','i','c','s',0};


void happyend(void)
{
#asm
	;  Set screens 4 bank 14 at $A000	
	ld a, 14
	call setROM3

	; First, uncompress pattern data and copy to VRAM
	ld hl, happyend_pattern
	ld de, $D600			; Safe buffer!
	call depack
	
	di
	call VDP_ActDeact
	ld de, 0
	ld bc, 6912
	ld hl, $d600
	call LDIRVRM_safe		; forms + tile definition

	; Second, uncompress color data and copy to VRAM
	ld hl, happyend_color
	ld de, $D600			; Safe buffer!
	call depack	

	ld de, $2000
	ld bc, 6144
	ld hl, $d600
	call LDIRVRM_safe		; color definition
	ei

	;  Set screens 2 bank 12 at $A000	
	ld a, 12
	call setROM3
	di
	ld de, $0400
	ld bc, 1024		; 128 tiles, starting at tile 128, first third
	ld hl, font_tiles
	call LDIRVRM_safe

	ld de, $2400
	ld bc, 1024		; 128 tiles, starting at tile 128, first third
	ld hl, font_colors
	call LDIRVRM_safe	

	ld de, $0C00
	ld bc, 1024		; 128 tiles, starting at tile 128, second third
	ld hl, font_tiles
	call LDIRVRM_safe

	ld de, $2C00
	ld bc, 1024		; 128 tiles, starting at tile 128, second third
	ld hl, font_colors
	call LDIRVRM_safe

	ld de, $1400
	ld bc, 1024		; 128 tiles, starting at tile 128, last third
	ld hl, font_tiles
	call LDIRVRM_safe

	ld de, $3400
	ld bc, 1024		; 128 tiles, starting at tile 128, last third
	ld hl, font_colors
	call LDIRVRM_safe	
	ei

	; Enable VDP and continue...
	call VDP_ActDeact
#endasm
	if(*(unsigned char*)(inertia_cheat)) wyz_load_music(MUSIC_HAPPYEND_CHEATER);
		else	wyz_load_music(MUSIC_HAPPYEND_OK);	
#asm
	; Print all congrats strings
	ld b, 0
	ld c, 4
	ld l, 16
	ld ix,_happyend_string

.happy_printstring
	ld a, 50
.happy_waitloop1
	halt
	dec a
	jr nz,	 happy_waitloop1
	
	push bc
	push hl
	push ix
	call PrintString
	pop ix
	pop hl
	pop bc
	inc c
	ld de, 17
	add ix, de
	dec l
	jr nz, happy_printstring


	ld a, 150
.happy_waitloop3
	halt
	dec a
	jr nz, happy_waitloop3

; remove message, removing one line each
	; Print all congrats strings
	ld b, 0
	ld c, 4
	ld l, 16
	ld ix,_happyend_string+17

.happy_removestring
	ld a, 10
.happy_waitloop2
	halt
	dec a
	jr nz,	 happy_waitloop2
	
	push bc
	push hl
	push ix
	call PrintString
	pop ix
	pop hl
	pop bc
	inc c
	dec l
	jr nz, happy_removestring

; decompress and show alien!
	;  Set screens 4 bank 14 at $A000	
	ld a, 14
	call setROM3

	; First, uncompress pattern data and copy to VRAM
	ld hl, happyend2_pattern
	ld de, $D600			; Safe buffer!
	call depack
	
	call VDP_ActDeact
	ld de, 0
	ld bc, 6912
	ld hl, $d600
	call LDIRVRM_safe		; forms + tile definition

	; Second, uncompress color data and copy to VRAM
	ld hl, happyend2_color
	ld de, $D600			; Safe buffer!
	call depack	

	ld de, $2000
	ld bc, 6144
	ld hl, $d600
	call LDIRVRM_safe		; color definition

	ld a, 12
	call setROM3
	ld de, $1400+96*8
	ld bc, 256		; 32 tiles, starting at tile 224, last third
	ld hl, font_tiles+96*8
	call LDIRVRM_safe

	ld de, $3400+96*8
	ld bc, 256		; 32 tiles, starting at tile 224, last third
	ld hl, font_colors+96*8
	call LDIRVRM_safe	
	call VDP_ActDeact


	ld a, (inertia_cheat)
	and a
	jr z, happy_nocheat
	
	; Print cheater strings
	ld b, 15
	ld c, 22
	ld ix,_cheater1
	call PrintString
	ld b, 13
	ld c, 23
	ld ix,_cheater2
	call PrintString
.happy_nocheat
	call waitfirepress
#endasm
	wyz_stop_music();
}

#asm
.PaintShipFull
	ld ix, _blink_startx
	ld iy, _blink_starty
	ld de, _blink_width
	exx
	ld de, _blink_height
	exx
.loop_changecolour
	push af

	ld hl, $2000	; beginning of the color area
	ld a, (iy+0)
	ld c, 0
	ld b, a		; Y * 256
	add hl, bc
	ld a, (ix+0)
	rla
	rla
	rla		; X * 8
	ld c, a
	ld b, 0
	add hl, bc	; HL = first position to change

	exx
	ld a, (de)
	exx
	ld b, a		; B= height
	ld a, (de)
	rla
	rla
	rla		
	ld c, a		; C = width in bytes to change

.innerloopY_changecolour
	ld a, (_blink_color)
	ex de, hl	; DE = start in VRAM
	push bc
	push de
	push hl
	ld b, 0
	di
	call FILLVRM
	ei
	pop de
	pop hl
	pop bc
	inc h
	djnz innerloopY_changecolour	

	pop af
	inc ix
	inc iy
	inc de
	exx
	inc de
	exx
	dec a
	jp nz, loop_changecolour
	ret

.PaintShip
	ld ix, _blink_startx
	ld iy, _blink_starty
	ld de, _blink_width
	exx
	ld de, _blink_height
	exx
	dec a
	jr z, loop_changecolour2
.loop_increment
	inc ix
	inc iy
	inc de
	exx
	inc de
	exx
	dec a
	jr nz, loop_increment


.loop_changecolour2
	push af

	ld hl, $2000	; beginning of the color area
	ld a, (iy+0)
	ld c, 0
	ld b, a		; Y * 256
	add hl, bc
	ld a, (ix+0)
	rla
	rla
	rla		; X * 8
	ld c, a
	ld b, 0
	add hl, bc	; HL = first position to change

	exx
	ld a, (de)
	exx
	ld b, a		; B= height
	ld a, (de)
	rla
	rla
	rla		
	ld c, a		; C = width in bytes to change

.innerloopY_changecolour2
	ld a, (_blink_color)
	ex de, hl	; DE = start in VRAM
	push bc
	push de
	push hl
	ld b, 0
	di
	call FILLVRM
	ei
	pop de
	pop hl
	pop bc
	inc h
	djnz innerloopY_changecolour2

	pop af
	ret

#endasm
void show_genesis_pieces(void)
{
#asm
	;  Set screens 3 bank 13 at $A000	
	ld a, 13
	call setROM3

	; First, uncompress pattern data and copy to VRAM
	ld hl, shippieces_pattern
	ld de, $D600			; Safe buffer!
	call depack

	call VDP_ActDeact
	ld de, 0
	ld bc, 6912
	ld hl, $d600
	call LDIRVRM_safe		; forms + tile definition

	; Second, uncompress color data and copy to VRAM
	ld hl, shippieces_color
	ld de, $D600			; Safe buffer!
	call depack	

	ld de, $2000
	ld bc, 6144
	ld hl, $d600
	call LDIRVRM_safe		; color definition


	; Play sound
	ld a, (ROMBank2)
	push af
	; Set the player bank 6 in 0x8000
	ld a, 6
	call setROM2	
	call ASSEMBLE_EFFECT
	pop af
	call setROM2
	
	; first, if previous_level == 5, we display the genesis assembled message
	ld a, (_previous_level)
	cp 5
	jr z, startpaint

	; Not 5, so hide the message :)
	; first area to hide: 68th tile in the second bank, 7 tiles 
	xor a
	ld de, $2a20
	ld bc, 56
	call FILLVRM
	; second area to hide: 83th tile in the second bank, 9 tiles 
	xor a
	ld de, $2a98
	ld bc, 72
	call FILLVRM

.startpaint
	call VDP_ActDeact
	ld a, $70
	ld (_blink_color), a
	ld a, (_previous_level)
	call PaintShipFull

	ld b, 5
.blinkloop
	push bc
	ld a, $40
	ld (_blink_color), a
	ld a, (_previous_level)
	call PaintShip
	ld b, 50
.sillyloop
	halt
	djnz sillyloop
	ld a, $70
	ld (_blink_color), a
	ld a, (_previous_level)
	call PaintShip
	ld b, 50
.sillyloop2
	halt
	djnz sillyloop2
	pop bc
	djnz blinkloop
#endasm
             wyz_stop_music();
}

/* Tried to do a screen split... but it took too much CPU time :( */
/*
#asm
.msx2GameISR
	ld c, 7
	ld b, 3
	call WRTVDP_DI
	call SetDefaultPalette
	ld c, 7
	ld b, 0
	call WRTVDP_DI
	ret
#endasm*/

void main()
{
//  wyz_load_music(0);
#asm
        xor a
        ld (_in_game), a
	ld a, 1
	di
	ld (ROMBank1), a
	ld ($6000), a
	ei		; Place engine in $6000 - $7FFF
	call initROMbanks
	call searchramnormal80
	jp c, not_enough_ram			; if we found less than 32K of RAM, print an error message and halt
	ld (_RAMbank), a	; Remember where we found RAM


	ld a, (_slot2address)
	call enableSLOT2	; Enable ROM in $8000 - $BFFF	
;	ld a, (ROMBank2)
;	push af
	; Set the player bank 6 in 0x8000
	ld a, 6
	call setROM2	
	call WYZ_INIT		; init WYZ player
;	pop af
;	call setROM2


	ld a, (_RAMbank)
	call enableSLOT2	; Enable RAM 
	call INIT_ENGINE	 ; Initialize Screen 2
	ld hl, _gameISR
	call INSTALL_ISR	; Install ISR


	ld a, (_slot2address)
	call enableSLOT2	; Enable ROM in $8000 - $BFFF	
#endasm

// Some random initialization stuff
#asm
	; Copy default high scores
	ld de, hiscore_names
	ld hl, def_hiscore_names
	ld bc, 54
	ldir	
	ld de, hiscore_values
	ld hl, def_hiscore_values
	ld bc, 12
	ldir	

	; Initialize some variables that should have a default value
	xor a
	ld (_border_color),a
	ld (inertia_cheat),a
	ld a, 6
	ld (_DELAY60Hz), a
	
#endasm

 score=0;
 for(;;)
 {
#asm
	ei
	call Fadeout

                ld bc, $0107
                call WRTVDP	; black backdrop
#endasm
	// Call menu
	wyz_load_music(MUSIC_MAINMENU);
	mainmenu();
#asm
	call Fadeout
#endasm
	wyz_stop_music();
	// when we are back from the main menu, the selected joystick is now there
	// Load the common sprites, used everywhere
#asm
	call VDP_ActDeact
	di
#endasm
	LoadSprBlock(0, 0, 22);
	LoadSprBlock(50,50,14);
#asm
	ei
	call VDP_ActDeact
#endasm

#ifdef JUMPTOLEVEL
	#asm
	ld a, (starting_level)
	ld (_previous_level),a
	ld (_current_level),a
	#endasm
#else
	previous_level=current_level=0;
#endif
	next_extralife=200;		
	credit_counter=3;		// 3 credits
	respawn_xpos=0;
	score=0;
	update_superb=update_score=update_life=0;
#asm
		ld hl, (hiscore_values)
		ld (_hiscore), hl	; store high score
#endasm


	end_game=0;
	while(!end_game)
	{
		 life_counter=5;		// 5 lives to start with
		 while(life_counter != 255)
		 {
			if(current_level == 7)	// We won!!! 
			{
				happyend();
				life_counter=255;
				end_game=1;
			}
			else
			{
				if(previous_level != current_level)
				 {
				  if ((current_level > 1) && (current_level < 7))    show_genesis_pieces();
				  previous_level=current_level;
				 }
				#asm
				    call VDP_ActDeact
				#endasm
			 	init_level();


				/* Tried to do a screen split... but it took too much CPU time :( */
				/*#asm
				; On MSX2 machines, set the horizontal interrupt
        			ld	a,($2d)		; read MSX version
				or	a		; is it MSX1?
				jr z, nosetMSX2
				di
				ld a, 127
				call SetHorInterruptLine
				ld a, 16
				call EnDisHorInterrupts
				ld hl, msx2GameISR
				call INSTALL_HORISR
				ei
				.nosetMSX2
				#endasm*/
				#asm

				    ld a, 1
				    ld (_in_game), a
				    halt			; wait for 9 interrupts before enabling the display
				    halt			; this allows for a first display of the background :)
				    halt
				    halt
				    halt
				    halt
				    halt
				    halt
				    halt
				    call VDP_ActDeact
				#endasm
				 while(mayday<7)
				 {
				    #asm
					halt
				    #endasm
				    joy=read_joystick(joystick_type); 	
				    if((!mayday) && !(draw_stage & 1)) 	// only check joystick if we are not dying, and every other frame
				    {
					if(ship0spr == 6) ship0spr = 8;
					else	ship0spr=0;

					 if(*(unsigned char*)(inertia_cheat))
					 {
					  if(joy & JOY_UP)
					  {
						speed_y = -3;
						ship0spr=2;
					  }
					  else if(joy & JOY_DOWN)
					  {
						speed_y=3;
						ship0spr=4;
					  }
					  else speed_y=0;
					  
					  if(joy & JOY_LEFT) speed_x = -4;
					  else  if(joy & JOY_RIGHT)	speed_x = 4;
					    else speed_x=0;
					 }
					 else
					 {
					  if(joy & JOY_UP)
					  {
/*
						if(speed_y > -3) speed_y --;
						ship0spr=2;*/
					  #asm	
						ld a, (_speed_y)
						cp -2	; $fe
						jp nc, move_up_inertia
						cp 4
						jp nc, no_move_up_inertia
					.move_up_inertia
						dec a
						ld (_speed_y), a
					.no_move_up_inertia
						ld a, 2
						ld (_ship0spr), a
					  #endasm
					  }
					  else if(joy & JOY_DOWN)
					  {
					/*	if(speed_y < 3) speed_y ++;
						ship0spr=4;*/
					#asm
						ld a, (_speed_y)
						cp -4
						jp nc, move_down_inertia
						cp 3
						jp nc, no_move_down_inertia
					.move_down_inertia
						inc a
						ld (_speed_y), a
					.no_move_down_inertia
						ld a, 4
						ld (_ship0spr), a
			
					#endasm
					  }
					  else if(speed_y > 0) speed_y--;	// No keys pressed, decrease speed
					   else if(speed_y < 0) speed_y++;

					  if(joy & JOY_LEFT)
					  {
					/*	if(speed_x > -4) speed_x --;*/
					  #asm	
						ld a, (_speed_x)
						cp -3	
						jp nc, move_left_inertia
						cp 5
						jp nc, no_move_left_inertia
					.move_left_inertia
						dec a
						ld (_speed_x), a
					.no_move_left_inertia
					 #endasm
					  }
					  else  if(joy & JOY_RIGHT)
					  {
						/*if(speed_x < 4) speed_x++;*/
					#asm
						ld a, (_speed_x)
						cp -4
						jp nc, move_right_inertia
						cp 4
						jp nc, no_move_right_inertia
					.move_right_inertia
						inc a
						ld (_speed_x), a
					.no_move_right_inertia
					#endasm
					  }
					  else if(speed_x > 0) speed_x--;
					   else if(speed_x < 0) speed_x++;
					 }


					  if (joy & JOY_FIRE)
				  	  {
					#asm
						call DoShoot
					#endasm
					  }
					  else
					  {
						if ((frames_fire_pressed > 15) && (available_superbombs))
						{
						 Do_Superbomb();
						 available_superbombs--;		
						 update_superb=1;
						 #asm
							; Play sound
							ld a, (_slot2address)
							call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!
							ld hl, FX_BLAST
							push hl
							call _wyz_effect
							pop hl
						    	ld  a,(_RAMbank)
							call enableSLOT2_EI	; Enable RAM in $8000 - $BFFF	
						 #endasm
						}
						frames_fire_pressed=0;

					  }

					  // Update ship speed if needed

					  ship_x += speed_x;
					  if (ship_x > 208)
					  {
						if(speed_x > 0) ship_x = 208;
							else ship_x=0;
					  }
					  else if (ship_x < 8) ship_x = 8;

					  ship_y += speed_y;
					  if (ship_y > 112)
					  {
						if(speed_y > 0) ship_y = 112;
							else ship_y=0;
					  }

				    }
				   else if(mayday==1)
				   {
					ship0spr = MAINSHIP_EXPLOSION;
					life_counter--;
					mayday++;
					if (map_xpos >= level_checkpoints[(current_level <<2)+3]) respawn_xpos = level_checkpoints[(current_level <<2)+3];
					 else if (map_xpos >= level_checkpoints[(current_level <<2)+2]) respawn_xpos = level_checkpoints[(current_level <<2)+2];
					 else if (map_xpos >= level_checkpoints[(current_level <<2)+1]) respawn_xpos = level_checkpoints[(current_level <<2)+1];
					 else respawn_xpos = level_checkpoints[(current_level <<2)];
				//	wyz_stop_music();
					#asm
					; Play sound
					ld a, (_slot2address)
					call enableSLOT2	; Enable ROM in $8000 - $BFFF	; right at the end!
					ld hl, FX_EXPLOSION
					push hl
					call _wyz_effect
					pop hl
				    	ld  a,(_RAMbank)
					call enableSLOT2_EI	; Enable RAM in $8000 - $BFFF
					#endasm
				   }
				   else if((mayday) && (draw_stage & 1))
				   {
					ship0spr+=2;
					mayday++;
				   }
				  if (!draw_stage)
				  {
				    // Move map to new position
				    if((map_xpos < (CurLevel_XLength - 10)))
				    {
				    	//map_displacement++;
				  	//if (map_displacement > 0xb)
				  	//{
				  	//	map_displacement = 0;
				  	//	map_xpos++;
				   	//}
					map_displacement+=2;
				  	if (map_displacement > 0xb)
				  	{
				  		map_displacement -= 12;
				  		map_xpos++;
				   	}
				    }

				    else if(!final_enemy_active)  // Reached the end of the level, time to activate the final enemy!!!!
				    { 
					#asm
						ld  a,(_RAMbank)	
						call enableSLOT2	; Enable RAM in $8000 - $BFFF
					#endasm
					activate_final_enemy();
				    }

					#asm
						di
						ld  a,(_RAMbank)	
						call enableSLOT2	; Enable RAM in $8000 - $BFFF
					#endasm

				    while (((int)((enemy_locations[new_enemy].x - map_xpos) * 24) - (map_displacement<<1) + enemy_locations[new_enemy].x_desp) < 238)
				    {
				  	NewEnemy(&enemy_locations[new_enemy],0);
				  	new_enemy++;
				    }
					#asm
						ld a, (_slot2address)
						call enableSLOT2_EI	; Enable ROM in $8000 - $BFFF and re-enable interrupts
					#endasm

				    if (frames_to_shoot) frames_to_shoot--; // Less time to wait
				  }
				#asm
					ld a, (_border_color)
					and a
					jr z, nochangeborder
					dec a
					ld (_border_color), a
					ld c, 7
					ld b, a
					call WRTVDP
				.nochangeborder
				#endasm
				 }
				/* Tried to do a screen split... but it took too much CPU time :( */
				/*#asm
				; On MSX2 machines, disable the horizontal interrupt
        			ld	a,($2d)		; read MSX version
				or	a		; is it MSX1?
				jr z, nounsetMSX2
				di
				ld a, 0
				call EnDisHorInterrupts
				ld hl, 0
				call INSTALL_HORISR
				ei
				.nounsetMSX2
				#endasm*/
				clean_sprites();
				#asm
					xor a
					ld (_in_game), a
					ld b, 25
				.waitloop_endgame
					halt
					djnz waitloop_endgame
				#ifdef DUAL_PSG
					call _wyz_fade_music
				#else
					call _wyz_stop_music
				#endif
					call Fadeout
				#endasm
				wyz_stop_music();

			 	// Clean up sprite stuff here		 

			}
		 }
		 // Display the game over screen, with its music and all stuff. 
		 if (!end_game)
		 {
		 	end_game=gameover();
		 }
 	}
 }
}

// Fade out music. Only useful in physical version

#ifdef DUAL_PSG
void wyz_fade_music(void)
{
     #asm
	; We should not fade out for songs number 9 or 12
	ld a, (_current_song)
	cp 9
	ret z
	cp 12
	ret z

	ld a, (ROMBank2)
	push af
	; Set the player bank 6 in 0x8000
	ld a, 6
	call setROM2	

      	ld a, 6
        call FADE_MUSIC

	pop af
	call setROM2
     #endasm

}
#else
void wyz_fade_music(void)
{
}
#endif

unsigned char norammsg[]={'S','o','r','r','y',',',' ','I',' ','n','e','e','d',' ','a','t',' ','l','e','a','s','t',' ','3','2',' ','K','B',' ','o','f',' ','R','A','M',0};

// A simple message if there is not enough RAM
#asm
.not_enough_ram
	call $6C		; Switch to Screen 0
	ld hl, _norammsg
.noram_loop
	ld a, (hl)
	and a
	jr z, noram_end
	call $a2		; CHPUT
	inc hl
	jr noram_loop
.noram_end
	di
	halt
#endasm


// Here is the aplib depack routine, for now

#asm
; aPPack decompressor
; original source by dwedit
; very slightly adapted by utopian
; optimized by Metalbrain
; No license was found in the original code, so
; I assumed it is ok to release as ASL 2.0
; I hope I am not doing anything wrong with this

;hl = source
;de = dest

.depack		ld	ixl,128

.apbranch1	ldi
.aploop0	ld	ixh,1		;LWM = 0
.aploop		call 	ap_getbit // Load the common sprites, used everywhere
 		jr 	nc,apbranch1
		call 	ap_getbit
		jr 	nc,apbranch2
		ld 	b,0
		call 	ap_getbit
		jr 	nc,apbranch3
		ld	c,16		;get an offset
.apget4bits	call 	ap_getbit
		rl 	c
		jr	nc,apget4bits
		jr 	nz,apbranch4

		ld 	a,b
.apwritebyte	ld 	(de),a		;write a 0
		inc 	de
		jr	aploop0
.apbranch4	and	a
		ex 	de,hl 		;write a previous byte (1-15 away from dest)
		sbc 	hl,bc
		ld 	a,(hl)
		add	hl,bc
		ex 	de,hl
		jr	apwritebyte
.apbranch3	ld 	c,(hl)		;use 7 bit offset, length = 2 or 3
		inc 	hl
		rr 	c
		ret 	z		;if a zero is encountered here, it is EOF
		ld	a,2
		adc	a,b
		push 	hl
		ld	iyh,b
		ld	iyl,c
		ld 	h,d
		ld 	l,e
		sbc 	hl,bc
		ld 	c,a
		jr	ap_finishup2
.apbranch2	call 	ap_getgamma	;use a gamma code * 256 for offset, another gamma code for length
		dec 	c
		ld	a,c
		sub	ixh
		jr 	z,ap_r0_gamma		;if gamma code is 2, use old r0 offset,
		dec 	a
		;do I even need this code?
		;bc=bc*256+(hl), lazy 16bit way
		ld 	b,a
		ld 	c,(hl)
		inc 	hl
		ld	iyh,b
		ld	iyl,c

		push 	bc
		
		call 	ap_getgamma

		ex 	(sp),hl		;bc = len, hl=offs
		push 	de
		ex 	de,hl

		ld	a,4
		cp	d
		jr 	nc,apskip2
		inc 	bc
		or	a
.apskip2	ld 	hl,127
		sbc 	hl,de
		jr 	c,apskip3
		inc 	bc
		inc 	bc
.apskip3	pop 	hl		;bc = len, de = offs, hl=junk
		push 	hl
		or 	a
.ap_finishup	sbc 	hl,de
		pop 	de		;hl=dest-offs, bc=len, de = dest
.ap_finishup2	ldir
		pop 	hl
		ld	ixh,b
		jr 	aploop

.ap_r0_gamma	call 	ap_getgamma		;and a new gamma code for length
		push 	hl
		push 	de
		ex	de,hl

		ld	d,iyh
		ld	e,iyl
		jr 	ap_finishup


.ap_getbit	ld	a,ixl
		add	a,a
		ld	ixl,a
		ret	nz
		ld	a,(hl)
		inc	hl
		rla
		ld	ixl,a
		ret

.ap_getgamma	ld 	bc,1
.ap_getgammaloop call 	ap_getbit
		rl 	c
		rl 	b
		call 	ap_getbit
		jr 	c,ap_getgammaloop
		ret

#endasm
