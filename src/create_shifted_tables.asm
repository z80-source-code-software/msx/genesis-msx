;   Copyright 2012 Francisco Javier Peña
;
;   Licensed under the Apache License, Version 2.0 (the "License");
;   you may not use this file except in compliance with the License.
;   You may obtain a copy of the License at
;
;       http://www.apache.org/licenses/LICENSE-2.0
;
;   Unless required by applicable law or agreed to in writing, software
;   distributed under the License is distributed on an "AS IS" BASIS,
;   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;   See the License for the specific language governing permissions and
;   limitations under the License.

; Creación de la tabla de tiles shifteados,
; a partir de los 32 tiles normales de un nivel
;
; Entrada:
;	- DE: Puntero a la tabla
;	-  B: Número de tiles (1-32)
;
; Salida:
;	- TablaTiles_Temp (alineada en 4K): Tiles shifteados,
;         según el siguiente criterio:
;
;		$1010 XXYY YYYZ ZZZZ
;
;	   XX: shift (0-3)
;	   YYYYY: número de tile (0-31)
;	   ZZZZZ: bytes del tile shifteado (32). Los primeros 8 bytes
;                 son la primera columna, los siguientes la segunda, etc.
;
;	   Los tiles de origen están también organizados por columnas
;
; El algoritmo es sencillo (y no muy rápido), y se basa en hacer rl (hl) a lo bruto,
; después de organizar la tabla con los datos iniciales
;


CreaTablaTiles:	
	push iy
				; en primer lugar, limpia la tabla de tiles con ceros
	push de
	push bc
	ld hl, TablaTiles_Temp
	ld de, TablaTiles_Temp+1
	ld (hl),0
	ld bc, 4095
	ldir
	pop bc
	pop de
	
	ld hl, TablaTiles_Temp
	;ld a, b
	ld ixh, b		; ixh == number of tiles
	push bc
	push de
	ld bc, 8
copiatiles_outerloop:
	ld ixl, 24
	add hl,bc		; skip the first tile
copiatiles_innerloop:
	ld a, (de)
	inc de
	ld (hl), a		; just copy for now
	inc hl
	dec ixl
	jr nz, copiatiles_innerloop
	dec ixh
	jr nz, copiatiles_outerloop ; do it number of tiles times

	pop de
	pop bc
	ld hl, TablaTiles_Temp+1024
	;ld a, b
	ld ixh, b		; ixh == number of tiles
	push bc
	push de
	ld bc, 8
copiatiles_outerloop2:
	ld ixl, 24
	add hl,bc		; skip the first tile
copiatiles_innerloop2:
	ld a, (de)
	inc de
	ld (hl), a		; just copy for now
	inc hl
	dec ixl
	jr nz, copiatiles_innerloop2
	dec ixh
	jr nz, copiatiles_outerloop2 ; do it number of tiles times

	pop de
	pop bc
	ld hl, TablaTiles_Temp+2048
	;ld a, b
	ld ixh, b		; ixh == number of tiles
	push bc
	push de
	ld bc, 8
copiatiles_outerloop3:
	ld ixl, 24
	add hl,bc		; skip the first tile
copiatiles_innerloop3:
	ld a, (de)
	inc de
	ld (hl), a		; just copy for now
	inc hl
	dec ixl
	jr nz, copiatiles_innerloop3
	dec ixh
	jr nz, copiatiles_outerloop3 ; do it number of tiles times

	pop de
	pop bc
	ld hl, TablaTiles_Temp+3072
	;ld a, b
	ld ixh, b		; ixh == number of tiles
	push bc
;	push de			; not needed anymore
	ld bc, 8
copiatiles_outerloop4:
	ld ixl, 24
	add hl,bc		; skip the first tile
copiatiles_innerloop4:
	ld a, (de)
	inc de
	ld (hl), a		; just copy for now
	inc hl
	dec ixl
	jr nz, copiatiles_innerloop4
	dec ixh
	jr nz, copiatiles_outerloop4 ; do it number of tiles times

;	pop de
;	jr endCreaTablaTiles
	; Ahora tenemos los tiles ya copiados, con el primer tile en blanco
	; El siguiente paso es recorrer la tabla de tiles, haciendo rl (hl)


	ld iyl, 0		; un contador simple
	ld hl, TablaTiles_Temp
	ld (SaveTablaTiles), hl

newshift:
	ld a, iyl
	add a, 2
	ld iyh, a		; iyh mantiene el contador actual
	ld iyl, a		; iyl mantiene el contador para la siguiente vez
	ld hl, (SaveTablaTiles)
	ld de, 1024
	add hl, de
	ld (SaveTablaTiles), hl ; sumando 1024 pasamos al siguiente set de tiles shifteados
shiftloop:
	ld hl, (SaveTablaTiles)
	pop bc			; cleanup
	;ld a, b
	ld ixh, b		; ixh== numero de tiles
	push bc
	ld bc, 8		; lo usamos luego

rotateouter:
	ld a, 8			; 8 pixels de alto
	ld de, 24

looprotate:
	and a			; limpiar el carry flag
	add hl, de		; pasamos al ultimo
	rl (hl)
	rla			; guardar el carry flag en el bit menos significativo de A
	sbc hl, bc		; pasamos al anterior
	rra			; recuperamos el carry flag
	rl (hl)
	rla
	sbc hl, bc
	rra
	rl (hl)
	rla
	sbc hl, bc
	rra
	rl (hl)			; rotamos los 4 bytes

	inc hl			; siguiente línea
	dec a
	jr nz, looprotate
	
	add hl, de		; pasamos al siguiente megatile

	dec ixh
	jp nz, rotateouter	; seguimos
	dec iyh
	jp nz, shiftloop	; lo hacemos iyh veces
	ld a, iyl
	cp 6
	jr nz, newshift		; calculate new shift and go

endCreaTablaTiles:
	pop bc 		; cleanup
	pop iy

	; Now, we have to copy the tiles to the appropriate places!


	ld de, $9800		; Preshifted tiles, part 0 (bitmap)
	ld hl, $A000		; Tablatiles_Temp, pointing at tile 0 with no shift
	ld ixl, 4		; 4 iterations of the inner loop
outerloop_copytile0:
	ld a, 32		; Maximum number of tiles
loop_copytile0:
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi			; Copy 8 bytes
	ld bc, 24		; to increment hl
	add hl, bc		; skip till the next tile 0
	dec a
	jr nz, loop_copytile0
	; at the end of the first loop, DE is $9800 + 256, so it is the beginning of the preshifted tiles for shift 2
	; HL is $A000 + 1024, again at the beginning of the preshifted tiles for shift 2
	; so we can just continue with the loop 4 times and we are done
	dec ixl			
	jr nz, outerloop_copytile0	


	ld de, $9C00		; Preshifted tiles, part 2 (bitmap)
	ld hl, $A000+24		; Tablatiles_Temp, pointing at tile 2 with no shift
	ld ixl, 4		; 4 iterations of the inner loop
outerloop_copytile2:
	ld a, 32		; Maximum number of tiles
loop_copytile2:
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi
	ldi			; Copy 8 bytes
	ld bc, 24		; to increment hl
	add hl, bc		; skip till the next tile 2
	dec a
	jr nz, loop_copytile2
	dec ixl
	jr nz, outerloop_copytile2
	

	ld de, $9000		; preshifted tiles, parts 0-1 and 1-2
	ld hl, $A000+8		; TablaTiles_Temp, at tile 1 with no shift
	ld ixl, 4
outerloop_copytile1and2:
	ld a, 32		; maximum number of tiles
loop_copytile1and2:
	ldi	
	ldi
	ldi
	ldi
	ldi	
	ldi
	ldi
	ldi
	ldi	
	ldi
	ldi
	ldi
	ldi	
	ldi
	ldi
	ldi			; copy 16 bytes
	ld bc, 16		; to increment hl
	add hl, bc		; skip to tht next tile 1
	dec a	
	jr nz, loop_copytile1and2
	dec ixl
	jr nz, outerloop_copytile1and2

	; FIXME: for now we are ignoring the colors and making everything monochrome. This should go in another function!!!!
	;ld hl, $A000
	;ld de, $A001
	;ld a, $F1		; white foreground, black background
	;ld (hl), a
	;ld bc, 1023
	;ldir			; fill all tiles with this color

	ret

