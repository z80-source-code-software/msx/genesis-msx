;   Copyright 2012 Francisco Javier Peña
;
;   Licensed under the Apache License, Version 2.0 (the "License");
;   you may not use this file except in compliance with the License.
;   You may obtain a copy of the License at
;
;       http://www.apache.org/licenses/LICENSE-2.0
;
;   Unless required by applicable law or agreed to in writing, software
;   distributed under the License is distributed on an "AS IS" BASIS,
;   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;   See the License for the specific language governing permissions and
;   limitations under the License.

; MSX-2 specific routines, plus fade for MSX1
; Fade routines, one for MSX-1 and another one for MSX-2

default_palette: DW $000,$000,$611,$733,$117,$327,$151,$627,$171,$373,$661,$664,$411,$265,$555,$777
;default_palette: DW $0000,$0000,$0630,$0430,$0225,$0325,$0050,$0637,$0070,$0377,$0570,$0670,$0530,$177,$545,$767
;default_palette: DW $000,$000,$500,$700,$222,$555,$070,$727,$272,$474,$550,$770,$400,$266,$666,$777

LDIRMV	EQU $0059	; BIOS function to load from VRAM to memory


; Write palette
; Warning, it assumes interrupts are enabled on entry!!!!
; HL: palette

WRITEPALETTE:
	ld c, 	$99		; first VDP write register
	di			; interrupts could screw things up
	xor	a		; from color 0
	out	(c),a
	ld	a,128+16	; write R#16
	out	(c),a
	ei
	inc	c		; prepare to write palette data, to port 0x9A
	;ld	b,32		; 16 color * 2 bytes for palette data
	;otir				
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	ret

; Simple fade out

Fadeout:
        ld	a,($2d)		; read MSX version
	or	a		; is it MSX1?
	jp z,   fadeMSX1
fadeMSX2:
	ld hl, default_palette	; copy the default palette
	ld de, palette
	ld bc, 32
	ldir
	ld e, 7			; do it 7 times
loop_fademsx2:
	ld d, 16		; 16 words per palette
	ld hl, palette
decpalette:	
	ld a, (hl)	
	ld b, a			
	and $f0		; get the high nibble
	jr z, nodecrement2
	sub $10		; substract 1 from the high nibble		
nodecrement2:
	ld c, a			; and store it on C
	ld a, b
	and $0f		; get the low nibble
	jr z, nodecrement3	
	dec a			; decrement if not 0
nodecrement3:	
	or c			; combine the high and low nibbles
	ld (hl), a		; and send it back to the array	
	inc hl	
	ld a, (hl)		; this time we need 2 nibbles	
	or a			; is a == 0?
	jr z, nodecrement
	dec a
	ld (hl), a		; write the decremented value
nodecrement:
	inc hl
	dec d
	jr nz, decpalette 	; same for all 16 words
	halt
	halt
	halt
	halt	
	ld hl, palette
	call WRITEPALETTE	; write the new palette
	dec e
	jr nz, loop_fademsx2	
; Now clear the screen, and set the "good" palette again
	xor a
	ld de, $2000
	ld bc, 6144
	call FILLVRM
SetDefaultPalette:
	ld hl, default_palette	; copy the default palette
	call WRITEPALETTE
	ret


fadeMSX1:
	ld hl, $2000	; inicio de la zona de colores en VRAM
	ld de, $d600	; FIXME this is just a temporary location
	ld bc, 6144	; read the full color zone
	call LDIRMV

	ld b, 8		; 8 veces, 1 por línea
bucle_fade1:
	ld hl, $d600	
	ld a, b
	dec a
	ld e, a		; de = b - 1
	ld d, 0
	add hl, de	; la dirección de inicio + el byte a cargarse
	ld de, 768	; 6144 / 8 = 768
bucle_fade2:
	ld (hl), 0	; la línea se pone a 0
	inc hl
	inc hl
	inc hl
	inc hl
	inc hl
	inc hl
	inc hl
	inc hl		; y pasamos a la siguiente (+8), optimizar sería buena idea :)
	dec de
	ld a, d		; check if de != 0
	and a
	jr nz, bucle_fade2
	ld a, e
	and a			
	jr nz, bucle_fade2 ; if d == 0 and e == 0, continue
	push bc
	ld de, $2000   ; inicio de la zona de colores en VRAM
	ld hl, $d600	; FIXME this is just a temporary location
	ld bc, 6144	; write the full color zone
	call LDIRVRM_safe
	pop bc	
	djnz bucle_fade1 ;do it 8 times
	ret

; Set palette for a level
; Interrupts MUST be disabled on entry
; Input: A: level (0-6)

SetLevelPalette:
	; only write palette if this is a MSX2 or better
	ex af, af'
        ld	a,($2d)		; read MSX version
	or	a		; is it MSX1?
	ret z
	ex af, af'
	; multiply a*32
	rrca
	rrca
	rrca
	ld c, a
	ld b, 0
	ld hl, palette_level1
	add hl, bc
;	ld de, palette
;	ld bc, 32
;	ldir
	call WRITEPALETTE_DI
	ret

; Set line for horizontal interrupt
; A: line (0-255)
; Disable interrupts before doing this!!!

SetHorInterruptLine:
	ld c, 	$99		; first VDP write register
	out	(c),a		; write line
	ld	a,128+19	; write R#19
	out	(c),a
	ex 	af, af'
	ret

; Enable/disable horizontal interrupts
; A: 16 (enable) or 0 (disable)
; Disable interrupts before doing this!!!

RG0SAV: EQU $F3DF

EnDisHorInterrupts:
	ld c, a
	ld a, (RG0SAV)
	or c
	ld c, $99
	out (c), a		; write value
	ld a, 128		; write R#0
	out (c), a
	ret

; Write palette
; Warning, it assumes interrupts are enabled on entry!!!!
; HL: palette

WRITEPALETTE_DI:
	ld c, 	$99		; first VDP write register
	xor	a		; from color 0
	out	(c),a
	ld	a,128+16	; write R#16
	out	(c),a
	inc	c		; prepare to write palette data, to port 0x9A
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	outi
	ret

palette_level1: DW  $000,$000,$500,$700,$222,$555,$070,$727,$272,$474,$550,$770,$400,$266,$666,$777
palette_level2: DW  $000,$000,$500,$600,$336,$447,$241,$744,$452,$564,$671,$774,$400,$366,$764,$776
palette_level3: DW  $000,$000,$612,$743,$227,$527,$262,$757,$272,$474,$672,$774,$412,$466,$666,$777
palette_level4: DW  $000,$000,$512,$612,$527,$227,$461,$727,$471,$674,$472,$774,$412,$261,$767,$777
palette_level5: DW  $000,$000,$612,$734,$427,$547,$452,$747,$562,$662,$672,$774,$412,$466,$666,$777
palette_level6: DW  $000,$000,$612,$734,$207,$507,$252,$747,$262,$272,$672,$772,$512,$466,$666,$777
palette_level7: DW  $000,$000,$500,$700,$222,$555,$070,$727,$272,$474,$550,$770,$400,$266,$666,$777
