//   Copyright 2012 Francisco Javier Peña
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef CONSTANTS_H
#define CONSTANTS_H

#include "sprdefs.h"

#define MAX_ENEMIES 8
unsigned char level_pages[]={2,2,2,2,3,3,3};
unsigned int level_address[]={0x6000,0x66F2,0x6E7D,0x7466,0x6000,0x6920,0x723B};	// Level addresses
unsigned int enemy_address[]={0x63B9,0x6C24,0x70C5,0x798E,0x6567,0x6E9A,0x76AF};        // Enemy addresses
unsigned int finalenemy_address[]={0x7C87,0x7CA8,0x7CE1,0x7D02,0x7A30,0x7A51,0x7A72};  // Addresses of final enemy definitions
struct Enemy *enemy_locations = 0xA400; 
unsigned char *color_for_sprites = 0x9D00;

// Enemy and specific sprites for each level
unsigned int levelsprites[]={69,22,100,22,131,129,203};
// Final enemy sprites
unsigned int finalspr[]={76,93,113,64,159,175,216};
// Final enemy sprite count
unsigned int finalspr_count[]={17,7,16,5,16,18,16};	
// Level checkpoints (up to 4 per level, first is the beginning)
unsigned char level_checkpoints[]={ 0, 62, 142, 200,		// Level 1
				    0, 40, 84, 145,		// Level 2
				    0, 40, 83, 135,		// Level 3
				    0, 30, 78, 126,		// Level 4
				    0, 53, 100, 153,		// Level 5
				    0, 50, 100, 170,		// Level 6
				    0, 60, 140, 230};		// Level 7

// The enemy_sprites and behavior_types arrays have one entry per enemy type...

// Enemy sprites
unsigned char enemy_sprites[]={	ENEMY_EYE,
				ENEMY_CANNON_DOWN_LEFT,
				ENEMY_CANNON_DOWN_RIGHT,
				ENEMY_CANNON_UP,
				ENEMY_1,
				ENEMY_KAMIKAZE,
				ENEMY_FURBY,
				ENEMY_FINAL4_EYE,
				ENEMY_POWERUP,
				ENEMY_SHIP1,
				ENEMY_SHIP2,
				ENEMY_SHIP3,
				ENEMY_SHIP4,
				ENEMY_TURRET,
				ENEMY_FINAL1_SHOOT,
				ENEMY_ASTEROID,
				ENEMY_TRASH1,
				ENEMY_TRASH2,
				ENEMY_TRASH3,
				ENEMY_TRASH4,
				ENEMY_TRASH5,
				ENEMY_SALTARIN,
				ENEMY_CASCO,
				ENEMY_EGG,
				ENEMY_UGLYGUY,
				ENEMY_1_BIS,
				ENEMY_SHIP2_BIS,
				ENEMY_SHIP3_BIS,
				ENEMY_SALTARIN_BIS,
				ENEMY_CASCO_BIS,
				ENEMY_EGG_BIS,
				ENEMY_UGLYGUY_BIS,
				ENEMY_SHIP1_LEVEL7,
				ENEMY_SHIP2_LEVEL7,
				ENEMY_SHIP3_LEVEL7,
				ENEMY_SHIP4_LEVEL7,
				ENEMY_TURRET_LEVEL7,
				ENEMY_PACOSHIP_1,
				ENEMY_PACOSHIP_2,
				ENEMY_FINAL1_LEVEL7_UL,
				ENEMY_FINAL1_LEVEL7_UR,
				ENEMY_FINAL1_LEVEL7_DL,
				ENEMY_FINAL1_LEVEL7_DR		};

unsigned char enemy_score[]={	1,2,2,3,2,4,3,10,2,1,
				1,2,3,1,1,1,1,1,1,1,
				1,3,3,4,5,3,2,2,3,3,
				4,5,2,2,3,3,4,6,5,10,
				10,10,10};

// Final enemy sprites and behaviours
unsigned char finalenemy_sprites[]={FINALE_SPR1,FINALE_SPR2,FINALE_SPR3,FINALE_SPR4,FINALE_SPR5,FINALE_SPR6,FINALE_SPR7};

// Shoot sprites
unsigned char shoot_sprites[]={SHOT_BASIC,SHOT_BASIC,SHOT_LASER,SHOT_HOMING,SHOT_BOMB,SHOT_MEGA};
unsigned char shoot_energy[]={1,1,3,3,2,4};
unsigned char shoot_max_number[]={2,4,2,2,4,5};
unsigned char shoot_speed[]={8,8,6,6,8,4};

// Behaviors are directly associated to the enemy sprite with the same index

#define BEHAV_DO_NOTHING 0
#define BEHAV_SHOOT_LEFT 1
#define BEHAV_SHOOT_TARGET 2
#define BEHAV_SHOOT_TARGET_LEFT 3
#define BEHAV_SHOOT_TARGET_RIGHT 4
#define BEHAV_POWERUP 	5
#define BEHAV_TURRET 	6
#define BEHAV_SHOOT_LEFT_Y 7
#define BEHAV_SHOOT_LEFT_WAIT 8
#define BEHAV_ASTEROID 	9
#define BEHAV_SALTARIN 	10
#define BEHAV_CASCO 	11
#define BEHAV_EGG	12
#define BEHAV_UGLYGUY	13
#define BEHAV_SALTARIN_BIS 	14
#define BEHAV_CASCO_BIS 	15
#define BEHAV_FOLLOW	16
#define BEHAV_FINAL1_L7	17

unsigned char behavior_types[]=	{BEHAV_DO_NOTHING,		//ENEMY_EYE
				BEHAV_SHOOT_TARGET_LEFT,	//ENEMY_CANNON_DOWN_LEFT
				BEHAV_SHOOT_TARGET_RIGHT,	//ENEMY_CANNON_DOWN_RIGHT
				BEHAV_SHOOT_TARGET,		//ENEMY_CANNON_UP
				BEHAV_SHOOT_LEFT,		//ENEMY_1
				BEHAV_DO_NOTHING,		//ENEMY_KAMIKAZE
				BEHAV_DO_NOTHING,		//ENEMY_FURBY
				BEHAV_DO_NOTHING,		//ENEMY_FINAL4_EYE
				BEHAV_POWERUP,			//ENEMY_POWERUP
				BEHAV_DO_NOTHING,		//ENEMY_SHIP1
				BEHAV_SHOOT_LEFT_Y,		//ENEMY_SHIP2
				BEHAV_DO_NOTHING,		//ENEMY_SHIP3
				BEHAV_SHOOT_LEFT_WAIT,		//ENEMY_SHIP4
				BEHAV_TURRET,			//ENEMY_TURRET
				BEHAV_DO_NOTHING,		//ENEMY_FINAL1_SHOOT
				BEHAV_ASTEROID,			//ENEMY_ASTEROID
				BEHAV_DO_NOTHING,		//ENEMY_TRASH1
				BEHAV_DO_NOTHING,		//ENEMY_TRASH2
				BEHAV_DO_NOTHING,		//ENEMY_TRASH3
				BEHAV_DO_NOTHING,		//ENEMY_TRASH4
				BEHAV_DO_NOTHING,		//ENEMY_TRASH5
				BEHAV_SALTARIN,			//ENEMY_SALTARIN
				BEHAV_CASCO,			//ENEMY_CASCO
				BEHAV_EGG,			//ENEMY_EGG
				BEHAV_UGLYGUY,			//ENEMY_UGLYGUY
				BEHAV_SHOOT_LEFT_WAIT,		//ENEMY_1_BIS
				BEHAV_SHOOT_LEFT_Y,		//ENEMY_SHIP2_BIS
				BEHAV_DO_NOTHING,		//ENEMY_SHIP3_BIS
				BEHAV_SALTARIN_BIS,		//ENEMY_SALTARIN_BIS
				BEHAV_CASCO_BIS,		//ENEMY_CASCO_BIS
				BEHAV_EGG,			//ENEMY_EGG_BIS
				BEHAV_UGLYGUY,			//ENEMY_UGLYGUY_BIS
				BEHAV_DO_NOTHING,		//ENEMY_SHIP1_LEVEL7
				BEHAV_SHOOT_LEFT_Y,		//ENEMY_SHIP2_LEVEL7
				BEHAV_DO_NOTHING,		//ENEMY_SHIP3_LEVEL7
				BEHAV_SHOOT_LEFT_WAIT,		//ENEMY_SHIP4_LEVEL7
				BEHAV_TURRET,			//ENEMY_TURRET_LEVEL7
				BEHAV_DO_NOTHING,		//ENEMY_PACOSHIP_1
				BEHAV_FOLLOW,			//ENEMY_PACOSHIP_2
				BEHAV_FINAL1_L7,		//ENEMY_FINAL1_LEVEL7_UL
				BEHAV_FOLLOW,			//ENEMY_FINAL1_LEVEL7_UR
				BEHAV_FOLLOW,			//ENEMY_FINAL1_LEVEL7_DL
				BEHAV_FOLLOW,			//ENEMY_FINAL1_LEVEL7_DR
				};

// Default high scores
#asm
.def_hiscore_names  defb 93,94,94,95,96,91,91,91,0
		defb 's','e','j','u','a','n',91,91,0
		defb 'a','n','j','u','e','l',91,91,0
		defb 'p','a','g','a','n','t','i','p',0
		defb 'w','y','z',91,91,91,91,91,0
		defb 't','b','r','a','z','i','l',91,0		; 6 high scores for now

.def_hiscore_values defw 2000, 1500, 1000, 500, 250, 100
#endasm




// Music themes for each level and final enemy
unsigned char level_music[]={4,5,6,7,8,9,10};
unsigned char finalenemy_music[]={11,12,13,14,11,12,13};
// Effects channel for each music, A:1, B:2, C:3
#ifndef DUAL_PSG
unsigned char fxchannel_music[]=  {1,1,1,1,1,2,1,1,1,3,1,1,1,1,1};
#else
// For the dual PSG version, it will always be changing between 2 and 3
unsigned char fxchannel_music[]=  {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2};
#endif
// ROM bank for each music
unsigned char rombank_music[]={10,7,8,10,9,9,8,7,7,8,9,7,10,9,8};


#define MUSIC_MAINMENU		0
#define MUSIC_GAMEOVER  	1
#define MUSIC_HAPPYEND_OK 	2
#define MUSIC_HAPPYEND_CHEATER  3

// WYZ player addresses (get the good ones!!!!!!!!!)

#ifndef DUAL_PSG
	#define WYZ_INIT	0x8000		// PLAYER_INIT
	#define CARGA_CANCION 	0x8099		// CARGA_CANCION in the wyzplayer sym file
	#define WYZ_PLAY	0x8040		// It is INICIO in the wyzplayer sym file
	#define STOP_PLAYER	0x8074		// PLAYER_OFF

	#define LOAD_FX         0x85fd          // INICIA_EFECTO
	#define PLAY_FX         0x860d          // REPRODUCE_EFECTO
	#define STOP_FX         0x8661          // FIN_EFECTO
	#define FX_CHANNEL      0xC622          // CANAL_EFECTOS
	#define ASSEMBLE_EFFECT 0x8667		// ASSEMBLE_EFFECT
	#define SOUND_SFX	0xC5B5
#else
	#define WYZ_INIT	0x8000		// PLAYER_INIT
	#define CARGA_CANCION 	0x80C1		// CARGA_CANCION in the wyzplayer sym file
	#define WYZ_PLAY	0x801C		// It is INICIO in the wyzplayer sym file
	#define STOP_PLAYER	0x8071		// PLAYER_OFF
	#define FADE_MUSIC	0x802A

	#define LOAD_FX         0x8683          // INICIA_EFECTO
	#define PLAY_FX         0x86DA          // REPRODUCE_EFECTO
	#define STOP_FX         0x87A1          // FIN_EFECTO
	#define FX_CHANNEL      0xC622          // CANAL_EFECTOS
	#define ASSEMBLE_EFFECT 0x8792		// ASSEMBLE_EFFECT
	#define SOUND_SFX	0xC5B5
#endif


// FX definitions

#define FX_EXPLOSION    0               // Ship explosion                         
#define FX_BLAST        1               // Secondary shoot (blast)                
#define FX_HIT_CAPSULE  2               // Hit power-up capsule                   
#define FX_DAMAGE       3               // Hit enemy, but not kill it             
#define FX_DOUBLE_SHOOT 4               // Double shoot                           
#define FX_MENU_INERTIA 5               // Change inertia on main menu            
#define FX_POWERUP      6               // Get power up                           
#define FX_SINGLE_SHOOT 7               // Simple shoot                           
#define FX_TRIPLE_SHOOT 8               // Triple shoot                           
#define FX_START_GAME   9               // Start game                             
#define FX_ORGANIC_EXPL 10              // Explosion of organic creatures
#define FX_LASER	11		// Laser
#define FX_SCORE	12		// Increment score
#define FX_DISPARO_MULTI	13	// Multidirectional shoot
#define FX_DISPARO_HOMMING	14	// Homing missile

// Addresses in main menu
#define menu $8000
#define hiscore_names $C531
#define hiscore_values $C567
#define sound_selection $C526
#define inertia_cheat  0xC525
#define starting_level $c52d

// Static screen addresses
#define background	$B7D1		// Main game background, at page 11
#define font_colors	$A400
#define font_tiles	$A000		// Font for main game numbers, at page 12
#define gameover_color	$B1BD		// Game over screen, at page 12
#define gameover_pattern $A800
#define shippieces_pattern $A000	// Ship pieces screen, at page 13
#define shippieces_color $A88F
#define happyend_pattern $A000	// Happy end screen, at page 14
#define happyend_color $A658
#define happyend2_pattern $A95E	// Happy end screen with alien, at page 14
#define happyend2_color $B753

// Addresses in auxiliary memory area
#define DoShoot $7800

// Blink addresses when showing the Genesis pieces
unsigned char blink_startx[5]={14,6,12,15,19};
unsigned char blink_starty[5]={2,11,10,10,11};
unsigned char blink_width[5]={3,6,3,4,6};
unsigned char blink_height[5]={8,8,12,12,8};

#endif
