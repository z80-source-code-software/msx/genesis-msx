;   Copyright 2012 Francisco Javier Peña
;
;   Licensed under the Apache License, Version 2.0 (the "License");
;   you may not use this file except in compliance with the License.
;   You may obtain a copy of the License at
;
;       http://www.apache.org/licenses/LICENSE-2.0
;
;   Unless required by applicable law or agreed to in writing, software
;   distributed under the License is distributed on an "AS IS" BASIS,
;   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;   See the License for the specific language governing permissions and
;   limitations under the License.

; How are the sprites managed?
; We can load up to 64 sprites in the VDP memory (hey, this is the same as we can have on the Speccy, cool!)
; But we can only display 32 sprites at once, so...
; we must make sure that we are not attempting to draw more than 32 sprites in a single frame

; Flickering is implemented following an algorithm proposed by viejo_archivero
; We manage a copy of the 128 bytes sprite attribute table, and sprites are drawn there
; Every interrupt the table is transferred to screen

; Limited to 16x16 sprites
; Kind of compatible with the speccy version :)

; Input:
;	D: color: b0-3:Color, b4-6:unused, b7:EC (Early Clock) must be 0
;	E: sprite number
;	B: X position
;	C: Y position

; Used variables:
; 	curspr	: number of the sprite to draw, must be reset to zero every frame

; Required sprite alignment:
; Four 8x8 sub-sprites: up-left, low-left, up-right, low-right. Warning: not like the spectrum one!!! 
;			we have to swap the second and third sub-sprites
;
; So, in SevenuP terminology, this means: Char line, Y Char, X Char, Frame number, Mask, interleave sprite


DrawSprite:
	ld a, (curspr)
	cp 32
	ret nc			; too many sprites, we cannot go any further
	ld hl, flickerptr
	cp (hl)
	jr nz, draw_noflicker
	inc a
	inc a			; skip flickerptr, because the ship will be drawn there
	and $1f
	ld (curspr), a
draw_noflicker:
	add a, a
	add a, a		; A = curspr * 4, place in memory where to store the sprite attribute
	ld h, $B0
	ld l, a			; HL = pointer

	dec c			; the Y position for 0 is $ff, so decrement
	ld (hl), c
	inc l
	ld (hl), b
	inc l
	ld a, e
	add a, a
	add a, a
	ld (hl), a		; E * 4, since sprites are 16x16
	inc l
	ld (hl), d
	
	ld e, a			; we can play with A again
	ld a, $80
	add a, l
	ld l, a			; HL points to the 4th attribute in the second copy
	ld (hl), d
	dec l
	ld (hl), e
	dec l
	ld (hl), b
	dec l
	ld (hl), c

	ld a, (curspr)
	inc a
	ld (curspr), a		; curspr++, if we go past 32 we will corrupt something!!
		
	ret


DrawShipSprite:
	ld a, (flickerptr)
	add a, a
	add a, a		; A = curspr * 4, place in memory where to store the sprite attribute
	ld h, $B0
	ld l, a			; HL = pointer

	dec c			; the Y position for 0 is $ff, so decrement
	ld (hl), c
	inc l
	ld (hl), b
	inc l
	ld a, e
	add a, a
	add a, a
	ld (hl), a		; E * 4, since sprites are 16x16
	inc l
	ld (hl), d
	
	ld e, a			; we can play with A again
	ld a, $80
	add a, l
	ld l, a			; HL points to the 4th attribute in the second copy
	ld (hl), d
	dec l
	ld (hl), e
	dec l
	ld (hl), b
	dec l
	ld (hl), c
	ret


; Sprite transfer routine, including flickering management
;
; Used variables:
;
;	flickerptr: where to start transferring

EndSpriteDraw:
	; First, move unused sprites out of the screen...
	; This might be optimized a little, I guess
	ld a, (curspr)
	cp 32
	ret nc	; no need to mess with the memory!
	add a, a
	add a, a		; A = curspr * 4, place in memory where to store the sprite attribute
	ld h, $B0
	ld l, a			; HL = pointer
	add a, $80
	ld e, a
	ld d, $B0
	ld a, (curspr)
	sub 32
	neg			; we will run the loop 32 - curspr times
	ld b, a
	ld a, 200
moveoutloop:
	ld (hl), a
	inc l
	inc l
	inc l
	inc l
	ld (de), a
	inc e
	inc e
	inc e
	inc e
	djnz moveoutloop
        ret


	; The algorithm is simple: copy the first table to the second, then transfer to VRAM starting on flickerptr
	; flickerptr is incremented by 4 every time, to move the flickering pointer somewhere else
	; maybe we should implement a change and force the main ship to not flicker


TransferSprites:
	ld a, (flickerptr)
	add a, a
	add a, a		; A = curspr * 4, place in memory where to store the sprite attribute
	ld h, $B0
	ld l, a			; HL = pointer
	ld de, VRAMATTR
	call LDIRVRM_fast_128
	ld a, (flickerptr)
	add a, 2
	ld hl, curspr
	cp (hl)
	jr c, transfer_nowrap
	xor a
transfer_nowrap:
	ld (flickerptr), a
	ret

; Load sprite block in VRAM
;
; Input:
;	B -> dstspr (0-63): first sprite to load
;	C -> numspr (1-64): number of sprites to load
;	HL -> sourceptr : pointer to the source to load from
; Assumes interrupts are disabled

LoadSprBlock: 
	; First, build the destination address in VRAM: $3800 + dstspr*32
	ld a, b
	rrca
	rrca
	rrca
	ld d, a
	and $f8		; The 5 high bits go to E
	ld e, a
	ld a, d
	and $7		; The 3 low bits go to B, so numspr * 32
	or $38
	ld d, a		; DE: place in VRAM to store the sprite

	; Now, the amount of bytes to transfer is numspr * 32, or numspr << 5	
	ld a, c
	rrca
	rrca
	rrca
	ld b, a
	and $f8		; The 5 high bits go to C
	ld c, a
	ld a, b
	and $7		; The 3 low bits go to B, so numspr * 32
	ld b, a		; BC: amount of bytes to transfer

	call LDIRVRM_safe_DI
	ret
