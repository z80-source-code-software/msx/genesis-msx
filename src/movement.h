//   Copyright 2012 Francisco Javier Peña
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

#ifndef MOVEMENTS_H
#define MOVEMENTS_H
extern void __FASTCALL__  *movement_funcs[];

#define MOVE_NONE 0
#define MOVE_LEFT 1
#define MOVE_RIGHT 2
#define MOVE_PINGPONG 3
#define MOVE_EXPLOSION 4
#define MOVE_FURBY  5		
#define MOVE_KAMIKAZE 6
#define MOVE_WAVE 7
#define MOVE_LEFTANIM 8
#define MOVE_UPLEFT 9
#define MOVE_UPRIGHT 10
#define MOVE_DOWNLEFT 11
#define MOVE_LEFT_EXPIRE 12
#define MOVE_PINGPONG_FINAL4 13
#define MOVE_HOMING 14
#define MOVE_DOWNRIGHT 15
#define MOVE_UP 16
#define MOVE_TARGET 17
#define MOVE_KAMIKAZE_NOANIM 18
#define MOVE_WAVE_NOGONELEFT 19
#endif
